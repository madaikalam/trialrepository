using System;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections;
using System.Collections.Generic;
using CArt = ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class ChartFilters : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
             LoadFiltersData();
            if (!IsPostBack)
            {
               
                FilterBoxYears.Attributes.Add("onchange", "javascript:Years_onChange();");
                //Set default values
                Taxonomy999Selection.Value = FilterBoxYears.SelectedValue;
            }
        }

        /// <summary>
        /// Loads the filters data.
        /// </summary>
        public void LoadFiltersData()
        {
            string FilterXml = GetDataFilterXML();
            DataSet TaxonomyDatSet = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml);
            TaxonomyDatSet.Relations.Add("NodeRelation", TaxonomyDatSet.Tables[0].Columns["ID"], TaxonomyDatSet.Tables[0].Columns["parentTaxonomyID"]);
            //--
            //string[] temp = TaxonomyDatSet.Tables[0].Columns["taxonomytypeid"];
            //--
            buildTree(1, SingleFilterTaxonomyView1, TaxonomyDatSet,false);
            buildTree(2, SingleFilterTaxonomyView2, TaxonomyDatSet, false);
            buildTree(3, SingleFilterTaxonomyView3, TaxonomyDatSet, false);
            buildTree(10, SingleFilterTaxonomyView10, TaxonomyDatSet, false);

            buildTree(1, MultipleFilterTaxonomyView1, TaxonomyDatSet,true);
            buildTree(2, MultipleFilterTaxonomyView2, TaxonomyDatSet, true);
            buildTree(3, MultipleFilterTaxonomyView3, TaxonomyDatSet, true);
            buildTree(10, MultipleFilterTaxonomyView10, TaxonomyDatSet, true);

            BindYears();
        }

        /// <summary>
        /// Binds the years dropdown.
        /// </summary>
        private void BindYears()
        {
            //Get list of valid years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            string SelectedValue = StartYear.ToString();
            while (EndYear > StartYear)
            {
                StartYear++;
                SelectedValue += "," + StartYear;
            }

            string[] ValidYears = SelectedValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            FilterBoxYears.DataSource = ValidYears;
            FilterBoxYears.DataBind();

            MultipleFilterBoxYears.DataSource = ValidYears;
            MultipleFilterBoxYears.DataBind();
        }

        /// <summary>
        /// Builds the tree.
        /// </summary>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="treeView">The tree view.</param>
        /// <param name="taxonomyDatSet">The taxonomy dat set.</param>
        /// <param name="isMultipleFilter">Is multiple filter.</param>
        private void buildTree(int taxonomyID, 
            CArt.TreeView treeView,
            DataSet taxonomyDatSet,
            bool isMultipleFilter)
        {
            if (taxonomyDatSet.Tables.Count > 0)
            {
                string TaxonomySelectedValue = ((HiddenField)this.FindControl("Taxonomy" + taxonomyID + "Selection")).Value;
                TaxonomySelectedValue = TaxonomySelectedValue.StartsWith(",") ? TaxonomySelectedValue : "," + TaxonomySelectedValue;

                //Filter rows of the given taxonomy type
                DataRow[] rows = taxonomyDatSet.Tables[0].Select("taxonomyTypeID=" + taxonomyID);
                foreach (DataRow dbRow in rows)
                {
                    if (dbRow.IsNull("parentTaxonomyID"))
                    {
                        ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Name"].ToString(), 
                            dbRow["ID"].ToString(),
                            true, 
                            isMultipleFilter,
                            TaxonomySelectedValue.Contains(","+dbRow["ID"].ToString()+","));

                        treeView.Nodes.Add(newNode);

                        PopulateSubTree(dbRow, 
                            newNode,
                            isMultipleFilter,
                            TaxonomySelectedValue);
                    }
                }
            }
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The db row.</param>
        /// <param name="node">The node.</param>
        /// <param name="isMultipleFilter">Is multiple filter.</param>
        /// <param name="taxonomySelectedValue">The taxonomy selected value.</param>
        private void PopulateSubTree(DataRow dbRow, 
            CArt.TreeViewNode node,
            bool isMultipleFilter,
            string taxonomySelectedValue)
        {
            taxonomySelectedValue = taxonomySelectedValue.StartsWith(",") ? taxonomySelectedValue : "," + taxonomySelectedValue;

            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Name"].ToString(), 
                    childRow["ID"].ToString(),
                    true, 
                    isMultipleFilter,
                    taxonomySelectedValue.Contains(","+childRow["ID"].ToString() + ","));

                node.Nodes.Add(childNode);

                PopulateSubTree(childRow, 
                    childNode, 
                    isMultipleFilter,
                    taxonomySelectedValue);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hassCheckbox">Has checkbox</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text, 
            string value, 
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        /// <summary>
        /// Gets the data filter XML.
        /// </summary>
        /// <returns></returns>
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {

                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //Get all keys (ids) from dictionary
                        List<int> keys = new List<int>(selectedIDsList.Keys);
                        //convert int array to string array
                        string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });

                        //Build selections xml
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                            taxonomyRow["ID"].ToString(),
                            string.Join(",", SelectedIDs),
                            taxonomyRow["ColumnRowID"].ToString());
                    }

                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        //public string getclientid(string hdnname)
        //{
        //    return ((HiddenField)this.FindControl(hdnname)).ClientID;
        //}

    }
}
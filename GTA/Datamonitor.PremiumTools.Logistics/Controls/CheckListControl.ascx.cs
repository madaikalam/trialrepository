using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class CheckListControl : BaseSelectionControl
    {        
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private DataTable BuildGridData()
        {            
            //Get data to bind the check box list                      
            //Get current selections from sessionstate
            Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(FullTaxonomyTypeID);
            //Get taxonomy list           
            DataSet TaxonomyList = new DataSet();
            if (Source == "results")
            {
                string Path = Server.MapPath(GlobalSettings.TaxonomyDefaultsConfigPath);
                XmlDocument TaxonomyTypes = new XmlDocument();
                TaxonomyTypes.Load(Path);
                XmlNode TaxonomyType = TaxonomyTypes.SelectSingleNode("//TaxonomyDefaults/TaxonomyType[@ID='" + TaxonomyTypeID + "']");

                if (TaxonomyType != null)
                {
                    bool ShowFullTaxonomyInGridFilter = Convert.ToBoolean(TaxonomyType.Attributes.GetNamedItem("ShowFullTaxonomyInGridFilter").Value);
                    if (ShowFullTaxonomyInGridFilter)
                    {
                        TaxonomyList = SqlDataService.GetTaxonomyList(TaxonomyTypeID, Source);
                    }
                    else
                    {
                        string FilterXml = GetFilterCriteriaFromSession();
                        TaxonomyList = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml, TaxonomyTypeID);
                    }
                }                
            }
            else
            {
                TaxonomyList = ParentIDs.Length > 0 ?
                    SqlDataService.GetTaxonomyForSelection(TaxonomyTypeID, ParentIDs) :
                    SqlDataService.GetTaxonomyList(TaxonomyTypeID, Source);
            }

            DataTable TaxonomyTable = new DataTable();
            if (TaxonomyList!=null && TaxonomyList.Tables.Count > 0)
            {
                TaxonomyTable = TaxonomyList.Tables[0];
                TaxonomyTable.Columns.Add("CheckStatus");

                if (TaxonomyTable != null)
                {
                    //Update check box status based on current selections
                    foreach (DataRow Row in TaxonomyTable.Rows)
                    {
                        if (CurrentSelections != null &&
                            CurrentSelections.ContainsKey(Convert.ToInt32(Row["ID"].ToString())))
                        {
                            Row["CheckStatus"] = "checked";
                        }
                        else
                        {
                            Row["CheckStatus"] = "";
                        }
                    }
                }
            }
            return TaxonomyTable;
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public override void LoadData()
        {
            //check whether search textbox should be displayed or not
            string Path = Server.MapPath(GlobalSettings.TaxonomyLinksConfigPath);
            XmlDocument TaxonomyTypes = new XmlDocument();
            TaxonomyTypes.Load(Path);
            XmlNode TaxonomyType = TaxonomyTypes.SelectSingleNode("//Taxonomies/Taxonomy[@FullID='" + FullTaxonomyTypeID + "']");

            Grid1.ShowSearchBox = false;

            if (TaxonomyType != null &&
                TaxonomyType.Attributes.GetNamedItem("EnableSearch").Value == "true")
            {                
                Grid1.ShowSearchBox = true;
            }
            //bind grid
            Grid1.DataSource = BuildGridData();
            Grid1.DataBind();
            Grid1.ShowFooter = (Grid1.PageCount > 1);

        }

       

    }
}
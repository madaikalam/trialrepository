<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DynamicChartfilters.ascx.cs" Inherits="Datamonitor.PremiumTools.Generic.Controls.DynamicChartfilters" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<style type="text/css">
 .clear-left {    
    clear:left;    
    }
.rightdiv
{   
    margin-bottom:4px;
    width:200px;
    float:left;
}
.leftdiv
{
    width:200px;
    float:left;
    margin-bottom:2px;
    color:#000000;
}
input[type=checkbox] 
{
	width:5px;
	margin-left:-65px;
	margin-right:-65px;
	padding:0px;
	text-align:left;
}
</style>

<script type="text/javascript">
function SingleFilterTaxonomyView999_onNodeSelect(sender, eventArgs)
{       
    SingleFilterBox999.set_text(eventArgs.get_node().get_text());
    SingleFilterBox999.collapse();
    document.getElementById('<%=hdnSingleTaxonomy999Selection.ClientID %>').value = eventArgs.get_node().get_value();   
    $('#PlotChartMsg').show();
}
function MultipleFilterTaxonomyView999_onNodeCheckChange(sender,eventArgs)
{
    var hdnControl=document.getElementById('<%= hdnMultipleTaxonomy999Selection.ClientID %>');    
    OnMultipleTreeCheckChange(eventArgs.get_node(),hdnControl,MultipleFilterBox999);
    $('#PlotChartMsg').show();
}
function OnMultipleTreeCheckChangeText(checkedNode,hdnControl,filterBox)
{ 
    if (checkedNode.get_checked()) 
    {
        hdnControl.value = hdnControl.value  + checkedNode.get_value()+ ",";        
    }
    else
    {
        hdnControl.value = hdnControl.value.replace(checkedNode.get_value()+",","");
    }   
}

</script>

<div id="divPL" runat="server">
</div>
<div id="FilterDiv999" class="clear-left">
<div class="leftdiv"><asp:Label ID="lblYears" runat="server">Year:</asp:Label></div>
    <div id="SingleFilterDiv999" class="rightdiv">   
        <ComponentArt:ComboBox id="SingleFilterBox999" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="203"
            DropDownHeight="220"
            DropDownWidth="200" >
          <DropdownContent>
              <ComponentArt:TreeView id="SingleFilterTaxonomyView999" Height="220" Width="200"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
                <NodeSelect EventHandler="SingleFilterTaxonomyView999_onNodeSelect" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
    </div>
    <div id="MultipleFilterDiv999" class="rightdiv">
       <ComponentArt:ComboBox id="MultipleFilterBox999" runat="server"
            KeyboardEnabled="false"
            AutoFilter="false"
            AutoHighlight="false"
            AutoComplete="false"
            CssClass="comboBox"                                            
            HoverCssClass="comboBoxHover"
            FocusedCssClass="comboBoxHover"
            TextBoxCssClass="comboTextBox"
            DropDownCssClass="comboDropDown"
            ItemCssClass="comboItem"
            ItemHoverCssClass="comboItemHover"
            SelectedItemCssClass="comboItemHover"
            DropHoverImageUrl="../assets/images/drop_hover.gif"
            DropImageUrl="../assets/images/drop.gif"
            Width="203"
            DropDownHeight="220"
            DropDownWidth="200" >
          <DropdownContent>
              <ComponentArt:TreeView id="MultipleFilterTaxonomyView999" Height="220" Width="200"
                DragAndDropEnabled="false"
                NodeEditingEnabled="false"
                KeyboardEnabled="true"
                CssClass="TreeView"
                NodeCssClass="TreeNode"                                                        
                NodeEditCssClass="NodeEdit"
                SelectedNodeCssClass="NodeSelected"
                LineImageWidth="19"
                LineImageHeight="20"
                DefaultImageWidth="16"
                DefaultImageHeight="16"
                ItemSpacing="0"
                NodeLabelPadding="3"
                ImagesBaseUrl="../assets/images/tvlines/"
                LineImagesFolderUrl="../assets/images/tvlines/"
                ShowLines="true"
                EnableViewState="false"                           
                runat="server" >
              <ClientEvents>
                <NodeCheckChange EventHandler="MultipleFilterTaxonomyView999_onNodeCheckChange" />
              </ClientEvents>
              </ComponentArt:TreeView>
          </DropdownContent>
        </ComponentArt:ComboBox>
    </div>
    
    <asp:HiddenField ID="hdnSingleTaxonomy999Selection" runat="server" />
    <asp:HiddenField ID="hdnMultipleTaxonomy999Selection" runat="server" />
</div>

<input type="hidden" id="hdnControlsList" name="hdnControlsList" 
    value="<%=HiddenControlsList %>" />

using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Xml;
using System.Text;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using CArt=ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class DynamicChartfilters : System.Web.UI.UserControl
    {
        // PRIVATE FIELDS
        private string _yAxis; 
        private string filterOptionsXML;
        private StringBuilder ControlsList = new StringBuilder();
        private string _filterData;
        private string _filterDataText;
        private string ChartWizardIndicators;

        /// <summary>
        /// Gets the hidden controls list.
        /// </summary>
        /// <value>The hidden controls list.</value>
        public string HiddenControlsList
        {
            get { return ControlsList.ToString(); }
        }

        // PROPERTIES

        /// <summary>
        /// Gets or sets the type of the y-Axis.
        /// </summary>
        /// <value>The type of the y-Axis.</value>
        public string YAxis
        {
            get { return _yAxis; }
            set { _yAxis = value; }
        }
        /// <summary>
        /// Gets or sets the filetr options XML.
        /// </summary>
        /// <value>The filetr options XML.</value>
        public string FilterOptionsXML
        {
          get { return filterOptionsXML; }
          set { filterOptionsXML = value; }
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                DataSet ds = SqlDataService.GetPredefinedChartMetaData(Convert.ToInt32(Request.QueryString["id"]));

                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string filterXml = ds.Tables[0].Rows[0]["Selections"].ToString();
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(filterXml);
                    FilterOptionsXML = doc.SelectSingleNode("//FilterOptions").OuterXml;
                }
            }

            //Get chartwizard indicator selections
            if (GlobalSettings.ShowChartWizard &&
                Request.QueryString["source"] != null &&
                Request.QueryString["source"].ToString().Equals("wizard") &&
                Session["SelectedIndicators"] != null &&
                Session["SelectedIndicators"].ToString().Trim() != "")
            {
                ChartWizardIndicators = Session["SelectedIndicators"].ToString().Trim();
            }
            else
            {
                ChartWizardIndicators = "";
            }

            CreateFilterOptions();
            BindYears();
        }

        /// <summary>
        /// 
        /// </summary>
        private void CreateFilterOptions()
        {
            string FilterXml = GetDataFilterXML();

            string CurentPage = System.Web.HttpContext.Current.Request.Url.Segments[Request.Url.Segments.Length - 1];
            string Source = CurentPage.Equals("heatmap.aspx", StringComparison.CurrentCultureIgnoreCase) ? "heatmap" : null;

            DataSet TaxonomyDatSet = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml, YAxis, Source);

            TaxonomyDatSet.Relations.Add("NodeRelation", TaxonomyDatSet.Tables[0].Columns["ID"], TaxonomyDatSet.Tables[0].Columns["parentTaxonomyID"]);
            //Get taxonomy types list
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();
            
            //divPL.Controls.Add(pnl);
            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                XmlDocument doc = new XmlDocument();
                if (!string.IsNullOrEmpty(FilterOptionsXML))
                {
                    doc.LoadXml(FilterOptionsXML);
                }

                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    string TaxonomyTypeID = taxonomyRow["ID"].ToString();
                    string TaxonomyName = taxonomyRow["Name"].ToString();
                    string TaxonomyType = taxonomyRow["ColumnName"].ToString();
                    if (taxonomyRow["ShowInChartFilters"].ToString().ToLower() == "true" &&
                        TaxonomyDatSet.Tables[0].Select("taxonomyTypeID=" + TaxonomyTypeID).Length > 0)
                    {
                        Panel pnl = new Panel();
                        pnl.ID = "FilterDiv" + TaxonomyTypeID;
                        pnl.CssClass = "clear-left";
                        divPL.Controls.Add(pnl);
                        bool IsVisibleFilter = true;

                        if (!string.IsNullOrEmpty(FilterOptionsXML))
                        {
                            string xPathExpression = "//FilterOptions/TaxonomyType[@ID='" + TaxonomyTypeID + "']";
                            XmlElement element = (XmlElement)doc.SelectSingleNode(xPathExpression);

                            if (element != null)
                            {
                                IsVisibleFilter = Convert.ToBoolean(element.Attributes["Visible"].Value);
                            }
                        }

                        if (IsVisibleFilter)
                        {
                            //Add taxonomy name div
                            Panel pnlTaxonomyName = new Panel();
                            Label lbl = new Label();
                            lbl.Text = taxonomyRow["Name"].ToString() + ": ";
                            pnlTaxonomyName.Controls.Add(lbl);
                            pnlTaxonomyName.CssClass = "leftdiv";
                            pnl.Controls.Add(pnlTaxonomyName);
                        }
                        
                        
                        ControlsList.Append(TaxonomyTypeID);
                        ControlsList.Append("|");
                        //Building SingleFilterBox(dropdown)
                        CreateFilterDropdown(pnl,
                            TaxonomyTypeID,
                            TaxonomyDatSet,
                            false,
                            "Single",
                            IsVisibleFilter);
                        ControlsList.Append("|");
                        //Building MultipleFilterBox(dropdown)
                        CreateFilterDropdown(pnl,
                            TaxonomyTypeID,
                            TaxonomyDatSet,
                            true,
                            "Multiple",
                            IsVisibleFilter);
                        ControlsList.Append("|");
                        ControlsList.Append(TaxonomyName);
                        ControlsList.Append(";");

                    }
                }
                ControlsList.Append("999");
                ControlsList.Append("|");
                ControlsList.Append(hdnSingleTaxonomy999Selection.ClientID);
                ControlsList.Append("|");
                ControlsList.Append(hdnMultipleTaxonomy999Selection.ClientID);
                ControlsList.Append("|");
                ControlsList.Append("Year");
                ControlsList.Append(";");
            }
        }


        /// <summary>
        /// Creates the filter dropdown.
        /// </summary>
        /// <param name="pnl">The PNL.</param>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyDatSet">The taxonomy dataset.</param>
        /// <param name="isMultiple">if set to <c>true</c> [is multiple].</param>
        /// <param name="singleOrMultiple">The single or multiple.</param>
        /// <param name="isVisibleFilter">if set to <c>true</c> [is visible filter].</param>
        private void CreateFilterDropdown(Panel pnl,
            string taxonomyTypeID,
            DataSet taxonomyDatSet,
            bool isMultiple,
            string singleOrMultiple,
            bool isVisibleFilter)
        {
            HiddenField hdn = new HiddenField();
            hdn.ID = "hdn" + singleOrMultiple + "Taxonomy" + taxonomyTypeID + "Selection";
            pnl.Controls.Add(hdn);

            HiddenField hidden = new HiddenField();
            hidden.ID = "hdn" + singleOrMultiple + "Taxonomy" + taxonomyTypeID + "SelectionText";
            pnl.Controls.Add(hidden);

            //Prepare list of hidden fields            
            ControlsList.Append(hdn.ClientID);
            ControlsList.Append("@"+hidden.ClientID);

            
            if (isVisibleFilter)
            {
                CArt.ComboBox Combo = new CArt.ComboBox();
                Combo.ID = singleOrMultiple + "FilterBox" + taxonomyTypeID;
                Combo.SkinID = "ComboSkin";
                CArt.ComboBoxContent Content = new ComponentArt.Web.UI.ComboBoxContent();
                CArt.TreeView Tree = new CArt.TreeView();
                Tree.ID = singleOrMultiple + "FilterTaxonomyView" + taxonomyTypeID;
                Tree.SkinID = "TreeViewSkin";
                Content.Controls.Add(Tree);
                Combo.DropDownContent = Content;
                
                Panel childPnl = new Panel();
                childPnl.ID = singleOrMultiple + "FilterDiv" + taxonomyTypeID;
                childPnl.CssClass = "rightdiv";
                childPnl.Controls.Add(Combo);
                pnl.Controls.Add(childPnl);

                CArt.ClientEvent ce = new CArt.ClientEvent();
                if (!isMultiple)
                {
                    ce.EventHandler = "SingleFilterTaxonomyView" + taxonomyTypeID + "_onNodeSelect";
                    Tree.ClientEvents.NodeSelect = ce;
                }
                else
                {
                    ce.EventHandler = "MultipleFilterTaxonomyView" + taxonomyTypeID + "_onNodeCheckChange";
                    Tree.ClientEvents.NodeCheckChange = ce;
                }

                string Script = "";
                //if (!isMultiple)
                //{
                //    Script += " function SingleFilterTaxonomyView" + taxonomyTypeID + "_onNodeSelect(sender, eventArgs){ SingleFilterBox" + taxonomyTypeID + ".set_text(eventArgs.get_node().get_text()); SingleFilterBox" + taxonomyTypeID + ".collapse(); ";
                //    Script += "document.getElementById('" + hdn.ClientID + "').value = eventArgs.get_node().get_value(); }";
                //}
                //else
                //{
                //    Script += " function MultipleFilterTaxonomyView" + taxonomyTypeID + "_onNodeCheckChange(sender,eventArgs){ ";
                //    Script += " var hdnControl=document.getElementById('" + hdn.ClientID + "'); ";
                //    Script += " OnMultipleTreeCheckChange(eventArgs.get_node(),hdnControl,MultipleFilterBox" + taxonomyTypeID + "); } ";
                //}
                if (!isMultiple)
                {
                    Script += " function SingleFilterTaxonomyView" + taxonomyTypeID + "_onNodeSelect(sender, eventArgs){ SingleFilterBox" + taxonomyTypeID + ".set_text(eventArgs.get_node().get_text()); SingleFilterBox" + taxonomyTypeID + ".collapse(); ";
                    Script += "document.getElementById('" + hdn.ClientID + "').value = eventArgs.get_node().get_id(); ";                    
                    Script += "document.getElementById('" + hidden.ClientID + "').value = eventArgs.get_node().get_value(); $('#PlotChartMsg').show();}";
                }
                else
                {
                    Script += " function MultipleFilterTaxonomyView" + taxonomyTypeID + "_onNodeCheckChange(sender,eventArgs){ ";
                    Script += " var hdn=document.getElementById('" + hdn.ClientID + "'); ";
                    Script += " var hdnControl=document.getElementById('" + hidden.ClientID + "'); ";
                    Script += " OnMultipleTreeCheckChange(eventArgs.get_node(),hdn,MultipleFilterBox" + taxonomyTypeID + "); ";
                    Script += " OnMultipleTreeCheckChangeText(eventArgs.get_node(),hdnControl,MultipleFilterBox" + taxonomyTypeID + "); $('#PlotChartMsg').show();} ";
                }
                Page.ClientScript.RegisterClientScriptBlock(this.GetType(), singleOrMultiple + "TreeEventsScript" + taxonomyTypeID, Script, true);
            }
        }

        public void LoadDataInCallback(string filterData,string filterDataText)
        {
            _filterData = filterData;
            _filterDataText = filterDataText;

            LoadData();
        }

        /// <summary>
        /// Loads the data.
        /// </summary>
        public void LoadData()
        {           
            string FilterXml = GetDataFilterXML();
            string yaxisValue = string.IsNullOrEmpty(Request.QueryString["id"]) ? YAxis : string.Empty;            
            string CurentPage = System.Web.HttpContext.Current.Request.Url.Segments[Request.Url.Segments.Length - 1];
            string Source = CurentPage.Equals("heatmap.aspx", StringComparison.CurrentCultureIgnoreCase) ? "heatmap" : null;

            DataSet TaxonomyDatSet = SqlDataService.GetUniqueTaxonomyByCriteria(FilterXml, yaxisValue, Source);
            TaxonomyDatSet.Relations.Add("NodeRelation", TaxonomyDatSet.Tables[0].Columns["ID"], TaxonomyDatSet.Tables[0].Columns["parentTaxonomyID"]);
            //Get taxonomy types list
            DataSet TaxonomyTypes = GlobalSettings.GetTaxonomyTypes();

            if (TaxonomyTypes != null && TaxonomyTypes.Tables.Count > 0)
            {
                XmlDocument doc = new XmlDocument();
                if (!string.IsNullOrEmpty(FilterOptionsXML))
                {
                    doc.LoadXml(FilterOptionsXML);
                }
                foreach (DataRow taxonomyRow in TaxonomyTypes.Tables[0].Rows)
                {
                    string TaxonomyTypeID = taxonomyRow["ID"].ToString();
                    string TaxonomyName = taxonomyRow["Name"].ToString();
                    if (taxonomyRow["ShowInChartFilters"].ToString().ToLower() == "true" &&
                        TaxonomyDatSet.Tables[0].Select("taxonomyTypeID=" + TaxonomyTypeID).Length > 0)
                    {
                        CArt.TreeView SingleFilterTree = (CArt.TreeView)this.FindControl("SingleFilterTaxonomyView" + TaxonomyTypeID);
                        CArt.TreeView MultipleFilterTree = (CArt.TreeView)this.FindControl("MultipleFilterTaxonomyView" + TaxonomyTypeID);
                        bool IsVisibleFilter = true;
                        string SelectedIds = string.Empty;
                        string SelectedTexts = string.Empty;
                        if (!string.IsNullOrEmpty(FilterOptionsXML))
                        {
                            string xPathExpression = "//FilterOptions/TaxonomyType[@ID='" + TaxonomyTypeID + "']";
                            XmlElement element = (XmlElement)doc.SelectSingleNode(xPathExpression);

                            if (element != null)
                            {
                                IsVisibleFilter = Convert.ToBoolean(element.Attributes["Visible"].Value);
                                SelectedIds = element.Attributes["SelectedValue"].Value;
                                SelectedTexts = element.Attributes["SelectedText"].Value;
                            }
                        }                       

                        //Maintain selections                        
                        HiddenField HdnSingle = (HiddenField)this.FindControl("hdnSingleTaxonomy" + TaxonomyTypeID + "Selection");
                        HiddenField HdnMultiple = (HiddenField)this.FindControl("hdnMultipleTaxonomy" + TaxonomyTypeID + "Selection");

                        HiddenField HdnSingleText = (HiddenField)this.FindControl("hdnSingleTaxonomy" + TaxonomyTypeID + "SelectionText");
                        HiddenField HdnMultipleText = (HiddenField)this.FindControl("hdnMultipleTaxonomy" + TaxonomyTypeID + "SelectionText");

                        if (!IsVisibleFilter)
                        {
                            HdnSingle.Value = SelectedIds;
                            HdnMultiple.Value = SelectedIds;
                            HdnSingleText.Value = SelectedTexts;
                            HdnMultipleText.Value = SelectedTexts;
                        }
                        else
                        {
                            //loading hidden fields data with its state maintained through callback
                            if (!string.IsNullOrEmpty(_filterData) && !string.IsNullOrEmpty(_filterDataText))
                            {

                                string[] filter;
                                filter = _filterData.TrimEnd(';').Split(';');
                                string[] filterText;
                                filterText = _filterDataText.TrimEnd(';').Split(';');

                                for (int i = 0; i < filter.Length; i++)
                                {
                                    if (filter[i].Split('|')[0].StartsWith(TaxonomyTypeID + "|"))
                                    {
                                        HdnSingle.Value = filter[i].Split('|')[1];
                                        HdnMultiple.Value = filter[i].Split('|')[2];

                                        HdnSingleText.Value = filterText[i].Split('|')[1];
                                        HdnMultipleText.Value = filterText[i].Split('|')[2];
                                    }
                                }                       
                            }
                            //build single tree
                            buildTree(Convert.ToInt32(TaxonomyTypeID),
                                SingleFilterTree,
                                TaxonomyDatSet,
                                false);
                            //build multiple tree
                            buildTree(Convert.ToInt32(TaxonomyTypeID),
                                MultipleFilterTree,
                                TaxonomyDatSet,
                                true);

                            CArt.ComboBox SingleFilterDropdown = (CArt.ComboBox)this.FindControl("SingleFilterBox" + TaxonomyTypeID);
                            CArt.ComboBox MultipleFilterDropdown = (CArt.ComboBox)this.FindControl("MultipleFilterBox" + TaxonomyTypeID);

                            if (string.IsNullOrEmpty(HdnSingle.Value))
                            {
                                bool IsNodeSelected = false;

                                //Check chart wizard selections are available or not
                                if (!string.IsNullOrEmpty(ChartWizardIndicators))
                                {
                                    //If chart wizard selected indicators found then select one fo them as default selection
                                    string[] ChartWizardIndicatorArray = ChartWizardIndicators.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string IndicatorID in ChartWizardIndicatorArray)
                                    {
                                        if (SingleFilterTree.FindNodeById(IndicatorID) != null)
                                        {
                                            SingleFilterTree.SelectedNode = SingleFilterTree.FindNodeById(IndicatorID);
                                            SingleFilterDropdown.Text = SingleFilterTree.SelectedNode.Text;
                                            HdnSingle.Value = SingleFilterTree.SelectedNode.ID;
                                            HdnSingleText.Value = SingleFilterTree.SelectedNode.Text;
                                            IsNodeSelected = true;
                                            break;
                                        }
                                    }
                                }

                                //Check node selection is done or not
                                if (!IsNodeSelected)
                                {
                                    //Default selections - Single filter 
                                    SelectPrioritiesForSingleFilterCombo(SingleFilterDropdown,
                                        SingleFilterTree,
                                        HdnSingle,
                                        HdnSingleText,
                                        TaxonomyDatSet);
                                }
                            }
                            else
                            {
                                if (SingleFilterTree.FindNodeById(HdnSingle.Value) != null)
                                {
                                    SingleFilterTree.SelectedNode = SingleFilterTree.FindNodeById(HdnSingle.Value);
                                    SingleFilterDropdown.Text = SingleFilterTree.SelectedNode.Text;
                                }
                            }
                            if (string.IsNullOrEmpty(HdnMultiple.Value))
                            {
                                //Default selections - Multiple filter
                                bool CheckAll = false;

                                //For heatmap country multiple dropdown all checkboxes should be checked bydefault
                                if (this.Page.Request.Url.ToString().ToLower().Contains("heatmap") &&
                                    TaxonomyTypeID=="1")
                                {   
                                    CheckAll = true;
                                }

                                bool IsNodeChecked = false;

                                //Check chart wizard selections are available or not
                                if (!CheckAll && !string.IsNullOrEmpty(ChartWizardIndicators))
                                {
                                    //If chart wizard selected indicators found then select one fo them as default selection
                                    string[] ChartWizardIndicatorArray = ChartWizardIndicators.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                                    foreach (string IndicatorID in ChartWizardIndicatorArray)
                                    {
                                        ComponentArt.Web.UI.TreeViewNode CurrentNode = MultipleFilterTree.FindNodeById(IndicatorID);
                                        if (CurrentNode != null)
                                        {
                                            CurrentNode.Checked = true;
                                            MultipleFilterDropdown.Text += CurrentNode.Text + ";";
                                            HdnMultiple.Value += CurrentNode.ID+",";
                                            HdnMultipleText.Value = CurrentNode.Text + ",";
                                            IsNodeChecked = true;
                                        }
                                    }
                                }

                                //Check node selection is done or not
                                if (!IsNodeChecked)
                                {
                                    //Selecting defaults
                                    SelectPrioritiesForMultipleFilterCombo(MultipleFilterDropdown,
                                        MultipleFilterTree,
                                        HdnMultiple,
                                        HdnMultipleText,
                                        CheckAll,
                                        TaxonomyDatSet);
                                }

                            }
                            else
                            {
                                CArt.TreeViewNode[] CheckedNodes = MultipleFilterTree.CheckedNodes;
                                MultipleFilterDropdown.Text = "";
                                //HdnMultiple.Value = "";
                                //HdnMultipleText.Value = "";
                                foreach (CArt.TreeViewNode CheckedNode in CheckedNodes)
                                {
                                    MultipleFilterDropdown.Text += CheckedNode.Text + ";";
                                   // HdnMultiple.Value += CheckedNode.Value + ",";
                                    //HdnMultiple.Value += CheckedNode.ID + ",";
                                    //HdnMultipleText.Value += CheckedNode.Value + ",";
                                }
                                MultipleFilterDropdown.Text = MultipleFilterDropdown.Text.TrimEnd(new char[] { ';' });
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="multipleFilterDropdown"></param>
        /// <param name="multipleFilterTree"></param>
        /// <param name="hdnMultiple"></param>
        private void SelectPrioritiesForMultipleFilterCombo(CArt.ComboBox multipleFilterDropdown,
            CArt.TreeView multipleFilterTree,
            HiddenField hdnMultiple,
            HiddenField hdnMultipleText,
            bool selectAll,
            DataSet TaxonomyDatSet)
        {
            int DefaultSelectionsCount = 5;
            multipleFilterDropdown.Text = "";
            hdnMultiple.Value = "";
            if (hdnMultipleText != null)
            {
                hdnMultipleText.Value = "";
            }
            foreach (CArt.TreeViewNode tnode in multipleFilterTree.Nodes)
            {
                CheckOnSubNodes(tnode,
                    ref DefaultSelectionsCount,
                    multipleFilterDropdown,
                    hdnMultiple,
                    hdnMultipleText,
                    TaxonomyDatSet,
                    selectAll);

                if (DefaultSelectionsCount <= 0 && !selectAll) break;
                if (TaxonomyDatSet.Tables[1].Select("ID='" + tnode.ID + "'").Length > 0 || selectAll)
                {
                    multipleFilterDropdown.Text += tnode.Text + ";";
                    //hdnMultiple.Value += tnode.Value + ",";
                    hdnMultiple.Value += tnode.ID + ",";
                    if (hdnMultipleText != null)
                    {
                        //hdnMultipleText.Value += tnode.Text + ",";
                        hdnMultipleText.Value += tnode.Value + ",";
                    }
                    tnode.Checked = true;
                    DefaultSelectionsCount--;
                }
            }
            multipleFilterDropdown.Text = multipleFilterDropdown.Text.TrimEnd(new char[] { ';' });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="multipleFilterDropdown"></param>
        /// <param name="multipleFilterTree"></param>
        /// <param name="hdnMultiple"></param>
        private void SelectDefaultsForMultipleFilterCombo(CArt.ComboBox multipleFilterDropdown,
            CArt.TreeView multipleFilterTree,
            HiddenField hdnMultiple,
            HiddenField hdnMultipleText,
            bool selectAll)
        {
            int DefaultSelectionsCount = 5;
            multipleFilterDropdown.Text = "";
            hdnMultiple.Value = "";
            if (hdnMultipleText != null)
            {
                hdnMultipleText.Value = "";
            }
            foreach (CArt.TreeViewNode tnode in multipleFilterTree.Nodes)
            {
                CheckOnSubNodes(tnode,
                    ref DefaultSelectionsCount,
                    multipleFilterDropdown,
                    hdnMultiple,
                    hdnMultipleText);

                if (DefaultSelectionsCount <= 0 && !selectAll) break;
                if (tnode.Nodes.Count == 0 && tnode.ShowCheckBox)
                {
                    multipleFilterDropdown.Text += tnode.Text + ";";
                    //hdnMultiple.Value += tnode.Value + ",";
                    hdnMultiple.Value += tnode.ID + ",";
                    if (hdnMultipleText != null)
                    {
                        //hdnMultipleText.Value += tnode.Text + ",";
                        hdnMultipleText.Value += tnode.Value + ",";
                    }
                    tnode.Checked = true;
                    DefaultSelectionsCount--;
                }
            }
            multipleFilterDropdown.Text = multipleFilterDropdown.Text.TrimEnd(new char[] { ';' });            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="singleFilterDropdown"></param>
        /// <param name="singleFilterTree"></param>
        /// <param name="hdnSingle"></param>
        private void SelectDefaultsForSingleFilterCombo(CArt.ComboBox singleFilterDropdown,
            CArt.TreeView singleFilterTree,
            HiddenField hdnSingle,
            HiddenField hdnSingleText)
        {            
            CArt.TreeViewNode LeafNode = null;
            foreach (CArt.TreeViewNode tnode in singleFilterTree.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    LeafNode = GetFirstLeafNode(tnode);
                    break;
                }
                else
                {
                    LeafNode = tnode;
                    break;
                }
            }
            if (LeafNode != null)
            {
                singleFilterDropdown.Text = LeafNode.Text;
                //hdnSingle.Value = LeafNode.Value;
                hdnSingle.Value = LeafNode.ID;
                if (hdnSingleText != null)
                {
                    //hdnSingleText.Value = LeafNode.Text;
                    hdnSingleText.Value = LeafNode.Value;
                }
                //LeafNode.CssClass = "comboItemHover";
                singleFilterTree.SelectedNode = LeafNode;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="singleFilterDropdown"></param>
        /// <param name="singleFilterTree"></param>
        /// <param name="hdnSingle"></param>
        private void SelectPrioritiesForSingleFilterCombo(CArt.ComboBox singleFilterDropdown,
            CArt.TreeView singleFilterTree,
            HiddenField hdnSingle,
            HiddenField hdnSingleText,
            DataSet TaxonomyDatSet)
        {
            CArt.TreeViewNode LeafNode = null;
            // if (TaxonomyDatSet.Tables[1].Select("ID='" + tnode.ID + "'").Length > 0)
            foreach (DataRow DbRow in TaxonomyDatSet.Tables[1].Rows)
            {
                string ID = DbRow["ID"].ToString();
                LeafNode = singleFilterTree.FindNodeById(ID);
                if (LeafNode != null) break;
            }
            if (LeafNode == null)
            {
                foreach (CArt.TreeViewNode tnode in singleFilterTree.Nodes)
                {
                    if (tnode.Nodes.Count > 0)
                    {
                        LeafNode = GetFirstLeafNode(tnode);
                        break;
                    }
                    else
                    {
                        LeafNode = tnode;
                        break;
                    }
                }
            }            
            singleFilterDropdown.Text = LeafNode.Text;
            //hdnSingle.Value = LeafNode.Value;
            hdnSingle.Value = LeafNode.ID;
            if (hdnSingleText != null)
            {
                //hdnSingleText.Value = LeafNode.Text;
                hdnSingleText.Value = LeafNode.Value;
            }
            //LeafNode.CssClass = "comboItemHover";
            singleFilterTree.SelectedNode = LeafNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <returns></returns>
        private CArt.TreeViewNode GetFirstLeafNode(CArt.TreeViewNode parentNode)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    return GetFirstLeafNode(tnode);                   
                }
                else
                {
                    return tnode;
                }
            }
            return parentNode;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="defaultSelectionsCount"></param>
        /// <param name="multipleFilterDropdown"></param>
        /// <param name="hdnMultiple"></param>
        private void CheckOnSubNodes(CArt.TreeViewNode parentNode, 
            ref int defaultSelectionsCount,
            CArt.ComboBox multipleFilterDropdown, 
            HiddenField hdnMultiple,
            HiddenField hdnMultipleText,
            DataSet TaxonomyDatSet,
            bool SelectAll)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                CheckOnSubNodes(tnode,
                    ref defaultSelectionsCount, 
                    multipleFilterDropdown,
                    hdnMultiple,
                    hdnMultipleText,
                    TaxonomyDatSet,
                    SelectAll);

                if (defaultSelectionsCount <= 0 && !SelectAll) break;
                if (TaxonomyDatSet.Tables[1].Select("ID='" + tnode.ID + "'").Length > 0 || SelectAll)
                {
                    multipleFilterDropdown.Text += tnode.Text + ";";
                    //hdnMultiple.Value += tnode.Value + ",";
                    //hdnMultipleText.Value += tnode.Text + ",";
                    hdnMultiple.Value += tnode.ID + ",";
                    hdnMultipleText.Value += tnode.Value + ",";
                    tnode.Checked = true;
                    defaultSelectionsCount--;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="parentNode"></param>
        /// <param name="defaultSelectionsCount"></param>
        /// <param name="multipleFilterDropdown"></param>
        /// <param name="hdnMultiple"></param>
        private void CheckOnSubNodes(CArt.TreeViewNode parentNode,
            ref int defaultSelectionsCount,
            CArt.ComboBox multipleFilterDropdown,
            HiddenField hdnMultiple,
            HiddenField hdnMultipleText)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                CheckOnSubNodes(tnode,
                    ref defaultSelectionsCount,
                    multipleFilterDropdown,
                    hdnMultiple,
                    hdnMultipleText);

                if (defaultSelectionsCount <= 0) break;
                if (tnode.Nodes.Count == 0 && tnode.ShowCheckBox)
                {
                    multipleFilterDropdown.Text += tnode.Text + ";";
                    //hdnMultiple.Value += tnode.Value + ",";
                    //hdnMultipleText.Value += tnode.Text + ",";
                    hdnMultiple.Value += tnode.ID + ",";
                    hdnMultipleText.Value += tnode.Value + ",";
                    tnode.Checked = true;
                    defaultSelectionsCount--;
                }
            }
        }

      
      
        /// <summary>
        /// Gets the data filter XML.
        /// </summary>
        /// <returns></returns>
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {

                    Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());

                    string SelectedTaxonomyIDs = "";
                    string SelectedParentTaxonomyIDs = "";

                    //Check if any selections are found for the current taxonomytypeid
                    if (selectedIDsList != null && selectedIDsList.Count > 0)
                    {
                        //convert int array to string array
                        foreach (KeyValuePair<int, string> selectedID in selectedIDsList)
                        {
                            if (selectedID.Value.Contains("(All)"))
                            {
                                SelectedParentTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                            else
                            {
                                SelectedTaxonomyIDs += selectedID.Key.ToString() + ",";
                            }
                        }
                    }
                    //If request came from chart wizard and found any SelectedIndicators then add those selections to SelectionsXML
                    if (taxonomyRow["ID"].ToString() == "2" &&
                           GlobalSettings.ShowChartWizard &&
                           Request.QueryString["source"] != null &&
                           Request.QueryString["source"].ToString().Equals("wizard") &&
                           Session["SelectedIndicators"] != null &&
                           Session["SelectedIndicators"].ToString() != "")
                    {
                        SelectedTaxonomyIDs += Session["SelectedIndicators"].ToString();
                    }
                    //check any SelectedTaxonomyIDs are tehre or not
                    if (SelectedTaxonomyIDs.Length > 0 || SelectedParentTaxonomyIDs.Length > 0)
                    {
                        //Build selections xml - when SelectedTaxonomyIDs are found for the current taxonomytypeid
                        SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' parentSelectionIDs='{2}' columnID='{3}' />",
                            taxonomyRow["ID"].ToString(),
                            SelectedTaxonomyIDs.TrimEnd(','),
                            SelectedParentTaxonomyIDs.TrimEnd(','),
                            taxonomyRow["ColumnRowID"].ToString());
                    }

                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        /// <summary>
        /// Builds the tree.
        /// </summary>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="treeView">The tree view.</param>
        /// <param name="taxonomyDatSet">The taxonomy dat set.</param>
        /// <param name="isMultipleFilter">Is multiple filter.</param>
        private void buildTree(int taxonomyID,
            CArt.TreeView treeView,
            DataSet taxonomyDatSet,
            bool isMultipleFilter)
        {
            string Prefix = isMultipleFilter ? "Multiple" : "Single";

            if (taxonomyDatSet.Tables.Count > 0 && this.FindControl("hdn"+Prefix + "Taxonomy" + taxonomyID + "Selection") != null)
            {
                string TaxonomySelectedValue = ((HiddenField)this.FindControl("hdn" + Prefix + "Taxonomy" + taxonomyID + "Selection")).Value;
                //Filter rows of the given taxonomy type
                DataRow[] rows = taxonomyDatSet.Tables[0].Select("taxonomyTypeID=" + taxonomyID);
                foreach (DataRow dbRow in rows)
                {
                    if (dbRow.IsNull("parentTaxonomyID"))
                    {
                        TaxonomySelectedValue=TaxonomySelectedValue.StartsWith(",") ? TaxonomySelectedValue : "," + TaxonomySelectedValue;
                        TaxonomySelectedValue = TaxonomySelectedValue.EndsWith(",") ? TaxonomySelectedValue : TaxonomySelectedValue + ",";

                        ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["Name"].ToString(),
                            dbRow["ID"].ToString(),
                            dbRow["displayName"].ToString(),
                            true,
                            isMultipleFilter && Convert.ToBoolean(dbRow["isLeafNode"].ToString()),
                            TaxonomySelectedValue.Contains(","+dbRow["ID"].ToString() + ","));

                        treeView.Nodes.Add(newNode);

                        PopulateSubTree(dbRow,
                            newNode,
                            isMultipleFilter,
                            TaxonomySelectedValue);
                        
                        //if (isMultipleFilter)
                        //{
                        //    newNode.ShowCheckBox = (newNode.Nodes.Count == 0) ? true : false;
                        //}
                    }
                }
            }
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The db row.</param>
        /// <param name="node">The node.</param>
        /// <param name="isMultipleFilter">Is multiple filter.</param>
        /// <param name="taxonomySelectedValue">The taxonomy selected value.</param>
        private void PopulateSubTree(DataRow dbRow,
            CArt.TreeViewNode node,
            bool isMultipleFilter,
            string taxonomySelectedValue)
        {
            taxonomySelectedValue = taxonomySelectedValue.StartsWith(",") ? taxonomySelectedValue : "," + taxonomySelectedValue;
            taxonomySelectedValue = taxonomySelectedValue.EndsWith(",") ? taxonomySelectedValue : taxonomySelectedValue + ",";

            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Name"].ToString(),
                    childRow["ID"].ToString(),
                    childRow["displayName"].ToString(),
                    true,
                    isMultipleFilter && Convert.ToBoolean(childRow["isLeafNode"].ToString()),
                    taxonomySelectedValue.Contains(","+childRow["ID"].ToString() + ","));

                node.Nodes.Add(childNode);

                PopulateSubTree(childRow,
                    childNode,
                    isMultipleFilter,
                    taxonomySelectedValue);
                
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hassCheckbox">Has checkbox</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text,
            string value,
            string displayText,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            //node.Value = value;
            node.ID = value;
            node.Value = displayText;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        /// <summary>
        /// 
        /// </summary>
        private void BindYears()
        {
            bool IsVisibleFilter = true;
            if (!string.IsNullOrEmpty(FilterOptionsXML))
            {
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(FilterOptionsXML);

                string xPathExpression = "//FilterOptions/TaxonomyType[@ID='999']";
                XmlElement element = (XmlElement)doc.SelectSingleNode(xPathExpression);

                if (element != null)
                {
                    IsVisibleFilter = Convert.ToBoolean(element.Attributes["Visible"].Value);
                    string SelectedIds = element.Attributes["SelectedValue"].Value;
                    hdnMultipleTaxonomy999Selection.Value = SelectedIds;
                    hdnSingleTaxonomy999Selection.Value = SelectedIds;
                    SingleFilterBox999.Visible = IsVisibleFilter;
                    lblYears.Visible = IsVisibleFilter;
                    MultipleFilterBox999.Visible = IsVisibleFilter;
                }
            }
            if(IsVisibleFilter)
            {
                //Get list of valid years from session
                int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                //get Min & Max years based on selection
                string FilterXml = GetDataFilterXML();
                DataSet Years = SqlDataService.GetMinMaxYears(FilterXml);
                if (Years != null &&
                    Years.Tables.Count > 0 &&
                    Years.Tables[0].Rows.Count > 0)
                {
                    if(!string.IsNullOrEmpty(Years.Tables[0].Rows[0][0].ToString()) &&
                       !string.IsNullOrEmpty(Years.Tables[0].Rows[0][1].ToString()))  
                    {
                        int MinYear = Convert.ToInt32(Years.Tables[0].Rows[0]["MinYear"].ToString());
                        int MaxYear = Convert.ToInt32(Years.Tables[0].Rows[0]["MaxYear"].ToString());
                        //compare min & max year with start & end years and take the intersection of them
                        StartYear = MinYear > StartYear ? MinYear : StartYear;
                        EndYear = MaxYear < EndYear ? MaxYear : EndYear;
                    }                    
                }

                string SelectedValue = StartYear.ToString();
                while (StartYear < EndYear)
                {
                    StartYear++;
                    SelectedValue += "," + StartYear;
                }

                string[] ValidYears = SelectedValue.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                DataTable YearsTable = new DataTable();
                YearsTable.Columns.Add("ID");
                YearsTable.Columns.Add("Name");
                foreach (string year in ValidYears)
                {
                    YearsTable.Rows.Add(year, year);
                }

                //string TaxonomySelectedValue = ((HiddenField)this.FindControl("hdn" + Prefix + "Taxonomy" + taxonomyID + "Selection")).Value;
                foreach (DataRow dbRow in YearsTable.Rows)
                {
                    //add to singles treeview
                    ComponentArt.Web.UI.TreeViewNode newNodeForSingle = CreateNode(dbRow["Name"].ToString(),
                        dbRow["ID"].ToString(),
                        dbRow["Name"].ToString(),
                        true,
                        false,
                        false);

                    SingleFilterTaxonomyView999.Nodes.Add(newNodeForSingle);

                    //add to multiple treeview
                    string TaxonomySelectedValue = hdnMultipleTaxonomy999Selection.Value;
                    TaxonomySelectedValue = TaxonomySelectedValue.StartsWith(",") ? TaxonomySelectedValue : "," + TaxonomySelectedValue;
                    TaxonomySelectedValue = TaxonomySelectedValue.EndsWith(",") ? TaxonomySelectedValue : TaxonomySelectedValue + ",";

                    ComponentArt.Web.UI.TreeViewNode newNodeForMultiple = CreateNode(dbRow["Name"].ToString(),
                        dbRow["ID"].ToString(),
                        dbRow["Name"].ToString(),
                        true,
                        true,
                        TaxonomySelectedValue.Contains(","+dbRow["ID"].ToString() + ","));

                    MultipleFilterTaxonomyView999.Nodes.Add(newNodeForMultiple);
                }


                #region default selections
                    if (string.IsNullOrEmpty(hdnSingleTaxonomy999Selection.Value))
                    {
                        //if current year is found in the year list then default will be current year only
                        //other wise first leaf node will be default
                        ComponentArt.Web.UI.TreeViewNode CurrentYearNode=SingleFilterTaxonomyView999.FindNodeById(DateTime.Now.Year.ToString());
                        if (CurrentYearNode != null)
                        {                            
                            SingleFilterTaxonomyView999.SelectedNode = SingleFilterTaxonomyView999.FindNodeById(DateTime.Now.Year.ToString());
                            hdnSingleTaxonomy999Selection.Value = DateTime.Now.Year.ToString();
                            SingleFilterBox999.Text = DateTime.Now.Year.ToString();
                        }
                        else
                        {
                            SelectDefaultsForSingleFilterCombo(SingleFilterBox999,
                                SingleFilterTaxonomyView999,
                                hdnSingleTaxonomy999Selection,
                                null);
                        }
                        
                    }
                    else
                    {                        
                        SingleFilterTaxonomyView999.SelectedNode = SingleFilterTaxonomyView999.FindNodeById(hdnSingleTaxonomy999Selection.Value);
                        if (SingleFilterTaxonomyView999.SelectedNode == null)
                        {
                            SelectDefaultsForSingleFilterCombo(SingleFilterBox999,
                                SingleFilterTaxonomyView999,
                                hdnSingleTaxonomy999Selection,
                                null);
                        }
                        else
                        {
                            SingleFilterBox999.Text = SingleFilterTaxonomyView999.SelectedNode.Text ;
                        }
                    }

                    if (string.IsNullOrEmpty(hdnMultipleTaxonomy999Selection.Value))
                    {   
                       //for years by default all will be selected in multiple dropdown year control
                        SelectDefaultsForMultipleFilterCombo(MultipleFilterBox999,
                            MultipleFilterTaxonomyView999,
                            hdnMultipleTaxonomy999Selection,
                            null,
                            true);
                  
                    }
                    else
                    {
                        CArt.TreeViewNode[] CheckedNodes = MultipleFilterTaxonomyView999.CheckedNodes;
                        MultipleFilterBox999.Text = "";
                        hdnMultipleTaxonomy999Selection.Value = "";
                        foreach (CArt.TreeViewNode CheckedNode in CheckedNodes)
                        {
                            MultipleFilterBox999.Text += CheckedNode.Text + ";";
                            hdnMultipleTaxonomy999Selection.Value += CheckedNode.Value + ",";
                        }
                        MultipleFilterBox999.Text = MultipleFilterBox999.Text.TrimEnd(new char[] { ';' });
                    }
                #endregion
            }
        }
    }
}
<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="SearchOptions.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Controls.SearchOptions" %>
<div id="SearchOtionsDiv" runat="server">
    <div id="database_tabs_bar">
        <ul>
            <li class="title">Search Options</li>  
            <li id="QuickSearchLI" runat="server">
                <asp:LinkButton ID="QuickSearchLink" runat="server" 
                    OnClick="QuickSearch_Click" 
                    Text="Quick Search"
                    ToolTip="Quick Search"></asp:LinkButton>
                
            </li>           
            <li id="SearchLI" runat="server">
                <asp:LinkButton ID="SearchLink" runat="server" 
                    OnClick="Search_Click" 
                    Text="Detailed Search"
                    ToolTip="Detailed Search"></asp:LinkButton>
                
            </li>            
           
        </ul>
    </div>
    <br /><br />
</div>
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class ChartLinks : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                DataSet ChartsData = SqlDataService.GetPredefinedChartList();
                if (ChartsData != null)
                {
                    if (ChartsData.Tables.Count > 1)
                    {
                        ChartsData.Relations.Add("ChartLinks",
                               ChartsData.Tables[0].Columns["GroupName"],
                               ChartsData.Tables[1].Columns["GroupName"]);

                        GroupNamesRepeater.DataSource = ChartsData;
                        GroupNamesRepeater.DataBind();
                        GroupNamesRepeater.Visible = ChartsData.Tables.Count > 0 && ChartsData.Tables[0].Rows.Count > 0;
                        ChartsRepeater.Visible = false;
                    }
                    else if (ChartsData.Tables.Count > 0)
                    {
                        ChartsRepeater.DataSource = ChartsData;
                        ChartsRepeater.DataBind();
                        GroupNamesRepeater.Visible = false;
                    }
                }
                ((Page)this.Page).ClientScript.RegisterStartupScript(this.GetType(), "togglescript", "toggleOnPageLoad();", true);
            }
            
        }

        public string GetLinkStyle(string page)
        {
            string ClassName = "none";
            string CurentPage = Request.Url.Segments[Request.Url.Segments.Length - 1];
            CurentPage = string.IsNullOrEmpty(Request.QueryString["id"]) ?
                 CurentPage:
                 string.Format("{0}?id={1}", CurentPage, Request.QueryString["id"]);

            if (CurentPage.ToLower().Equals(page.ToLower()))
            {
                ClassName = "selected";
            }

            if (page.ToLower() == "chartwizard.aspx" && !GlobalSettings.ShowChartWizard)
            {
                //Show / Hide chartwizard page link based on config
                ClassName = "hiddenLI";
            }

            return ClassName;

        }

        protected void GroupNamesRepeater_ItemDataBound(object sender, 
                                System.Web.UI.WebControls.RepeaterItemEventArgs e)
        { DataRowView drv = (DataRowView)e.Item.DataItem;
                RepeaterItem item = e.Item;
            
            if ((item.ItemType == ListItemType.Item) || (item.ItemType == ListItemType.AlternatingItem))
            {
               
                Repeater ChartLinkRepeater = (Repeater)e.Item.FindControl("ChartNamesRepeater");
                string groupName = drv.Row["GroupName"].ToString();
                if (((DataRowView)e.Item.DataItem).Row.GetChildRows("ChartLinks").Length > 0)
                {
                    DataView SectionsDataView = ((DataRowView)e.Item.DataItem).Row.GetChildRows("ChartLinks")[0].Table.DefaultView;
                    SectionsDataView.RowFilter = "GroupName like '" + groupName + "'";
                    DataTable sectionsDataTable = SectionsDataView.ToTable();
                    ChartLinkRepeater.DataSource = sectionsDataTable;
                    ChartLinkRepeater.DataBind();
                }
            }
        }
    }
}
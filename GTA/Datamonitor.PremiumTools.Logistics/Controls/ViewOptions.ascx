<%@ Control Language="C#" 
    AutoEventWireup="true" 
    CodeBehind="ViewOptions.ascx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Controls.ViewOptions" %>

    <div id="database_tabs_bar">
        <ul>
            <li class="title">View Options</li>
            <li id="View1LI" runat="server">
                <%--<asp:Image id="outlineViewImage" runat="server" ImageUrl="~/Assets/Images/outline.gif"                 />--%>
                <asp:HyperLink ID="View1Link" runat="server" NavigateUrl="#" ToolTip="Displays data in hierarchical outline." >Grid View</asp:HyperLink>
            </li>
            <li id="View2LI" runat="server">
                <asp:HyperLink ID="View2Link" runat="server" NavigateUrl="#" ToolTip="Displays data in flat list."  >Flat View</asp:HyperLink>
            </li>
            <li id="CountryComparisonLI" runat="server">
                <asp:HyperLink ID="CountryComparisonLink" runat="server" NavigateUrl="#" ToolTip="Click to compare countries data">Country Comparison</asp:HyperLink>
            </li>           
        </ul>
    </div>
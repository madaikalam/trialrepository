using System;
using System.Web;
using System.Web.UI;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Logistics.Controls
{
    public partial class Glossary : System.Web.UI.UserControl
    {
        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            divNotes.Visible = GlobalSettings.NotesLink.Length > 0;
            notesLink.NavigateUrl = GlobalSettings.NotesLink;

            //Methodology link
            divMethodology.Visible = GlobalSettings.MethodologyLink.Length > 0;
            MethodologyLink.NavigateUrl = GlobalSettings.MethodologyLink;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings.Get("ShowMDALink") != null &&
                ConfigurationManager.AppSettings.Get("ShowMDALink").Equals("true", StringComparison.CurrentCultureIgnoreCase))
            {
                divMDA.Visible = true;
            }
        }

        protected void MDALink_Click(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings.Get("MDAlink") != null && Session["SourceKC"] != null)
            {
                string AllowedKCs = ConfigurationManager.AppSettings.Get("MDAAllowedKCS");
                string CurrentKC = Session["SourceKC"].ToString();

                if (!AllowedKCs.Contains(CurrentKC))
                {
                    Response.Redirect("~/error/default.aspx?code=MDA", true);
                }
                else
                {
                    string url = ConfigurationManager.AppSettings.Get("MDAlink");
                    url = url.Replace("currentKC", CurrentKC);
                    Response.Redirect(url, true);
                }

            }
        }
       
    }
}
using System;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Generic.Controls
{
    public partial class SiteTitle : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                TitleLabel.Text = GlobalSettings.SiteTitle;
            }
        }
    }
}
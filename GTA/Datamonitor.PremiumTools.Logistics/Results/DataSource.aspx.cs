using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Results
{
    public partial class DataSource : Page
    {
        private string _lastUpdatedDate;
        private string _sourceInfo;

        public string LastUpdatedDate
        {
            get { return _lastUpdatedDate; }
            set { _lastUpdatedDate = value; }
        }

        public string SourceInfo
        {
            get { return _sourceInfo; }
            set { _sourceInfo = value; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            LastUpdatedDate="";
            SourceInfo="";
            SourceDiv.Visible = false;
            DateDiv.Visible = false;

            if (!string.IsNullOrEmpty(Request.QueryString["rowid"]))
            {
                //Get source information from database for the rowid
                DataSet SourceInfoData = SqlDataService.GetDataSourceInformation(Convert.ToInt32(Request.QueryString["rowid"].ToString().Trim()));
                                
                if (SourceInfoData != null &&
                    SourceInfoData.Tables.Count > 0 &&
                    SourceInfoData.Tables[0].Rows.Count > 0)
                {
                    //Bind the source and last updated date values
                    if (!string.IsNullOrEmpty(SourceInfoData.Tables[0].Rows[0]["LastUpdatedDate"].ToString()))
                    {
                        LastUpdatedDate = Convert.ToDateTime(SourceInfoData.Tables[0].Rows[0]["LastUpdatedDate"].ToString().Trim()).ToString("MMM yyyy");
                        DateDiv.Visible = true;
                    }
                    if (!string.IsNullOrEmpty(SourceInfoData.Tables[0].Rows[0]["Source"].ToString()))
                    {
                        SourceInfo = SourceInfoData.Tables[0].Rows[0]["Source"].ToString();
                        SourceDiv.Visible = true;
                    }
                }
            }
        }

    }
}

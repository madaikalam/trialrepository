<%@ Page Language="C#" MasterPageFile="~/MasterPages/ResultsMaster.Master"  AutoEventWireup="true"
    Codebehind="CountryComparisonGrid.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.CountryComparisonGrid"
    Theme="NormalTree" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ResultsMasterHolder" runat="server">
    <link href="../assets/css/arcticwhite.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/gridstyle.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../assets/Scripts/NumberFormat154.js"></script>
    
    <style type="text/css">
        .rightdiv
        {
            float:right;
            margin-bottom:4px;
        }
        .leftdiv
        {
            float:left;
            width:60px;
        }
        input[type=checkbox] 
        {
	        width:5px;
	        margin-left:-65px;
	        margin-right:-65px;
	        padding:0px;
	        text-align:left;
        }
        
        .ResultsPanel
        {
            float:left;
            
        }
    </style>

    <script type="text/javascript">
        function formatN(value)
        {
            //return value;
            if(value==null)
            return "";
            //for growth rows, data will come in the format xx.xx% - so format the number part
            var postfix='';
            if(String(value).indexOf('%')!=-1)
            {
                value=String(value).substring(0,value.length-1);
                postfix='%';
            }
            if(value != "0" && value == "")
    	    {
       	     return "";
    	    }
            var num = new NumberFormat();
            num.setInputDecimal('.');
            num.setNumber(value); // obj.value is '-1000.247'
            num.setPlaces('1', false);
            num.setCurrency(false);
            num.setNegativeFormat(num.LEFT_DASH);
            num.setNegativeRed(false);
            num.setSeparators(true, ',', ',');
            return num.toFormatted()+postfix;

        }
        function getSelectedCountries()
        {
            return $("#<%= hdnCountry.ClientID%>").val();
        }
        
        function getYears()
        {
            return $("#<%= hdnYears.ClientID%>").val();
        }
        function getSelectedYear()
        {
            return $("#<%= hdnYear.ClientID%>").val();
        }
        function getValueType(dataitem, fieldName)
        {        
            return "Actual";
        }

        function CountryTree_onNodeCheckChange(sender,eventArgs)
        {   
            var hdnControl=document.getElementById("<%= hdnCountry.ClientID%>");    
            var ids=hdnControl.value.split(",");
            var maxLimit=5;
            maxLimit="<%= GetCountryComparisonMaxLimit()%>";
            
            if(eventArgs.get_node().get_checked() && ids.length > maxLimit)
            {
                eventArgs.get_node().set_checked(false);
            }
            else
            {
                OnMultipleTreeCheckChange(eventArgs.get_node(),hdnControl,CountryCombo);
            }
            
        }
        function YearTree_onNodeSelect(sender, eventArgs)
        {       
            YearCombo.set_text(eventArgs.get_node().get_text());
            YearCombo.collapse();
            document.getElementById('<%=hdnYear.ClientID %>').value = eventArgs.get_node().get_value();   
        }
        function OnMultipleTreeCheckChange(checkedNode,hdnControl,filterBox)
        {

            var comboText = filterBox.get_text();
            if (checkedNode.get_checked()) 
            {
                hdnControl.value = hdnControl.value  + checkedNode.get_id()+ ",";
                if(comboText.length>0)
                {
                    comboText +=";"+checkedNode.get_text();
                } 
                else
                {
                    comboText +=checkedNode.get_text();
                } 
                filterBox.set_text(comboText );
            }
            else
            {
                hdnControl.value = hdnControl.value.replace(checkedNode.get_id()+",","");
                if(comboText.length>0)
                {            
                    if(comboText.indexOf(checkedNode.get_text()+";")!=-1)
                    {
                        comboText=comboText.replace(checkedNode.get_text()+";","");
                    }
                    else if(comboText.indexOf(";"+checkedNode.get_text())!=-1)
                    {
                        comboText=comboText.replace(";"+checkedNode.get_text(),"");
                    }            
                    else
                    {
                        comboText=comboText.replace(checkedNode.get_text(),"");
                    }
                    filterBox.set_text(comboText );
                }
            }
        }

        function getAnchorVisibility(val)
         {
             if(val.length>0)
             {     
                return "block";
             }
             else
             {
                return "none";
             }
         }
        function getTaxonomyFilterPage()
        {
            return "<%= getTaxonomyFilterPage()%>";
        }
    function getUniqueID()
     {
        var newDate = new Date;
        return newDate.getTime();  
     }
     function RefreshPage()
    {
       __EVENTTARGET.value = "";
       __EVENTARGUMENT.value = "";

       window.location.reload();
    }
    
     function ChangeDisplayOptionsChangedStatus() 
     {
        var hdnStatus=document.getElementById('<%=isDisplayOptionsChanged.ClientID %>');
        hdnStatus.value="true";     
     }
     function CheckDisplayOptionsChanged()
     {
        var hdnStatus=document.getElementById('<%=isDisplayOptionsChanged.ClientID %>');
        return hdnStatus.value==""?false:true;
     }
    function currency_onChange(dropdown)
    {   
        ChangeDisplayOptionsChangedStatus(); 
        var selection = dropdown.value;
        document.getElementById('<%=hdnConversionID.ClientID %>').value =selection;   
    }
     function TryRenderGrid()
     {
        if(IsIE8Browser())
        {
            ResultsGrid.render();
        }
     }
     function IsIE8Browser() 
     {
         var rv = -1;
         var ua = navigator.userAgent;    
         var re = new RegExp("Trident\/([0-9]{1,}[\.0-9]{0,})");    
         if (re.exec(ua) != null)
          {        
            rv = parseFloat(RegExp.$1);    
          }    
        return (rv == 4);
    }
    </script>     
     
     <div id="DisplayOptionsDiv" runat="server">
        
    <div id="display_options">
        <div style="margin-top:2px;">
            <div class="display_options" >
                <h3>                        
                    Display options 
                </h3>
            </div>
            <div>
              <table width="700">
                    <tr>
                        <td valign="top" style="padding-left:20px;" runat="server" id="DataFormats">                            
                            
                            <table><tr>
                            <td><b>Show data in</b> </td>
                            <td><asp:DropDownList ID="currency" Width="80px" runat="server" Font-Size="11px" /> </td></tr>
                            </table>
                            
                       </td>
                    </tr>
                </table>
            </div>
        </div>
       <div id="display_options_buttons">
            <a href="javascript:toggleLayer('display_options');TryRenderGrid();">Cancel</a>
            <asp:LinkButton ID="Update" runat="server" CssClass="save_button" OnClientClick="JavaScript:toggleLayer('display_options');return CheckDisplayOptionsChanged();"
                OnClick="Update_Click">Update</asp:LinkButton>  
        </div>
    </div>
    

        <div id="display_options_tab" style="margin-top:1px;">
            <a href="javascript:toggleLayer('display_options');TryRenderGrid();">
                <div>
                    Display options
                </div>
            </a>
         </div>     
                     
     </div>             
    <div style="float:left;margin-top:5px;width:100%;position:relative">
        <div style="padding-bottom: 20px;">           
            <div style="width:160px;float:left;padding-top:10px">
             <asp:LinkButton ID="lnkExcel" runat="server" OnClick="lnkExcel_Click"> 
               <img width="12" height="10" border="0" src="../assets/images/tool_excel.gif" />Extract to Excel</asp:LinkButton>
             
            </div>            
            
            <div class="button_right_chart" style="width: 120px;float:right">
                <asp:LinkButton ID="ResultsLink" runat="server" Text="Results" OnClick="ResultsLink_Click">
                </asp:LinkButton>
            </div>
            <div id="YearDiv" runat="server" style="float: right; padding-left: 20px;z-index:9999">
                <table>
                    <tr>
                        <td>
                            Year &nbsp;
                        </td>
                        <td>
                            <ComponentArt:ComboBox ID="YearCombo" runat="server" KeyboardEnabled="false" AutoFilter="false"
                                AutoHighlight="false" AutoComplete="false" CssClass="comboBox" HoverCssClass="comboBoxHover"
                                FocusedCssClass="comboBoxHover" TextBoxCssClass="comboTextBox" DropDownCssClass="comboDropDown"
                                ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover"
                                DropHoverImageUrl="../assets/images/drop_hover.gif" DropImageUrl="../assets/images/drop.gif"
                                Width="120" DropDownHeight="220" DropDownWidth="117">
                                <DropDownContent>
                                    <ComponentArt:TreeView ID="YearTree" Height="220" Width="117" DragAndDropEnabled="false"
                                        NodeEditingEnabled="false" KeyboardEnabled="true" CssClass="TreeView" NodeCssClass="TreeNode"
                                        NodeEditCssClass="NodeEdit" SelectedNodeCssClass="NodeSelected" LineImageWidth="19"
                                        LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0"
                                        NodeLabelPadding="3" ImagesBaseUrl="../assets/images/tvlines/" LineImagesFolderUrl="../assets/images/tvlines/"
                                        ShowLines="true" EnableViewState="false" runat="server">
                                        <ClientEvents>
                                            <NodeSelect EventHandler="YearTree_onNodeSelect" />
                                        </ClientEvents>
                                    </ComponentArt:TreeView>
                                </DropDownContent>
                            </ComponentArt:ComboBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="CountryDiv" runat="server" style="float: right; padding-left: 20px;z-index:9999">
                <table>
                    <tr>
                        <td>
                            Country &nbsp;</td>
                        <td>
                            <ComponentArt:ComboBox ID="CountryCombo" runat="server" KeyboardEnabled="false" AutoFilter="false"
                                AutoHighlight="false" AutoComplete="false" CssClass="comboBox" HoverCssClass="comboBoxHover"
                                FocusedCssClass="comboBoxHover" TextBoxCssClass="comboTextBox" DropDownCssClass="comboDropDown"
                                ItemCssClass="comboItem" ItemHoverCssClass="comboItemHover" SelectedItemCssClass="comboItemHover"
                                DropHoverImageUrl="../assets/images/drop_hover.gif" DropImageUrl="../assets/images/drop.gif"
                                Width="150" DropDownHeight="220" DropDownWidth="147">
                                <DropDownContent>
                                    <ComponentArt:TreeView ID="CountryTree" Height="220" Width="147" DragAndDropEnabled="false"
                                        NodeEditingEnabled="false" KeyboardEnabled="true" CssClass="TreeView" NodeCssClass="TreeNode"
                                        NodeEditCssClass="NodeEdit" SelectedNodeCssClass="NodeSelected" LineImageWidth="19"
                                        LineImageHeight="20" DefaultImageWidth="16" DefaultImageHeight="16" ItemSpacing="0"
                                        NodeLabelPadding="3" ImagesBaseUrl="../assets/images/tvlines/" LineImagesFolderUrl="../assets/images/tvlines/"
                                        ShowLines="true" EnableViewState="false" runat="server">
                                        <ClientEvents>
                                            <NodeCheckChange EventHandler="CountryTree_onNodeCheckChange" />
                                        </ClientEvents>
                                    </ComponentArt:TreeView>
                                </DropDownContent>
                            </ComponentArt:ComboBox>
                        </td>
                        <td><GMPT:CountryFilter id="CountryFilter1" runat="server"></GMPT:CountryFilter></td>
                    </tr>
                </table>
            </div>
            <br />
        </div>
    </div>
                     
            <ComponentArt:Grid ID="ResultsGrid" CssClass="grid" RunningMode="client" ShowHeader="false"
                ShowFooter="false" AllowColumnResizing="false" AllowEditing="false" HeaderCssClass="GridHeader"
                FooterCssClass="GridFooter" SliderHeight="20" SliderWidth="150" SliderGripWidth="9"
                SliderPopupOffsetX="20" IndentCellWidth="22" IndentCellCssClass="HeadingCell_Freeze"
                LoadingPanelClientTemplateId="LoadingFeedbackTemplate" LoadingPanelPosition="MiddleCenter"
                LoadingPanelFadeDuration="10" LoadingPanelFadeMaximumOpacity="60" ScrollBar="Auto"
                ImagesBaseUrl="../assets/images/" PagerStyle="numbered" PagerInfoPosition="BottomLeft"
                PagerPosition="BottomRight" PreExpandOnGroup="true" ScrollTopBottomImagesEnabled="true"
                ScrollTopBottomImageHeight="2" ScrollTopBottomImageWidth="16" ScrollImagesFolderUrl="../assets/images/scroller/"
                ScrollButtonWidth="16" ScrollButtonHeight="17" ScrollBarCssClass="ScrollBar" 
                AutoAdjustPageSize="false" FillContainer="false" Height="400"
                
                ManualPaging="false" ScrollGripCssClass="ScrollGrip" ScrollBarWidth="16" Width="920" runat="server"
                AllowHorizontalScrolling="True" EnableViewState="false">
                <Levels>
                    <ComponentArt:GridLevel AllowReordering="false" AllowGrouping="false" AllowSorting="false"
                        HeadingCellCssClass="HeadingCell" HeadingRowCssClass="HeadingRow" HeadingTextCssClass="HeadingCellText"
                        GroupHeadingCssClass="HeadingCell_Freeze" DataCellCssClass="DataCell" AlternatingRowCssClass="RowAlt"
                        RowCssClass="Row" SelectedRowCssClass="SelRow" SortAscendingImageUrl="asc.gif"
                        SortDescendingImageUrl="desc.gif">
                        <Columns>
                            
                        </Columns>
                    </ComponentArt:GridLevel>
                </Levels>
                <ClientTemplates>
                    <ComponentArt:ClientTemplate ID="CommonDataTemplate">
                        <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##">
                            ## DataItem.GetCurrentMember().get_value() ##</div>                            
                    </ComponentArt:ClientTemplate>
                  <ComponentArt:ClientTemplate Id="FormattedDataTemplate">                                                                           
                    <div class="## getValueType(DataItem, DataItem.GetCurrentMember().Column.DataField) ##"> ## formatN(DataItem.GetCurrentMember().get_value()) ##</div>
                  </ComponentArt:ClientTemplate>                     
                    <ComponentArt:ClientTemplate ID="TooltipTemplate">
                        <div title="## DataItem.GetCurrentMember().get_value() ##">
                            ## DataItem.GetCurrentMember().get_value() ##</div>
                    </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate Id="CommonDataTemplateWithDefinition">                                                                           
                        <div title="## DataItem.GetCurrentMember().get_value() ##"> 
                            <table cellpadding="0" cellspacing="0"><tr><td nowrap="nowrap">
                            ## DataItem.GetCurrentMember().get_value() ##
                            </td><td>
                            
                             <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('Definition').Text)##" 
                                href="../TaxonomyDefinition.aspx?taxonomyid=## DataItem.GetMember('taxonomyid').Text ##" 
                                onclick="return hs.htmlExpand(this, { contentId: 'TaxonomyDefinition', objectType: 'iframe'} )">
                                &nbsp;<img alt="" border="0" src="Assets/Images/help2.gif"  />
                            </a> 
                            </td></tr></table>
                        </div>
                      </ComponentArt:ClientTemplate>
                    <ComponentArt:ClientTemplate ID="ChartTemplate">                    
                        <div style="padding: 0px; font-weight: bold; text-align: center; width:60px;">                        
                            <table cellpadding="0" cellspacing="0" border="0" width="40px"><tr><td style="padding-right:3px">
                            <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('rowID').Text)##" 
                                href="CountryComparisonImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##&countries=## getSelectedCountries() ##&selectedyear=## getSelectedYear() ##&charttype=bar" 
                                onclick="return hs.htmlExpand(this, { contentId: 'CountryComparisonImage', objectType: 'iframe'} )">
                                <img alt="" border="0" src="../assets/images/chart_icon.gif" />
                            </a></td><td style="padding-right:3px">
                            <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('rowID').Text)##" 
                                href="CountryComparisonImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##&countries=## getSelectedCountries() ##&selectedyear=## getSelectedYear() ##&charttype=pie" 
                                onclick="return hs.htmlExpand(this, { contentId: 'CountryComparisonImage', objectType: 'iframe'} )">
                                <img alt="" border="0" src="../assets/images/pie-chart_2.gif" />
                            </a></td><td>
                            <a style="color: Red; text-decoration: none;display:## getAnchorVisibility(DataItem.GetMember('rowID').Text)##" 
                                href="CountryComparisonImage.aspx?rowid=## DataItem.GetMember('rowID').Text ##&countries=## getSelectedCountries() ##&selectedyear=## getYears() ##&charttype=line" 
                                onclick="return hs.htmlExpand(this, { contentId: 'CountryComparisonImage', objectType: 'iframe'} )">
                                <img alt="" border="0" src="../assets/images/linegraph.gif" />
                            </a></td></tr></table>
                        </div>
                        
                    </ComponentArt:ClientTemplate>                    
                    <ComponentArt:ClientTemplate ID="ColumnFilterTemplate">            
                        <div style="padding: 0px; font-weight: bold; text-align: left;overflow:hidden;">
                        <table><tr><td>
                            <a style="color: White; text-decoration: none;">
                                <span>## DataItem.DataField ##</span>
                            </a> </td><td>
                            <a style="color:White;text-decoration: none;text-align: center;" 
                                href="## getTaxonomyFilterPage()##?id=## DataItem.DataField ##&uid=## getUniqueID() ##" 
                                onclick="sortFlag=false;return hs.htmlExpand(this, { contentId: 'ColumnFilterSelections', objectType: 'iframe', preserveContent: false} )">
                               <img alt="Filter ## DataItem.DataField ##" 
                                    border="0" 
                                    src="../assets/images/filter.gif"/>  
                            </a>          </td></tr></table>       
                           
                        </div>
                  </ComponentArt:ClientTemplate>
                </ClientTemplates>
            </ComponentArt:Grid>
            
            <asp:HiddenField ID="hdnCountry" runat="server" />
            <asp:HiddenField ID="hdnYear" runat="server" />
            <asp:HiddenField ID="hdnYears" runat="server" />
            <asp:HiddenField ID="isDisplayOptionsChanged" runat="server" />
            <asp:HiddenField ID="hdnConversionID" runat="server" />

   <div class="highslide-html-content" id="CountryComparisonImage" style="width:350px;height:315px">
        <div class="highslide-header">
            <ul>
                <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                <li class="highslide-close">
                    <asp:LinkButton ID="LinkButton1" runat="server" OnClientClick="hs.close(this);return false;"
                        Text="Close"></asp:LinkButton>
                </li>
            </ul>
            <h1>Country Comparison Chart</h1>
        </div>
        <div class="highslide-body">               
        </div>
    </div>
    <div class="highslide-html-content" id="TaxonomyDefinition" style="width:280px;height:200px">
    <div class="highslide-header">
        <ul>
            <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
            <li class="highslide-close">
                <asp:LinkButton ID="LinkButton4" runat="server" OnClientClick="hs.close(this);return false;"
                    Text="Close"></asp:LinkButton>
            </li>
        </ul>
        <h1>Taxonomy Definition</h1>
    </div>
    <div class="highslide-body" style="padding:0px;margin:0px">
    </div>
</div>
<div class="highslide-html-content" id="ColumnFilterSelections" style="width:460px;height:470px;padding-bottom:35px">
            <div class="highslide-header">
                <ul>
                    <li class="highslide-move"><a href="#" onclick="return false">Move</a> </li>
                    <li class="highslide-close">
                        <asp:LinkButton ID="LinkButton3" runat="server" OnClientClick="hs.close(this);return false;"
                            Text="Close"></asp:LinkButton>
                    </li>
                </ul>
                <h1>Filter Selections</h1>
            </div>
            <div class="highslide-body" style="overflow:hidden;margin-left:10px" >                 
            </div>
            <div class="button_right" style="width:100px;margin-top:0px;margin-right:50px">
                <asp:LinkButton ID="UpdateResults" runat="server" OnClientClick="hs.close(this);RefreshPage();return false;"
                    Text="Update" Width="100px"></asp:LinkButton> 
            </div>     
        </div>
        
 <div style="height:20px"></div>
</asp:Content>

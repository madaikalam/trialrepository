using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Drawing;
using dotnetCHARTING;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using Aspose.Cells;
using Aspose.Words;
using System.IO;

namespace Datamonitor.PremiumTools.Generic.Results
{
    /// <summary>
    /// 
    /// </summary>
    public partial class CountryProfilerChart : AuthenticationBasePage //System.Web.UI.Page
    {

        private static Color[] DMPalette = new Color[] { Color.FromArgb(0, 34, 82), Color.FromArgb(108, 128, 191), Color.FromArgb(165, 174, 204), Color.FromArgb(210, 217, 229), Color.FromArgb(216, 69, 25), Color.FromArgb(255, 102, 0), Color.FromArgb(255, 172, 112), Color.FromArgb(250, 222, 199), Color.FromArgb(255, 153, 0), Color.FromArgb(255, 204, 0) };
        string trendChartType = string.Empty;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["rowid"] != null && !string.IsNullOrEmpty(Request.QueryString["rowid"].Trim()) &&
                Request.QueryString["type"] != null && !string.IsNullOrEmpty(Request.QueryString["type"].Trim()))
            {
                PlotCountryOverviewTrendChart();
                trendChartType = "countryoverview";

            }
        }


        /// <summary>
        /// Plots the country overview trend chart.
        /// </summary>
        private void PlotCountryOverviewTrendChart()
        {
            string RowID = Request.QueryString["rowid"].Trim();
            DataSet ChartData = null;
            ChartData = SqlDataService.GetCountryOverviewTrendChartData(Convert.ToInt64(RowID));
            if (ChartData != null &&
                    ChartData.Tables.Count > 1 &&
                    ChartData.Tables[0].Rows.Count > 0 &&
                    ChartData.Tables[1].Rows.Count > 0)
            {
                PlotXYChart(ChartData,
                    SampleChart,
                    ChartData.Tables[1].Rows[0]["YaxisLabel"].ToString(),
                    325,
                    175);

            }
        }

        /// <summary>
        /// Gets the type of the series.
        /// </summary>
        /// <param name="typeString">The type string.</param>
        /// <returns></returns>
        private SeriesType GetSeriesType(string typeString)
        {
            switch (typeString.ToLower())
            {
                case "bar": return SeriesType.Bar;
                case "line": return SeriesType.Line;
                case "area": return SeriesType.AreaLine;
                default: return SeriesType.Bar;
            }
        }

        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart.</param>
        /// <param name="yAxisText">The y axis text.</param>
        /// <param name="width">The width.</param>
        /// <param name="height">The height.</param>
        /// <returns></returns>
        private dotnetCHARTING.Chart PlotXYChart(DataSet chartDataset,
                              dotnetCHARTING.Chart chart,
                              string yAxisText,
                              Unit width,
                              Unit height)
        {
            chart.Use3D = false;
            chart.Visible = true;

            LogUsage("Country Browser  - Trend chart", "trend chart", " ");
            if (chartDataset != null &&
               chartDataset.Tables.Count > 0 &&
               chartDataset.Tables[0].Rows.Count > 0)
            {
                chart.FileManager.ImageFormat = dotnetCHARTING.ImageFormat.Png;
                chart.FileQuality = 100;
                DataEngine objDataEngine = new DataEngine();
                chart.SeriesCollection.Clear();

                objDataEngine.Data = chartDataset;
                dotnetCHARTING.SeriesCollection sc;

                //Set data fields
                objDataEngine.DataFields = "XAxis=" +
                    chartDataset.Tables[0].Columns[0].ColumnName +
                    ",YAxis=" + chartDataset.Tables[0].Columns[1].ColumnName +
                    ",splitBy=" + chartDataset.Tables[0].Columns[2].ColumnName;

                sc = objDataEngine.GetSeries();
                chart.SeriesCollection.Add(sc);
                chart.DataBind();
                TwoDimensionalCharts.AddCopyrightText(chart, Convert.ToInt32(width.Value) - 150);
                chart.DefaultSeries.Type = chartDataset.Tables[0].Rows.Count > 15 ? SeriesType.AreaLine : SeriesType.Bar;
                if ((SeriesType)chart.DefaultSeries.Type == SeriesType.AreaLine)
                {
                    //chart.DefaultSeries.DefaultElement.Transparency = 20;
                    chart.DefaultSeries.DefaultElement.Marker.Type = ElementMarkerType.None;
                }

                string xAxisText = chartDataset.Tables[0].Columns[0].ColumnName;

                //Set axis label texts and styles
                chart.XAxis.Label.Text = xAxisText;
                chart.YAxis.Label.Text = yAxisText;
                chart.XAxis.DefaultTick.Label.Color = Color.Black;
                chart.XAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.YAxis.DefaultTick.Label.Color = Color.Black;
                chart.YAxis.DefaultTick.Label.Font = new System.Drawing.Font("verdana", 7);
                chart.XAxis.Line.Color = Color.DarkGray;
                chart.XAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.YAxis.Line.Color = Color.DarkGray;
                chart.YAxis.DefaultTick.Line.Color = Color.DarkGray;
                chart.DefaultSeries.DefaultElement.Marker.Size = 4;
                if (GlobalSettings.AllowAutoYaxisScaleRange)
                {
                    chart.YAxis.Scale = Scale.Range;
                }

                //Set chart styles
                chart.Palette = DMPalette;
                chart.Debug = false;
                chart.Height = height;
                chart.Width = width;
                chart.ChartArea.Line.Color = Color.White;
                chart.ChartArea.Shadow.Color = Color.White;
                chart.XAxis.SpacingPercentage = 30;
                chart.ChartArea.ClearColors();
                chart.DefaultChartArea.Background.Color = Color.White;
                chart.ChartArea.Line.Color = Color.LightGray;
                chart.ChartArea.Shadow.Color = Color.White;

                //Set legend properties and styles

                chart.LegendBox.Visible = false;
                if (!Directory.Exists(Server.MapPath("temp")))
                    Directory.CreateDirectory(Server.MapPath("temp"));
                
                chart.FileManager.TempDirectory = "temp";
                ChartImg.ImageUrl = chart.FileManager.SaveImage(chart.GetChartBitmap()).Replace("\\", "/");
                hdnImageUrl.Value = ChartImg.ImageUrl;
            }
            return chart;
        }


    }
}

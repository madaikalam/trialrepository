<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountryProfilerChart.aspx.cs" 
    Inherits="Datamonitor.PremiumTools.Generic.Results.CountryProfilerChart" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Chart</title>


</head>
<body>
    <form id="form1" runat="server"> 

    
             
    <asp:Image ID="ChartImg" runat="server" AlternateText="" Visible="false"/>
      <dotnet:Chart ID="SampleChart" runat="server" Visible="true">                                        
        </dotnet:Chart>       
              
        <asp:HiddenField ID="hdnImageUrl" runat="server" />
    </form>
</body>
</html>

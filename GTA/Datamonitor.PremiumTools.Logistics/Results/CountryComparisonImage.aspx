<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CountryComparisonImage.aspx.cs" 
Inherits="Datamonitor.PremiumTools.Generic.Results.CountryComparisonImage" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Chart</title>
    <style type="text/css">
    BODY {
    margin:0 0 0 0;
	font-size: 11px;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	color: #000000;
    }
    
    </style>
</head>
<body>
    <form id="form1" runat="server">
     <div style="float:right; width:12px;padding:0px">
            <asp:LinkButton ID="lnkExcel" runat="server" OnClick="lnkExcel_Click">          
             <asp:Image ImageUrl="~/assets/images/tool_excel.gif" ImageAlign="right" Width="12" Height="10" BorderWidth="0" id="excel" runat="server"/>
        </asp:LinkButton> </div>
        <div style="float:left;padding:0px">
         <asp:Label ID="ChartTitle" Font-Bold="true" runat="server"></asp:Label></div>
    <%--<asp:Label ID="ChartTitle" Font-Bold="true" runat="server"></asp:Label>--%>
     
      <dotnet:Chart ID="SampleChart" runat="server" Visible="true">                    
             
        </dotnet:Chart>
         <asp:Image ID="ChartImg" runat="server" AlternateText="" Visible="false"/>
         <asp:HiddenField ID="hdnImageUrl" runat="server" />
    </form>
</body>
</html>

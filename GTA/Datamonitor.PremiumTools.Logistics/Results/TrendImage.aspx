<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrendImage.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.Results.ImagePage" %>
<%@ Register TagPrefix="dotnet" Namespace="dotnetCHARTING" Assembly="dotnetCHARTING"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Chart</title>

<style type="text/css">
.tdFirst
{
    text-align: center;
    height: 20px;
    width: 100px;
    background-color: #009900;
}
.tdFourth
{ 
    text-align: center;
    height: 20px;
    width: 100px;
    background-color: #ff0000;
}
.tdSecond
{
    text-align: center;
    height: 20px;
    width: 100px;
    background-color: #ffcc33;
}
.tdThird
{
    text-align: center;
    height: 20px;
    width: 100px;
    background-color: #ff6600;
}
BODY {
margin:0 0 0 0;
	font-size: 11px;
	font-family: Verdana, Arial, Helvetica, sans-serif; 
	color: #000000;
}
        .rightdiv
        {
            float:right;
            margin-bottom:4px;
        }
        .leftdiv
        {
            float:left;
            width:60px;
        }
        input[type=checkbox] 
        {
	        width:5px;
	        margin-left:-65px;
	        margin-right:-65px;
	        padding:0px;
	        text-align:left;
        }
        .canBold
        {
            font-weight:bold;
            border-bottom-color:gray;
            border-bottom-style:solid;
            border-bottom-width:1px;
        }
        .notBold
        {            
            border-bottom-color:gray;
            border-bottom-style:solid;
            border-bottom-width:1px;            
        }
                
        .lvl0
        {            	        
            margin:0 0 0 0;
	        padding-left:0px;
	 
        }        
                
        .lvl1
        {            	        
            margin:0 0 0 0;
	        padding-left:10px;
	 
        }        
        
        .lvl2
        {            	        
            margin:0 0 0 0;
	        padding-left:20px;
	 
        }                
        
    </style>
</head>
<body>
    <form id="form1" runat="server"> 
        <div style="float:right; width:12px;">
            <asp:LinkButton ID="lnkExcel" runat="server" OnClick="lnkExcel_Click">          
             <asp:Image ImageUrl="~/assets/images/tool_excel.gif" ImageAlign="right" Width="12" Height="10" BorderWidth="0" id="excel" runat="server"/>
        </asp:LinkButton> </div>
        <div style="float:left;"><asp:Label ID="ChartTitle" Font-Bold="true" runat="server"></asp:Label></div>
             
    <asp:Image ID="ChartImg" runat="server" AlternateText="" Visible="false"/>
      <dotnet:Chart ID="SampleChart" runat="server" Visible="true">                                        
        </dotnet:Chart>       
              
        <div id="QuartileDiv" runat="server" style="height:300px;overflow:auto">
        Indicator: <asp:Label ID="Indicatorlbl" runat="server" Font-Bold="true"></asp:Label> &nbsp;&nbsp;
            <asp:LinkButton ID="lnkExcelQuartile" runat="server" OnClick="lnkExcelQuartile_Click">         
                <asp:Image ImageUrl="~/assets/images/tool_excel.gif" ImageAlign="right" Width="12" Height="10" BorderWidth="0" id="Image1" runat="server"/>
            </asp:LinkButton> 
        <br />
        Year:<asp:Label ID="QuartileYear" runat="server" Font-Bold="true"></asp:Label><br />
        Units:<asp:Label ID="QuartileUnits" runat="server" Font-Bold="true"></asp:Label>
        
        
        <asp:Repeater ID="QuartileRepeater" runat="server">            
            <HeaderTemplate>                
                <table cellpadding="2" cellspacing="0" style="margin-top:10px">
                <tr class="canBold" style="background-color:#BFC4D1;height:19px;">  
                    <th>Rank</th>                  
                    <th align="left">Country</th>
                    <th align="right">Value</th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr class='<%#Eval("font")%>'>
                     <td class='<%=Request.QueryString["IsRank"] %><%# "td"+Eval("Rank")%>' style="width:10px"><%# Eval("RankNumber")%></td>
                    <td style="width:180px"><%#Eval("Country")%></td>
                    <td align="right" style="width:70px"><%#Eval("Value", "{0:N1}")%></td>
                </tr>
            </ItemTemplate>
            <FooterTemplate>
                </table>
            </FooterTemplate>
            </asp:Repeater>
        </div>
        <asp:HiddenField ID="hdnImageUrl" runat="server" />
    </form>
</body>
</html>

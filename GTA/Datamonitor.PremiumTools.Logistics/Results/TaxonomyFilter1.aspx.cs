using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;


namespace Datamonitor.PremiumTools.Generic.Results
{

    public partial class TaxonomyFilter1 : System.Web.UI.Page
    {
        //Private variables
        public int TaxonomyTypeID;
        public string TaxonomyType;
        private string _taxonomyTypeHasSubCategory;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack &&!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                //Get the taxonomytypeid to load
                string Taxonomy = Request.QueryString["id"].ToString();

                //Get all the taxonomy types list
                string Path = Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath);
                XmlDocument TaxonomyTypes = new XmlDocument();
                
                //Load the list into xml document
                TaxonomyTypes.Load(Path);

                //Select current taxonomytype's node from the whole list
                XmlNode TaxonomyType = TaxonomyTypes.SelectSingleNode("//TaxonomyTypes/TaxonomyType[@ColumnName='" + Taxonomy + "']");

                if (TaxonomyType != null)
                {
                    int ID = Convert.ToInt32(TaxonomyType.Attributes.GetNamedItem("ID").Value);
                    _taxonomyTypeHasSubCategory = TaxonomyType.Attributes.GetNamedItem("HasSubCategory").Value;

                    //Check the control type to load
                    string ControlType = TaxonomyType.Attributes.GetNamedItem("ControlType").Value;                    

                    BaseSelectionControl SelectionControl = new BaseSelectionControl();

                    //Load the respective control
                    if (ControlType.Equals("CHKLIST"))
                    {
                        SelectionControl = (CheckListControl)Page.LoadControl("~/Controls/CheckListControl.ascx");
                    }
                    else if (ControlType.Equals("TREE"))
                    {
                        SelectionControl = (TreeviewControl)Page.LoadControl("~/Controls/TreeviewControl.ascx");
                    }

                    //Assign Attributes to the control
                    this.TaxonomyTypeID = ID;
                    this.TaxonomyType = TaxonomyType.Attributes.GetNamedItem("Name").Value;
                    SelectionControl.TaxonomyTypeID = ID;
                    SelectionControl.TaxonomyType = TaxonomyType.Attributes.GetNamedItem("Name").Value;
                    SelectionControl.Source = "results";
                    SelectionControl.Visible = true;
                    SelectionControl.ControlHeight = 360;
                    SelectionControl.ControlWidth = 400;
                    SelectionControl.FlagChecked = false;
                    SelectionControl.FlagCheckboxText = "";
                    SelectionControl.ShowTaxonomyDefinition = GlobalSettings.ShowTaxonomyDefinitionInResults;

                    //Load data to the control
                    SelectionControl.LoadData();

                    //Add the control to the page
                    controlHolder.Controls.Add(SelectionControl);                    
                }
            }
        }

        public string TaxonomyTypeHasSubCategory()
        {
            return string.IsNullOrEmpty(_taxonomyTypeHasSubCategory) ? "false" : _taxonomyTypeHasSubCategory;
        }

    }
}

using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;


namespace Datamonitor.PremiumTools.Generic.Results
{
    
    public partial class TaxonomyFilter : System.Web.UI.Page
    {
        public int TaxonomyTypeID;
        public string TaxonomyType;
        private string _taxonomyTypeHasSubCategory;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack &&!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                string Taxonomy = Request.QueryString["id"].ToString();
                string Path = Server.MapPath(GlobalSettings.TaxonomyTypesConfigPath);
                XmlDocument TaxonomyTypes = new XmlDocument();
                
                TaxonomyTypes.Load(Path);
                XmlNode TaxonomyType = TaxonomyTypes.SelectSingleNode("//TaxonomyTypes/TaxonomyType[@ColumnName='" + Taxonomy + "']");

                if (TaxonomyType != null)
                {
                    int ID = Convert.ToInt32(TaxonomyType.Attributes.GetNamedItem("ID").Value);
                    string ControlType = TaxonomyType.Attributes.GetNamedItem("ControlType").Value;
                    _taxonomyTypeHasSubCategory = TaxonomyType.Attributes.GetNamedItem("HasSubCategory").Value;

                    BaseSelectionControl SelectionControl = new BaseSelectionControl();
                    if (ControlType.Equals("CHKLIST"))
                    {
                        SelectionControl = (CheckListControl)Page.LoadControl("~/Controls/CheckListControl.ascx");
                    }
                    else if (ControlType.Equals("TREE"))
                    {
                        SelectionControl = (TreeviewControl)Page.LoadControl("~/Controls/TreeviewControl.ascx");
                    }
                    this.TaxonomyTypeID = ID;
                    this.TaxonomyType = Taxonomy;
                    SelectionControl.TaxonomyTypeID = ID;
                    SelectionControl.TaxonomyType = Taxonomy;
                    SelectionControl.Source = "results";
                    SelectionControl.Visible = true;
                    SelectionControl.ShowTaxonomyDefinition = GlobalSettings.ShowTaxonomyDefinitionInResults;
                    SelectionControl.ControlHeight = 385;
                    SelectionControl.ControlWidth = 400;
                    SelectionControl.FlagChecked = false;
                    SelectionControl.FlagCheckboxText = "";
                    SelectionControl.LoadData();
                    
                    controlHolder.Controls.Add(SelectionControl);
                }
            }
        }

        public string TaxonomyTypeHasSubCategory()
        {
            return string.IsNullOrEmpty(_taxonomyTypeHasSubCategory) ? "false" : _taxonomyTypeHasSubCategory;
        }

    }
}

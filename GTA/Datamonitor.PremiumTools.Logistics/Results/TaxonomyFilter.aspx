<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TaxonomyFilter.aspx.cs" Inherits="Datamonitor.PremiumTools.Generic.Results.TaxonomyFilter" %>
 <%@ Register TagPrefix="ComponentArt" Namespace="ComponentArt.Web.UI" Assembly="ComponentArt.Web.UI" %>

<%@ Register Src="../Controls/CheckListControl.ascx" TagName="CheckListControl" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Taxonomy Filter</title>    
        <link href="~/assets/css/datamonitor.css" rel="stylesheet" type="text/css"/>
         <link href="~/assets/css/layout.css" rel="stylesheet" type="text/css"/>
    <link href="~/assets/css/treeview.css" rel="stylesheet" type="text/css"/>
    <link href="~/assets/css/gridStyle.css" rel="stylesheet" type="text/css"/>
        <style type="text/css">
        .highslide-header ul li.highslide-previous, .highslide-header ul li.highslide-next 
        {
	        display: none;
        }
    </style>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/scripts/highslide-with-html.js") %>"></script>

        <style type="text/css">
        BODY {	   
	    font-size: 11px;
	    font-family: Verdana, Arial, Helvetica, sans-serif;
	    color: #000000;
    }
        </style>
    <script type="text/javascript" src="<%= Page.ResolveClientUrl("~/assets/scripts/jquery-1.3.2.min.js") %>"></script>
    <script type="text/javascript">
    function UpdateSession(taxonomyTypeID, taxonomyID, name, actionType)
	{
	    var hasSubcategory ="<%= TaxonomyTypeHasSubCategory() %>";
	    var urlPath ="SessionHandler.aspx?taxonomyTypeID="+
	        taxonomyTypeID + 
	        "&taxonomyID="+
	        taxonomyID+
	        "&name=" + 
	        unescape(name.replace('&#43;','+')) +
	        "&hassubcategory=" + 
	        hasSubcategory +
	        "&actionType="+
	        actionType;
	        //alert(urlPath);
	        
        var options = {
            contentType: "text/html; charset=utf-8",
            type: "GET",
            url: urlPath,
            dataType: "html",            
            async: false,
            success: function(response) {
                //alert("success: " + response);
            },
            error: function(msg) { alert("failed: " + msg); }
        };

        var returnText = $.ajax(options).responseText;
        return returnText;
	}
	
	///This method is called when the check box inside grid selection control is checked 
	///Updates the corresponding selection in session
    function ListitemCheckChange(control, taxonomyType, taxonomyTypeID, taxonomyID, name)
    {           
        if (control.checked == true)
        {
            strMsg = UpdateSession(taxonomyTypeID, taxonomyID, name, "add");
        }
        else
        {
           strMsg = UpdateSession(taxonomyTypeID, taxonomyID, name, "remove");
        }        
    }
    
    function ExpandAndCheckAllChildNodes(currentNode,taxonomyTypeID,taxonomyType)
    {
        var cNodes=currentNode.Nodes(); 
        for(var i=0;i<cNodes.length;i++)
        {  
            //cNodes[i].set_checked(true); 
            cNodes[i].expand();            
            if(cNodes[i].Nodes().length>0)
            {
                ExpandAndCheckAllChildNodes(cNodes[i],taxonomyTypeID,taxonomyType);
            }
            else
            {
                var nodeID= (taxonomyTypeID==11)? (-1*cNodes[i].ID): cNodes[i].ID;
                UpdateSession(taxonomyTypeID, nodeID, cNodes[i].Value, "add");            
            }
        }
    }
    
    function CollapseAndUncheckAllChildNodes(currentNode,taxonomyTypeID,taxonomyType)
    {
        var cNodes=currentNode.Nodes(); 
        for(var i=0;i<cNodes.length;i++)
        {  
            //cNodes[i].set_checked(false); 
            //cNodes[i].collapse();
            
            if(cNodes[i].Nodes().length>0)
            {           
                CollapseAndUncheckAllChildNodes(cNodes[i],taxonomyTypeID,taxonomyType);
            }
            else
            {
                 var nodeID= (taxonomyTypeID==11)? (-1*cNodes[i].ID): cNodes[i].ID;
                UpdateSession(taxonomyTypeID, nodeID, cNodes[i].Value, "remove"); 
            }
        }
    }
        
    function tvForAll_onNodeCheckChange(node)
    {   
       var taxonomyTypeID = <%= TaxonomyTypeID %>;
       var taxonomyType = "<%= TaxonomyType %>";
       
       if(node.ID < 0)
       {
            taxonomyTypeID=11;
       }
       
       if (node.Checked==true)
        {
            if(node.Nodes().length>0)
            {        
                var currentTreeView =GetTreeView();	
                currentTreeView.beginUpdate();
                node.expand();                   
                node.checkAll();
                currentTreeView.endUpdate();                
                ExpandAndCheckAllChildNodes(node,taxonomyTypeID,taxonomyType);
            }
            else
            {
                var nodeID= (taxonomyTypeID==11)? (-1*node.ID): node.ID;
                UpdateSession(taxonomyTypeID, nodeID, node.Value, "add");    
            }
        }
        else
        {            
            if(node.Nodes().length>0)
            {
                var currentTreeView =GetTreeView();	
                currentTreeView.beginUpdate();
                node.unCheckAll();
                currentTreeView.endUpdate();
                CollapseAndUncheckAllChildNodes(node,taxonomyTypeID,taxonomyType);
            }
            else
            {
                var nodeID= (taxonomyTypeID==11)? (-1*node.ID): node.ID;
                UpdateSession(taxonomyTypeID, nodeID, node.Value, "remove");
            }
        }
    }
    
    </script>
</head>
<body style="background-color:White;background-image:none">
    <form id="form1" runat="server">
        <div id="controlHolder" runat="server" >
        </div>
        
    <input id="hdnTaxonomyTypeIDForFilter" type="hidden" value="" />
    <input id="hdnTaxonomyTypeForFilter" type="hidden" value="" />
    </form>
</body>
</html>

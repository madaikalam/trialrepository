<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="PredefinedViews.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.PredefinedViews" 
        Title="Datamonitor PremiumTools" %>

<%@ Register Src="Controls/CheckListControl.ascx" TagName="CheckListControl" TagPrefix="uc1" %>
<%@ Register Src="Controls/TreeviewControl.ascx" TagName="TreeviewControl" TagPrefix="uc2" %>
<%@ Register Src="Controls/SelectionList.ascx" TagName="Selections" TagPrefix="uc3" %>
<%@ Reference Control = "Controls/TreeviewControl.ascx" %>
<%@ Reference Control = "Controls/CheckListControl.ascx" %>

<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>        
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<link href="Assets/css/jquery-ui.css" rel="stylesheet" type="text/css"/>
<script type="text/javascript" src="Assets/Scripts/jquery.min.js"></script>
<script type="text/javascript" src="Assets/Scripts/jquery-ui.min-1.8.1.js"></script>  
<script type="text/javascript">	
	
	
	hs.graphicsDir = 'Assets/images/popup/';
	
	function ValidateSelections()
    {
        var returnStatus=false;
        var currentSelections = $("#<%=hdnUserSelections.ClientID%>").val();
        //check for mandatory selections	    
        var mandFields="<% =GetMandatoryTaxonomy() %>";
	    var mandTaxonomyTypeIDs="<% =GetMandatoryTaxonomyTypeIDs() %>";
	    var fieldsArray=mandTaxonomyTypeIDs.split(",");
	    var counter=0;
	    if(mandTaxonomyTypeIDs.length>0)
	    {
	        for(field in fieldsArray)
	        {	        
	            if(currentSelections.indexOf("|"+fieldsArray[field]+"~")==-1)
	            {
	                counter=counter+1;
	            }
	        }
	        if(counter == fieldsArray.length)
	        {
	            //mandatory selections are not done, through alert
	            mandFields=mandFields.replace(/,/g," or ");
	            alert("Select "+mandFields);
	        }
	        else 
            {
                //mandatory selections are done..
                
                var StartYearDropdown = document.getElementById('<%=StartYear.ClientID %>');
                var EndYearDropdown = document.getElementById('<%=EndYear.ClientID %>');
                
                if(StartYearDropdown.value > EndYearDropdown.value)
                {
                    alert('Start year cannot be greater than End year');                    
                }
                else
                {
                    //Update start year value to session
                    UpdateSession('999', StartYearDropdown.value, 'startyear', 'yearsUpdate');
                    //Update end year value to session
                    UpdateSession('999', EndYearDropdown.value, 'endyear', 'yearsUpdate');
                    
                    returnStatus=true;
                }
            }
	    }
	    return returnStatus;
    }
	
	
	function ValidateAndRedirect(redirectTo)
	{	
	    var redirectUrl;
	    if(redirectTo=='Analysis')
	    {
	        redirectUrl="analysis/countrycomparison.aspx";
	    }
	    else if(redirectTo=='Results')
	    {
	        redirectUrl="<%= GetResultsPage()%>";
	    }
	    
	    //check for mandatory selections
	    var currentSelections = $("#<%=hdnUserSelections.ClientID%>").val();
	    
	    var mandFields="<% =GetMandatoryTaxonomy() %>";
	    var mandTaxonomyTypeIDs="<% =GetMandatoryTaxonomyTypeIDs() %>";
	    var fieldsArray=mandTaxonomyTypeIDs.split(",");
	    var counter=0;
	    if(mandTaxonomyTypeIDs.length>0)
	    {
	        for(field in fieldsArray)
	        {	        
	            if(currentSelections.indexOf("|"+fieldsArray[field]+"~")==-1)
	            {
	                counter=counter+1;
	            }
	        }
	        if(counter == fieldsArray.length)
	        {
	            //mandatory selections are not done, through alert
	            mandFields=mandFields.replace(/,/g," or ");
	            alert("Select "+mandFields);
	        }
	        else 
            {
                //mandatory selections are done..
                
                var StartYearDropdown = document.getElementById('<%=StartYear.ClientID %>');
                var EndYearDropdown = document.getElementById('<%=EndYear.ClientID %>');
                
                if(StartYearDropdown.value > EndYearDropdown.value)
                {
                    alert('Start year cannot be greater than End year');                    
                }
                else
                {
                    //Update start year value to session
                    UpdateSession('999', StartYearDropdown.value, 'startyear', 'yearsUpdate');
                    //Update end year value to session
                    UpdateSession('999', EndYearDropdown.value, 'endyear', 'yearsUpdate');
            
                    //Update the session 
                    syncUserSelectionsToSession();
                    //redirect the page after updating the session with current selections
                    window.location.href = redirectUrl;
                }
            }
	    }
	}
	
	function syncUserSelectionsToSession()
	{	    
	    //Update the session 
        var selection=$("#<% = hdnUserSelections.ClientID%>").val();
        if(selection.length>0)
        {
            UpdateSession("", "",escape(selection),"addGroupSelection");
        }
	}
	
	function ClearSelections()
	{	
	    //clears all selections in right pane
        var taxTypeid= $("#hdnTaxonomyTypeID").val();
        var name=$("#hdnTaxonomyType").val();
        var controlType=$("#hdnControlType").val();
        var parentIDs=$("#<% =hdnparentIDs.ClientID %>").val();
        var fullid=$("#<% =hdnFullTaxonomyTypeID.ClientID %>").val();
        if(taxTypeid!="" && name!="" && controlType!="")
        {
            $("#advanced_criteria p").remove();
            //clear the session
            UpdateSession("0", "0", "0", "removeAll");            
            //clear the hidden user selections
            $("#<% = hdnUserSelections.ClientID%>").val("");

            //refresh the controls
            CallBack1.callback(taxTypeid,unescape(name),controlType,fullid,parentIDs, $("#<% = hdnUserSelections.ClientID%>").val(),false,'');
        }
	}
	
	function tryToUncheckSelectedNode(taxonomyID)
	{
	    var currentTreeView =GetTreeView();	   	    
	    var currentNode = currentTreeView.findNodeByProperty('ID',taxonomyID); 	    	    
        try
        {            
            if(currentNode != null)
            {
                //uncheck current node
                currentTreeView.beginUpdate();                 
                currentNode.set_checked(false);  
                
                var curCTemplate = currentNode.get_clientTemplateId(); 
                if(curCTemplate.indexOf("Pn")>-1)
                {
                    curCTemplate = curCTemplate.replace("PnSel", ""); 
                    curCTemplate = curCTemplate.replace("Pn", ""); 
                    curCTemplate = curCTemplate + 'PnSel';
                    currentNode.set_clientTemplateId(curCTemplate);
                }
                currentTreeView.endUpdate();
            }
        }
        catch(ex)
        {        }
	}
       
    function CollapseAndUncheckAllChildNodes(currentNode,fullid,taxonomyType)
    {
        var cNodes=currentNode.Nodes();
        var CurrSelections=$("#<%= hdnUserSelections.ClientID%>").val();
        
        for(var i=0;i<cNodes.length;i++)
        {  
            if(CurrSelections.indexOf('~'+currentNode.Value+'|') > -1 )
            {
                removeLinkFromSelection(fullid, taxonomyType, cNodes[i].ID, escape(cNodes[i].Value),"false");                
            }
            else if(CurrSelections.indexOf('~'+currentNode.Value+"(All)"+'|') > -1)
            {
                removeLinkFromSelection(fullid, taxonomyType, cNodes[i].ID, escape(cNodes[i].Value+"(All)"),"false");
                CollapseAndUncheckAllChildNodes(cNodes[i],fullid,taxonomyType);
            }
        }
    }
    
    ///This method is called when the check box inside tree control is checked 
	///Updates the corresponding selection in session
    function tvForAll_onNodeCheckChange(node)
    {   
        var taxonomyTypeID = $("#hdnTaxonomyTypeID").val();
        var taxonomyType = $("#hdnTaxonomyType").val();       
        var fullid=$("#<%= hdnFullTaxonomyTypeID.ClientID%>").val();         
        var newSelection="";
        var subTaxonomySelection ="";   
        
        if(node.Checked==true)
        {    
            if(node.Nodes().length==0)
            {
                addToSelection(fullid, taxonomyType, node.ID, escape(node.Value),"false");        
                //prepare selection string to 'newSelection'
                newSelection = taxonomyTypeID+'~'+ node.ID+'~'+node.Value; 
                subTaxonomySelection =  fullid+'~'+ node.ID+'~'+node.Value;               
                if(fullid!=null && fullid != taxonomyTypeID)
                {
                    newSelection = newSelection + '|' + subTaxonomySelection;
                }                        
            }
            else
            {
                addToSelection(fullid, taxonomyType, node.ID, escape(node.Value+"(All)"),"false");    
                //check childs
                var currentTreeView =GetTreeView();	
                currentTreeView.beginUpdate();
                node.checkAll();
                currentTreeView.endUpdate();   
                //prepare selection string to 'newSelection'
                newSelection = taxonomyTypeID+'~'+ node.ID+'~'+node.Value+"(All)"; 
                subTaxonomySelection =  fullid+'~'+ node.ID+'~'+node.Value+"(All)";               
                if(fullid!=null && fullid != taxonomyTypeID)
                {
                    newSelection = newSelection + '|' + subTaxonomySelection;
                }                             
            }
            //Persist the current selections to hidden field            
            var currentValue = $("#<% = hdnUserSelections.ClientID%>").val();
            if(currentValue.length==0)
            {
                currentValue = currentValue +'|';
            }
            //add selection string to hidden field   
            
            $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
            
        }
        else
        {
            var currentTreeView =GetTreeView();	
            currentTreeView.beginUpdate();
            //debugger;
            if($("#<%= hdnUserSelections.ClientID%>").val().indexOf('~'+node.Value+"(All)"+'|') > -1)
            {
                removeLinkFromSelection(fullid,taxonomyType, node.ID,  escape(node.Value+"(All)"),"false");
                node.unCheckAll();
                CollapseAndUncheckAllChildNodes(node,fullid,taxonomyType);
            }
            else
            {   
                if($("#<%= hdnUserSelections.ClientID%>").val().indexOf('~'+node.Value+'|') > -1)
                {
                    removeLinkFromSelection(fullid,taxonomyType, node.ID,  escape(node.Value),"false");    
                }
                
                var currNode=node;            
                while(currNode.ParentNode != null && currNode.ParentNode.get_checked())
                {                           
                    if($("#<%= hdnUserSelections.ClientID%>").val().indexOf('~'+currNode.ParentNode.Value+"(All)"+'|') > -1)
                    {
                        removeLinkFromSelection(fullid,taxonomyType, currNode.ParentNode.ID,  escape(currNode.ParentNode.Value+"(All)"),"false");                                
                    }
                    currNode.ParentNode.set_checked(false);
                    
                    for(var i=0;i<currNode.ParentNode.Nodes().length;i++)
                    {
                        var allSiblings=currNode.ParentNode.Nodes();
                        if(allSiblings[i].Value!=currNode.Value)
                        {
                            if(allSiblings[i].Nodes().length==0)
                            {
                                addToSelection(fullid, taxonomyType, allSiblings[i].ID, escape(allSiblings[i].Value),"false");   
                                 //prepare selection string to 'newSelection'
                                newSelection = taxonomyTypeID+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value; 
                                subTaxonomySelection =  fullid+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value;               
                                if(fullid!=null && fullid != taxonomyTypeID)
                                {
                                    newSelection = newSelection + '|' + subTaxonomySelection;
                                }   
                            }
                            else
                            {
                                addToSelection(fullid, taxonomyType, allSiblings[i].ID, escape(allSiblings[i].Value+"(All)"),"false");    
                                 //prepare selection string to 'newSelection'
                                newSelection = taxonomyTypeID+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value+"(All)"; 
                                subTaxonomySelection =  fullid+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value+"(All)";               
                                if(fullid!=null && fullid != taxonomyTypeID)
                                {
                                    newSelection = newSelection + '|' + subTaxonomySelection;
                                }   
                            }
                            //Persist the current selections to hidden field            
                            var currentValue = $("#<% = hdnUserSelections.ClientID%>").val();
                            if(currentValue.length==0)
                            {
                                currentValue = currentValue +'|';
                            }
                            //add selection string to hidden field    
                            $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
                        }
                    }
                   
                    currNode=currNode.ParentNode;                       
                }                    
                
            }
            
            currentTreeView.endUpdate();            
        }
        //alert($("#<%= hdnUserSelections.ClientID%>").val());
    }

	function removeLinkFromSelection(fullid,taxonomyType, taxonomyID, name, canUpdateSession)
	{
	    name=unescape(name);
        var strMsg = "true"; 

        var controlType = $("#hdnControlType").val();
        switch(controlType)
        {
            case "TREE":
              tryToUncheckSelectedNode(taxonomyID);
              break;
            default:
        }
	    
	    var str = "A[id='selspan" + taxonomyID + "']";
	    $(str).remove();
	    if(canUpdateSession == "true")
        {
	        //update session
            strMsg = UpdateSession(fullid, taxonomyID, name, "remove");
        }
        if(strMsg == "true")
        {
	        var selectionPara = "P[id='taxtype" + fullid + "']";
	        if($(selectionPara + " A").length==0)
	        {
	            $(selectionPara).remove();	          
	        }
	    }
	    //Persist the current selections to hidden field  
	    var currentValue = $("#<% = hdnUserSelections.ClientID%>").val(); 	    

        var newSelection = parseInt(fullid) +'~'+ taxonomyID+'~'+name; 
        var subTaxonomySelection =  fullid+'~'+ taxonomyID+'~'+name;

        $("#<% = hdnUserSelections.ClientID%>").val(currentValue.replace('|'+subTaxonomySelection+'|', '|'));
        currentValue = $("#<% = hdnUserSelections.ClientID%>").val(); 	    
        $("#<% = hdnUserSelections.ClientID%>").val(currentValue.replace('|'+newSelection+'|', '|'));
       
	}
    function removeFromSelection(fullid,taxonomyType, taxonomyID, name, canUpdateSession)
	{		
	
		if(fullid.charAt(0)=="A")
	    {
	        removeLinkFromSelection(fullid,taxonomyType, taxonomyID,  escape(name),"true");
	    }    
	    var currentTreeView =GetTreeView();	
	    var node = currentTreeView.findNodeByProperty('ID',taxonomyID); 	

	    if(node)
	    { 
            currentTreeView.beginUpdate();
            //debugger;
            if($("#<%= hdnUserSelections.ClientID%>").val().indexOf('~'+node.Value+"(All)"+'|') > -1)
            {
                removeLinkFromSelection(fullid,taxonomyType, node.ID,  escape(node.Value+"(All)"),"false");
                node.unCheckAll();
                CollapseAndUncheckAllChildNodes(node,fullid,taxonomyType);
            }
            else
            {   
                if($("#<%= hdnUserSelections.ClientID%>").val().indexOf('~'+node.Value+'|') > -1)
                {
                    removeLinkFromSelection(fullid,taxonomyType, node.ID,  escape(node.Value),"false");    
                }
                
                    var currNode=node;            
                    while(currNode.ParentNode != null && currNode.ParentNode.get_checked())
                    {                           
                        if($("#<%= hdnUserSelections.ClientID%>").val().indexOf('~'+currNode.ParentNode.Value+"(All)"+'|') > -1)
                        {
                            removeLinkFromSelection(fullid,taxonomyType, currNode.ParentNode.ID,  escape(currNode.ParentNode.Value+"(All)"),"false");    
                            currNode.unCheckAll();
                            for(var i=0;i<currNode.ParentNode.Nodes().length;i++)
                            {
                                var allSiblings=currNode.ParentNode.Nodes();
                                if(allSiblings[i].Value!=currNode.Value)
                                {
                                    if(allSiblings[i].Nodes().length==0)
                                    {
                                        addToSelection(fullid, taxonomyType, allSiblings[i].ID, escape(allSiblings[i].Value),"false");   
                                         //prepare selection string to 'newSelection'
                                        newSelection = parseInt(fullid)+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value; 
                                        subTaxonomySelection =  fullid+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value;               
                                        if(fullid!=null && fullid != parseInt(fullid))
                                        {
                                            newSelection = newSelection + '|' + subTaxonomySelection;
                                        }   
                                    }
                                    else
                                    {
                                        addToSelection(fullid, taxonomyType, allSiblings[i].ID, escape(allSiblings[i].Value+"(All)"),"false");    
                                         //prepare selection string to 'newSelection'
                                        newSelection = parseInt(fullid)+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value+"(All)"; 
                                        subTaxonomySelection =  fullid+'~'+ allSiblings[i].ID+'~'+allSiblings[i].Value+"(All)";               
                                        if(fullid!=null && fullid != parseInt(fullid))
                                        {
                                            newSelection = newSelection + '|' + subTaxonomySelection;
                                        }   
                                    }
                                    //Persist the current selections to hidden field            
                                    var currentValue = $("#<% = hdnUserSelections.ClientID%>").val();
                                    if(currentValue.length==0)
                                    {
                                        currentValue = currentValue +'|';
                                    }
                                    //add selection string to hidden field    
                                    $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
                                }
                            }
                        }
                        else
                        {
                            currNode.ParentNode.set_checked(false);
                            CollapseAndUncheckAllChildNodes(currNode,fullid,taxonomyType);
                        }
                        currNode=currNode.ParentNode;                       
                    }                    
                
            }
            
            currentTreeView.endUpdate(); 
        }
        else
        {  
	        removeLinkFromSelection(fullid,taxonomyType, taxonomyID, name, canUpdateSession);
        }
	}	
	function unCheckThisNode(nodeID)
	{
	    var taxonomyTypeID = $("#hdnTaxonomyTypeID").val();
        var taxonomyType = $("#hdnTaxonomyType").val();       
        var fullid=$("#<%= hdnFullTaxonomyTypeID.ClientID%>").val();         
        var newSelection="";
        var subTaxonomySelection ="";   
        
        var currentTreeView =GetTreeView();	   	            
        var node = currentTreeView.get_selectedNode();
        
        currentTreeView.beginUpdate();
        var curCTemplate = node.get_clientTemplateId(); 
        curCTemplate = curCTemplate.replace("PnSel", ""); 
        curCTemplate = curCTemplate.replace("Pn", ""); 
        curCTemplate = curCTemplate + 'PnSel';
        node.set_clientTemplateId(curCTemplate);
        currentTreeView.endUpdate();
        
	    removeLinkFromSelection(fullid,taxonomyType, node.ID,  escape(node.Value),"false"); 
	}
	function CheckThisNode(nodeID)
	{
	    var taxonomyTypeID = $("#hdnTaxonomyTypeID").val();
        var taxonomyType = $("#hdnTaxonomyType").val();       
        var fullid=$("#<%= hdnFullTaxonomyTypeID.ClientID%>").val();         
        var newSelection="";
        var subTaxonomySelection ="";   
        
	    var currentTreeView =GetTreeView();	   	            
        var node = currentTreeView.get_selectedNode();
        
        currentTreeView.beginUpdate();        
        var curCTemplate = node.get_clientTemplateId(); 
        curCTemplate = curCTemplate.replace("PnSel", ""); 
        curCTemplate = curCTemplate.replace("Pn", ""); 
        curCTemplate = curCTemplate + 'Pn';
        node.set_clientTemplateId(curCTemplate);        
        currentTreeView.endUpdate();
        
        addToSelection(fullid, taxonomyType, node.ID, escape(node.Value),"false");        
        //prepare selection string to 'newSelection'
        var newSelection = taxonomyTypeID+'~'+ node.ID+'~'+node.Value; 
        var subTaxonomySelection =  fullid+'~'+ node.ID+'~'+node.Value;               
        if(fullid!=null && fullid != taxonomyTypeID)
        {
            newSelection = newSelection + '|' + subTaxonomySelection;
        }    
         var currentValue = $("#<% = hdnUserSelections.ClientID%>").val();
        if(currentValue.length==0)
        {
            currentValue = currentValue +'|';
        }
        //add selection string to hidden field    
        $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
	}

    function renderControl(taxTypeid,name,controlType,needCallback, fullid, parentIDs, flagCheckboxChecked, flagCheckboxText)
    {   
        if($("#hdnControlType").val()=="TREE")
            RefreshTreview();      

        //change the navigation selection
        $("#navSection > ul li").removeClass("selected");
        $("#nav"+fullid).addClass("selected");   

        //set navigation changes          
        $("#hdnTaxonomyTypeID").val(taxTypeid);
        $("#hdnTaxonomyType").val(name);
        $("#hdnControlType").val(controlType);
       
        $("#<%= hdnFullTaxonomyTypeID.ClientID%>").val(fullid);
        $("#<%= hdnparentIDs.ClientID%>").val(parentIDs);
       
      if(needCallback=="true")
      {
        //syncUserSelectionsToSession();
        CallBack1.callback(taxTypeid,unescape(name),controlType, fullid, parentIDs, $("#<% = hdnUserSelections.ClientID%>").val(), flagCheckboxChecked, flagCheckboxText);
      }
      
      //UNCOMENT BELOW LINE FOR GFSA
      //SetNotes(fullid);
      
//      alert(document.getElementById("lblNote").innerText);

    }
    
    function SetNotes(taxonomyType)
    {
        if(taxonomyType=="3A")
        {
            document.getElementById("lblNote").innerText = "Product and channel data is mutually exclusive. Product data by channel or channel data by products is not available.";
        }
        else if(taxonomyType=="3B")
        {
            document.getElementById("lblNote").innerText = "Channel value is inclusive of all food and drinks purchased by the specific channel.";
        }
        else if(taxonomyType=="3C")
        {
            document.getElementById("lblNote").innerText = "Product value is inclusive of purchases across all foodservice channels as covered by Datamonitor for the specific product.";
        }
        else if(taxonomyType=="6")
        {
            document.getElementById("lblNote").innerText = "Transaction, outlets and Chain outlets data is only available at a country level. This information is not available at a product level.";
        }
        else 
        {
            document.getElementById("lblNote").innerText = "";
        }
    }
   
    function RefreshTreview()
    {
        var currentTreeView =GetTreeView();
        try
        {
            currentTreeView.get_nodes().clear();
        }
        catch(ex)
        {
        }
    }

    function PredefinedViews_onChange()
    {
        //var PredefinedViews = document.getElementById('<%//PredefinedViews.ClientID %>');        
//        if(PredefinedViews.value != "")
//        {
//           //Update session
//	        UpdateSession("0", PredefinedViews.value, "0", "PredefinedView");
//        }
//        else
//        {
//            //Clear existing taxonomy selections
//            UpdateSession("0", "0", "0", "PredefinedView");
//        }
    }
    
    function AC_AddCountryToSelection()
    {    
        var taxonomyTypeID = "1";
        var taxonomyType = "Country";    
        var currentValue=$("#<%= hdnUserSelections.ClientID%>").val();           
        var fullid="1A";  
        
        //Get the selected item in the autocomplete textbox
        var taxonomyItem=getCountryItem();
        if(taxonomyItem)
        {
            addToSelection(fullid, taxonomyType, taxonomyItem.split("~")[0], escape(taxonomyItem.split("~")[1]),"false");        
            
            //prepare selection string to 'newSelection'
            newSelection = taxonomyTypeID+'~'+ taxonomyItem; 
            subTaxonomySelection =  fullid+'~'+ taxonomyItem; 

            if(fullid!=null && fullid != taxonomyTypeID)
            {
                newSelection = newSelection + '|' + subTaxonomySelection;
            }
         
            $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
            $("#<%= SearchCountryTextBox.ClientID%>").val("");
        }
        
        //alert($("#<%= hdnUserSelections.ClientID%>").val());
        return false;
    }
    
    function AC_AddIndicatorToSelection()
    {    
        var taxonomyTypeID = "2";
        var taxonomyType = "Indicator";    
        var currentValue=$("#<%= hdnUserSelections.ClientID%>").val();           
        var fullid="2";  
        
        //Get the selected item in the autocomplete textbox
        var taxonomyItem=getIndicatorItem();
        if(taxonomyItem)
        {  
            addToSelection("A"+taxonomyTypeID, "Search Indicators", taxonomyItem.split("~")[0], escape(taxonomyItem.split("~")[1]),"false");        
            //prepare selection string to 'newSelection'
            newSelection = "A"+taxonomyTypeID+'~'+ taxonomyItem; 
                
            $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
            $("#<%= SearchIndicatorTextBox.ClientID%>").val("");
        }
        
        //alert($("#<%= hdnUserSelections.ClientID%>").val());
        return false;
    }

</script>

<style type="text/css">
    .ui-autocomplete 
    { 
        text-align:left;
    }
</style>
<script type="text/javascript">
	var jdataCountry;
	
	    $(function() 
	    {
	        $(".tbCountry").autocomplete(
	            {
	                source: function(request, response) 
	                {
	                    $.ajax({
	                        url: "AutoCompleteWebService.asmx/FetchTaxonomyResults",
	                        data: "{ 'taxonomyTypeID': 1, 'searchString': '" + request.term + "'}",
	                        dataType: "json",
	                        type: "POST",
	                        contentType: "application/json; charset=utf-8",
	                        dataFilter: function(data) 
	                                    {
	                                        return data; 
	                                    },
	                        success: function(data) 
	                                    {
	                                        if(data.d.length<=0) alert("No results found");
	                                        $.map(data.d,function(item) 
	                                                    {
	                                                        if(jdataCountry==null) {jdataCountry="";}
	                                                        jdataCountry+="|"+item.ID+"~"+item.Name;    	                                    
	                                                    }
	                                                );
	                                
	                                        response($.map(data.d, function(item) 
	                                                                {
	                                                                return { value: item.Name	}
	                                                                }
	                                                        )
	                                                )
	                                    },
	                        error: function(XMLHttpRequest, textStatus, errorThrown) 
	                                        {
	                                            alert(textStatus);
	                                        }
	                    });
	                },
	                minLength: 2
	            });
	    });
	    function getCountryItemValue()
	    {	
	        if(jdataCountry)
	        {
                var str= jdataCountry.split("|");
                var txtval=$("#<%=SearchCountryTextBox.ClientID%>").val();
                for(i=0;i<str.length;i++)
                {
                    if(str[i].indexOf(txtval)>-1)
                    {
                        return str[i].split("~")[0];
                    }
                }
            }
	        return false;
	    }
	    function getCountryItemText()
	    {		
	        if(jdataCountry)
	        {
	            var str= jdataCountry.split("|");
	            var txtval=$("#<%=SearchCountryTextBox.ClientID%>").val();
	            for(i=0;i<str.length;i++)
	            {
	                if(str[i].indexOf(txtval)>-1)
	                {
	                    return str[i].split("~")[1];
	                }
	            }
	        }
	        return false;
	    }
	    function getCountryItem()
	    {		
	        if(jdataCountry)
	        {
	            var str= jdataCountry.split("|");
	            var txtval=$("#<%=SearchCountryTextBox.ClientID%>").val();
	            for(i=0;i<str.length;i++)
	            {
	                if(str[i].indexOf(txtval)>-1)
	                {	                
	                    return str[i];
	                }
	            }
	        }
	        return false;
	    }
	    function clearCountrySearchTextBox()
	    {
	        $("#<%=SearchCountryTextBox.ClientID%>").val("");	
	    }
	</script>
	<script type="text/javascript">
	var jdataIndicator;
	
	    $(function() 
	    {
	        $(".tbIndicator").autocomplete(
	            {
	                source: function(request, response) 
	                {
	                    $.ajax({
	                        url: "AutoCompleteWebService.asmx/FetchTaxonomyResults",
	                        data: "{ 'taxonomyTypeID': 2 , 'searchString': '" + request.term + "'}",
	                        dataType: "json",
	                        type: "POST",
	                        contentType: "application/json; charset=utf-8",
	                        dataFilter: function(data) 
	                                    {
	                                        return data; 
	                                    },
	                        success: function(data) 
	                                    {
	                                        if(data.d.length<=0) alert("No results found");
	                                        $.map(data.d,function(item) 
	                                                    {
	                                                        if(jdataIndicator==null) {jdataIndicator="";}
	                                                        jdataIndicator+="|"+item.ID+"~"+item.Name;    	                                    
	                                                    }
	                                                );
	                                
	                                        response($.map(data.d, function(item) 
	                                                                {
	                                                                return { value: item.Name	}
	                                                                }
	                                                        )
	                                                )
	                                    },
	                        error: function(XMLHttpRequest, textStatus, errorThrown) 
	                                        {
	                                            alert(textStatus);
	                                        }
	                    });
	                },
	                minLength: 2
	            });
	    });
	    function getIndicatorItemValue()
	    {	
	        if(jdataIndicator)
	        {
                var str= jdataIndicator.split("|");
                var txtval=$("#<%=SearchIndicatorTextBox.ClientID%>").val();
                for(i=0;i<str.length;i++)
                {
                    if(str[i].indexOf(txtval)>-1)
                    {
                        return str[i].split("~")[0];
                    }
                }
            }
	        return false;
	    }
	    function getIndicatorItemText()
	    {		
	        if(jdataIndicator)
	        {
	            var str= jdataIndicator.split("|");
	            var txtval=$("#<%=SearchIndicatorTextBox.ClientID%>").val();
	            for(i=0;i<str.length;i++)
	            {
	                if(str[i].indexOf(txtval)>-1)
	                {
	                    return str[i].split("~")[1];
	                }
	            }
	        }
	        return false;
	    }
	    function getIndicatorItem()
	    {		
	        if(jdataIndicator)
	        {
	            var str= jdataIndicator.split("|");
	            var txtval=$("#<%=SearchIndicatorTextBox.ClientID%>").val();
	            for(i=0;i<str.length;i++)
	            {
	                if(str[i].indexOf(txtval)>-1)
	                {	                
	                    return str[i];
	                }
	            }
	        }
	        return false;
	    }
	    function clearIndicatorSearchTextBox()
	    {
	        $("#<%=SearchIndicatorTextBox.ClientID%>").val("");	
	    }
	</script>             
    <div id="maincontent_full">
        
         <GMPT:PageTitleBar id="PageTitleBar1" runat="server"></GMPT:PageTitleBar>
         <div style="float:right;position:relative;margin-top:-35px"><GMPT:SavedSearch ID="SavedSearch1" runat="server"/> </div>
    <ul id="database_tabs">        
        <li><a href="Results/CountryOverview.aspx" title="go to country view">Country Snapshot</a></li>
        <li><a class="selected">Search</a></li>
        <li><asp:LinkButton ID="ResultsLink" runat="server" OnClientClick="javascript:return ValidateSelections();" OnClick="ResultsLink_Click" Text="View Results" ToolTip="click to view results"></asp:LinkButton></li>        
        <li><asp:LinkButton ID="AnalysisLink" runat="server" OnClientClick="javascript:return ValidateSelections();" OnClick="AnalysisLink_Click" Text="Chart Results" ToolTip="click to view chart results"></asp:LinkButton></li>                
        <GMPT:Glossary ID="Links" runat="server" />
        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       </li>
    </ul>
    
    <br />  

     <div id="maincontent" class="productscan_powersearch" style="width:900px;border-left-width:0px">
          <div class="powersearch_col" style="width:577px">
          
          <h1>1) Make selections</h1>
           <div style="display:none;"><ComponentArt:Grid ID="Grid1" runat="server" /></div> 
           
           <div id="CountrySelectionsDiv"  style="width:100%">
            <div style="width:100%">
                <h2 >Search Countries</h2>
                <div style="width:400px;float:left" >
                <div class="ui-widget" style="padding-bottom:5px" >
     <asp:TextBox ID="SearchCountryTextBox" CssClass="tbCountry" runat="server" Width="100%" />
</div></div>
                <div class="button_right" style="width:155px">
                    <a id="A1" style="width:155px;display:inline-block;" 
                        onclick="AC_AddCountryToSelection()" 
                        
                        href="#"
                        title="click to add selection">Add to Selection</a>            
                </div>
                <br />
                <br />
                </div>
                    <div class="hr"></div>
            </div>
            
           <div id="IndicatorSelectionsDiv"  style="width:100%">
            <div style="width:100%">
                <h2 >Search Indicators</h2>
                <div style="width:400px;float:left" >
                 <div class="ui-widget" style="padding-bottom:5px" >
                     <asp:TextBox ID="SearchIndicatorTextBox" CssClass="tbIndicator" runat="server" Width="100%" />
                </div></div>
                <div class="button_right" style="width:155px">
                    <a id="LinkButton1" style="width:155px;display:inline-block;" 
                        onclick="AC_AddIndicatorToSelection()" 
                        
                        href="#"
                        title="click to add selection">Add to Selection</a>            
                </div>
                <br />
                <br />
                </div>
                    
            </div>

           <ComponentArt:CallBack id="CallBack1" Debug="false" runat="server" OnCallback="CallBack1_Callback" PostState="true" >
            <Content>   
                <asp:Label ID="SelectedTaxonomy" runat="server" CssClass="H2Heading"></asp:Label>                
                
                <div id="controlHolder" runat="server">   
                </div>
            <asp:Label ID="NoteLabel" runat="server" ></asp:Label>
            
            </Content>
            <LoadingPanelClientTemplate>
              <table class="loadingpanel" width="100%" style="height:340" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
            </LoadingPanelClientTemplate>
          </ComponentArt:CallBack>
            <label id="lblNote"></label><br />
            <div class="button_right" style="width: 150px;float:left" >
                   <asp:LinkButton Width="150px" ID="serachLink" runat="server" Text="Detailed Search" 
                   ToolTip="click to go detailed search"
                   OnClick="BuidSearch_Click"></asp:LinkButton>
                </div>
             </div>

          <div class="powersearch_col2">
            <h1>2) Confirm your  Search
            </h1>
            <asp:Label ID="ResultsFoundStatus" runat="server" ForeColor="red" Text="No results found for the current selection."></asp:Label>
            <%--<span style="color: #DC671E;font-weight: bold;margin-bottom: 3px;text-transform: uppercase;font-size: 10px;">Predefined views</span>
            <asp:DropDownList ID="PredefinedViews" runat="server" 
                AutoPostBack="true" 
                OnSelectedIndexChanged="PredefinedViews_SelectedIndexChanged"
                width="300"
                Font-Size="11px">
            </asp:DropDownList>--%>

            <div style="position:relative; 
                        width:300px; scrollbar-face-color: #BFC4D1;
	                    scrollbar-shadow-color: #FFFFFF;
	                    scrollbar-highlight-color: #FFFFFF;
	                    scrollbar-3dlight-color: #FFFFFF;
	                    scrollbar-darkshadow-color: #FFFFFF;
	                    scrollbar-track-color: #FFFFFF;
	                    scrollbar-arrow-color: #FFFFFF;
	                    padding-bottom:30px;">
                <uc3:Selections ID="UcSelections" runat="server"/>
               <br />
               
            Start Year <asp:DropDownList ID="StartYear" runat="server" 
                                Width="80"
                                Font-Size="11px">
                            </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;End Year <asp:DropDownList ID="EndYear" runat="server" 
                    Width="80"
                    Font-Size="11px">
                </asp:DropDownList>
             </div>
            <div class="hr">
            </div>
            <div class="button_clear" style="width:105px"><a href="javascript:ClearSelections();" style="width:105px" title="clear all selections">clear all</a></div>
            <div class="button_right" style="width:150px">
            <asp:LinkButton ID="ResultsLink1" runat="server" Width="150px" 
                OnClientClick="javascript:return ValidateSelections();" 
                OnClick="ResultsLink_Click" 
                Text="View Results"
                ToolTip="click to view results"></asp:LinkButton>            
            </div>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <div class="button_right" style="width:150px">
            <asp:LinkButton ID="AnalysisLink1" runat="server" Width="150px" 
                OnClientClick="javascript:return ValidateSelections();" 
                OnClick="AnalysisLink_Click" 
                Text="Chart Results"
                ToolTip="click to view chart results"></asp:LinkButton>
            </div>
          </div>
      </div>

</div> 
            <input id="hdnControlType" type="hidden" value="" />
            <input id="hdnUserSelections" type="hidden" value="|" runat="server"/>
            <input id="hdnFullTaxonomyTypeID" runat="server" type="hidden" value="" />
            <input id="hdnparentIDs" runat="server" type="hidden" value="" />

<script language="javascript" type="text/javascript">

$(".static a").attr("href","#");
$(".static a").css("cursor","default");

</script>
</asp:Content>


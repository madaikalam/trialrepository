using System;
using System.IO;
using System.Net.Mail;
using System.Web;
using System.Text;
using System.Data;
using System.Configuration;
namespace Datamonitor.PremiumTools.Generic.Logging
{
    /// <summary>
    /// This class is designed to log errors and send mails regarding 
    /// runtime errors in the application.   
    /// </summary>
    public static class ErrorHandler
    {
        /// <summary>
        /// Formats and logs error
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">Exception</param>
        public static void SaveToEventLog(string message, Exception ex)
        {
            try
            {
                string strPhysicalApplicationPath = ConfigurationManager.AppSettings["ErrorLogPath"].ToString();
                string strFileName = strPhysicalApplicationPath + "IP_" + DateTime.Now.ToString("ddMMMyyyy", System.Globalization.CultureInfo.CurrentCulture) + ".txt";
                string strBody = string.Empty;
                FileInfo fInfo = new FileInfo(strFileName);
                StringBuilder sbMailbody = new StringBuilder();


                //Build the error message to log
                sbMailbody.Append(message + Environment.NewLine + Environment.NewLine);
                sbMailbody.Append("Server Address :: " + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + Environment.NewLine);
                sbMailbody.Append("User Address   :: " + HttpContext.Current.Request.ServerVariables["REMOTE_HOST"] + Environment.NewLine);
                sbMailbody.Append("Script Name    :: " + HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"] + Environment.NewLine);
                sbMailbody.Append("Query Data     :: " + HttpContext.Current.Request.Url.Query + Environment.NewLine);
                sbMailbody.Append("Occured At     :: " + DateTime.Now + Environment.NewLine + Environment.NewLine);
                sbMailbody.Append("################################## -- Start of Error  -- #################################" + Environment.NewLine);
                sbMailbody.Append(ex.StackTrace + Environment.NewLine);
                sbMailbody.Append("################################### -- End of Error -- ###################################" + Environment.NewLine + Environment.NewLine);
                sbMailbody.Append(HttpContext.Current.Request.ServerVariables["ALL_HTTP"] + Environment.NewLine);
                strBody = sbMailbody.ToString();

                if (fInfo.Exists)
                {
                    FileStream fStream = new FileStream(strFileName, FileMode.Append, FileAccess.Write);
                    StreamWriter sWriter = new StreamWriter(fStream);
                    sWriter.WriteLine(strBody);
                    sWriter.Close();
                }
                else
                {
                    FileStream fStream = new FileStream(strFileName, FileMode.Create, FileAccess.Write);
                    StreamWriter sWriter = new StreamWriter(fStream);
                    sWriter.WriteLine(strBody);
                    sWriter.Close();
                }
            }
            catch (Exception e)
            {
                throw e.InnerException;
            }
        }

        /// <summary>
        /// Sends mail to the person specified to recieve mails
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        public static int SendErrorMail(string message, Exception ex)
        {
            string EmailReciever = ConfigurationManager.AppSettings["ErrorMailReciever"].ToString();
            string EmailSender = ConfigurationManager.AppSettings["ErrorMailSender"].ToString();
            string ErrorMailSubject = ConfigurationManager.AppSettings["ErrorMailSubject"].ToString();
            string smtpServer = ConfigurationManager.AppSettings["SMTPServer"].ToString();
            StringBuilder sbMailbody = new StringBuilder();

            //Build the error message to log
            sbMailbody.Append(message + Environment.NewLine + Environment.NewLine);
            sbMailbody.Append("Server Address :: " + HttpContext.Current.Request.ServerVariables["SERVER_NAME"] + Environment.NewLine);
            sbMailbody.Append("User Address   :: " + HttpContext.Current.Request.ServerVariables["REMOTE_HOST"] + Environment.NewLine);
            sbMailbody.Append("Script Name    :: " + HttpContext.Current.Request.ServerVariables["SCRIPT_NAME"] + Environment.NewLine);
            sbMailbody.Append("Query Data     :: " + HttpContext.Current.Request.Url.Query + Environment.NewLine);
            sbMailbody.Append("Occured At     :: " + DateTime.Now + Environment.NewLine + Environment.NewLine);
            sbMailbody.Append("################################## -- Start of Error  -- #################################" + Environment.NewLine);
            sbMailbody.Append(ex.StackTrace + Environment.NewLine);
            sbMailbody.Append("################################### -- End of Error -- ###################################" + Environment.NewLine + Environment.NewLine);
            sbMailbody.Append(HttpContext.Current.Request.ServerVariables["ALL_HTTP"] + Environment.NewLine);

            MailMessage errorMessage = new MailMessage();
            errorMessage.To.Add(new MailAddress(EmailReciever));
            errorMessage.From = new MailAddress(EmailSender);
            errorMessage.Subject = ErrorMailSubject;
            errorMessage.IsBodyHtml = true;
            errorMessage.Body = sbMailbody.ToString();
            SmtpClient SmtpClient = new SmtpClient(smtpServer);
            try
            {
                SmtpClient.Send(errorMessage);
            }
            catch
            {
                throw;
            }
            return 1;
        }


    }
}

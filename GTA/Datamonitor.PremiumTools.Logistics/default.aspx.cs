using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic.Library;

namespace Datamonitor.PremiumTools.Logistics
{
    public partial class _default :AuthenticationBasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (ConfigurationManager.AppSettings.Get("LandingPage") != null)
            {
                Response.Redirect(ConfigurationManager.AppSettings.Get("LandingPage"));
            }
        }
    }
}

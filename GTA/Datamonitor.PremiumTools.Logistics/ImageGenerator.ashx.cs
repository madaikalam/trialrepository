using System;
using System.Data;
using System.IO;
using System.Web;
using System.Collections;
using System.Web.Services;
using System.Web.Services.Protocols;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Generic.Library
{
    public class ImageGenerator : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            int countryID;
            int rowID;
            int rowIndex;

            if (!string.IsNullOrEmpty(context.Request.QueryString["countryid"]) && !string.IsNullOrEmpty(context.Request.QueryString["rowid"]))
            {
                countryID = Convert.ToInt32(context.Request.QueryString["countryid"]);
                rowID = Convert.ToInt32(context.Request.QueryString["rowid"]);               

                context.Response.ContentType = "image/jpeg";

                rowIndex = context.Request.QueryString["index"] != null ? Convert.ToInt32(context.Request.QueryString["index"]) : -1;                    
              
                Stream strm = GetImageBinary(countryID, rowID, rowIndex);
                if (strm != null)
                {
                    byte[] buffer = new byte[4096];
                    int byteSeq = strm.Read(buffer, 0, 4096);
                    while (byteSeq > 0)
                    {
                        context.Response.OutputStream.Write(buffer, 0, byteSeq);
                        byteSeq = strm.Read(buffer, 0, 4096);
                    }
                }
            }
        }

        public Stream GetImageBinary(int countryID, int rowID, int rowIndex)
        {
           
            try
            {
                DataSet ds = SqlDataService.GetCountryOverviewKSData(rowID,countryID);
                string values = string.Empty;

                for(int i=0; i< ds.Tables[0].Columns.Count; i++)
                {
                    values = ds.Tables[0].Rows[0][i].ToString().Length > 0 ?
                        values + ds.Tables[0].Rows[0][i].ToString() + "," :
                        values + "0,";
                }
                values = values.Substring(0, values.Length - 1);

                ImageHelper img = new ImageHelper();

                byte[] bytes = rowIndex != -1 ? img.CreateImage("Bars", values, rowIndex) : img.CreateImage("Bars", values);
                
                return new MemoryStream(bytes);
            }
            catch
            {
                return null;
            }
           
        }   


        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}



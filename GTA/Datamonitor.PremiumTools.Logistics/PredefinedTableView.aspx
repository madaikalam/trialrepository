<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PredefinedTableView.aspx.cs" 
Inherits="Datamonitor.PremiumTools.Logistics.PredefinedTableView" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Predefined Table View</title>
         
    <link href="~/assets/css/treeview.css" rel="stylesheet" type="text/css"/>
    <link href="~/assets/css/gridStyle.css" rel="stylesheet" type="text/css"/>
     <link href="~/assets/css/datamonitor.css" rel="stylesheet" type="text/css"/>
     
</head>
<body style="background-color:White;background-image:none;margin:0px;padding:0px 0px 0px 5px">
    <form id="form1" runat="server">
     
    <h3><asp:Label ID="ChartHeading" runat="server"></asp:Label></h3><br />
    <div style="font-size: 11px;margin:0px;overflow:auto;padding:0px">   
        
        <asp:GridView ID="PredefinedTableGrid" runat="Server"        
        CellPadding="4"
        CellSpacing="2"
            Width="700"
            OnRowDataBound="PredefinedTableGrid_RowDataBound">
        <HeaderStyle BackColor="#cccccc" Height="50px" VerticalAlign="top" HorizontalAlign="left" />
        <RowStyle BackColor="#f8f8f8" BorderWidth="0" />
        <AlternatingRowStyle BackColor="#f8f8f8" BorderWidth="0" />        
        <Columns>
        
        </Columns>
        </asp:GridView>
    </div>
    
    </form>
</body>
</html>


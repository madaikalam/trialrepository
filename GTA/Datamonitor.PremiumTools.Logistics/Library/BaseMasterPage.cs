using System;
using System.Web;
using System.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Library
{
    /// <summary>
    /// This class can be inherited by the master pages if user informtion is required
    /// </summary>
    public abstract class BaseMasterPage : MasterPage
    {
        //PUBLIC PROPERTIES

        public string KCUserID
        {
            get { return Session["KCUserId"] == null ? null : Session["KCUserId"].ToString(); }
        }

        public string SourceKC
        {
            get { return Session["SourceKC"] == null ? null : Session["SourceKC"].ToString(); }
        }

        public string NoHeader
        {
            get { return Session["noheader"] == null ? null : Session["noheader"].ToString(); }
        }

        public string H1Class
        {
            get
            {
                string KCName = string.Empty;
                switch (SourceKC)
                {
                    case "retail":
                    case "auto":
                    case "consumer":
                    case "fs":
                    case "forecourt":
                    case "logistics":
                    case "news_and_deals":
                    case "pharma":
                    case "ps":
                    case "utilities":
                    case "sourcing":
                        KCName = "orbys";
                        break;
                    case "technology":
                    case "bgcio":
                        KCName = "technology";
                        break;
                    /* Ovum branded sites.. */
                    case "ovumit":
                    case "vendor":
                    case "telecoms":
                    case "enterprise":
                        KCName = "ovum";
                        break;
                    default:
                        break;
                }
                return KCName;
            }
        }
        public string KCFullName
        {
            get
            {
                string KCName = string.Empty;
                switch (SourceKC)
                {
                    case "retail":
                        KCName = "Retail Knowledge Centre";
                        break;
                    case "auto":
                        KCName = "Automotive Knowledge Centre";
                        break;
                    case "consumer":
                        KCName = "Consumer Knowledge Centre";
                        break;
                    case "fs":
                        KCName = "Financial Services Knowledge Centre";
                        break;
                    case "forecourt":
                        KCName = "Forecourt Retailing Knowledge Centre"; ;
                        break;
                    case "logistics":
                        KCName = "Logistics and Express Parcels Knowledge Centre";
                        break;
                    case "news_and_deals":
                        KCName = "News and Deals Knowledge Centre";
                        break;
                    case "pharma":
                        KCName = "Pharmaceuticals and Healthcare Knowledge Centre";
                        break;
                    case "ps":
                        KCName = "Professional Services Knowledge Centre";
                        break;
                    case "technology":
                        KCName = "Technology Knowledge Centre";
                        break;
                    case "utilities":
                        KCName = "Utilities Knowledge Centre";
                        break;
                    case "sourcing":
                        KCName = "Orbys Black Book of Sourcing Knowledge Centre";
                        break;
                    /* Ovum branded sites.. */
                    case "ovumit":
                        KCName = "Ovum IT Knowledge Centre";
                        break;
                    case "vendor":
                        KCName = "Ovum Technology and Industries Knowledge Centre";
                        break;
                    case "telecoms":
                        KCName = "Telecoms Knowledge Centre";
                        break;
                    case "enterprise":
                        KCName = "Ovum Enterprise IT Knowledge Centre";
                        break;
                    default:
                        break;
                }
                return KCName;
            }
        }

        public string KCUrl
        {
            get
            {
                string KCUrl = string.Empty;
                switch (SourceKC)
                {
                    case "retail":
                    case "auto":
                    case "bgcio":
                    case "consumer":
                    case "fs":
                    case "forecourt":
                    case "logistics":
                    case "news_and_deals":
                    case "pharma":
                    case "ps":
                    case "technology":
                    case "utilities":
                    case "sourcing":
                        KCUrl = GlobalSettings.KCRedirectUrl;
                        break;
                    /* Ovum branded sites.. */
                    case "ovumit":
                    case "vendor":
                    case "telecoms":
                    case "enterprise":
                        KCUrl = GlobalSettings.OvumKCRedirectUrl;
                        break;
                    default:
                        break;
                }
                return KCUrl;
            }
        }
        protected override void OnInit(EventArgs e)
        {
            //Get current page
            string CurentPage = Request.Url.Segments[Request.Url.Segments.Length - 1];
            if (!CurentPage.Equals(GlobalSettings.SearchPage, StringComparison.CurrentCultureIgnoreCase))
            {
                if (HttpContext.Current.Session["sessionkey"] == null)
                {
                    //HttpContext.Current.Session["sessionkey"] = true;
                    Response.Redirect(GlobalSettings.SearchPage, true);
                }
            }
        }
    }
}

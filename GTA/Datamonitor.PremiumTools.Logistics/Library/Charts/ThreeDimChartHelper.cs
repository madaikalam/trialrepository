using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Security;
using System.Drawing;
using ComponentArt.Web.Visualization.Charting;
using System.Text;
using System.IO;


namespace Datamonitor.PremiumTools.Generic.Library
{
    public class ThreeDimensionalCharts
    {       
        /// <summary>
        /// Plots the XY chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>
        internal static Chart PlotXYChart(DataSet chartDataset, 
                                    Chart chart, 
                                    string chartSeriesType,
                                    string yAxisText,
                                    string AxisCriteria)
        {
            BuildAxisCriteria(chart, AxisCriteria);
            //Generate GUID
            string gId = System.Guid.NewGuid().ToString();
            //Place the charts in a specified path with Guid.            
            chart.ImageOutputDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
            chart.CustomImageFileName = "Chart_" + gId + ".png";
            chart.CacheInterval = 0;
            //To store the image at temperory path some time.
            chart.DeletionDelay = 8640000;           

            chart.Series.Clear();
            chart.Visible = false;
            Series s = new Series("S0");
            s.RemoveParentNameFromLabel = true;
            chart.Series.Add(s);
            //s.Depth = 4;
            chart.MainStyle = chartSeriesType;
            Series s1 = new Series("PreSeries");
            
            chart.DefineValue("x", chartDataset.Tables[0].Columns[0]);
            chart.DefineValue("y", chartDataset.Tables[0].Columns[2]);
            chart.DefineValue("series", chartDataset.Tables[0].Columns[1]);

            chart.ResizeMarginsToFitLabels = false;

            chart.Width = Unit.Pixel(530);
            chart.Height = Unit.Pixel(300);
            chart.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(chart.Width.Value), Convert.ToInt32(chart.Height.Value));

            //Setting chart label's font.                        
            chart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("Arial", 8);
            chart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

            if (chartSeriesType.Equals("Doughnut"))
            {
                // Create labels
                s.Labels.Add(new SeriesLabels());
                // Change the datapoint font color
                chart.DataPointLabelStyles[0].ForeColor = System.Drawing.Color.White;
                chart.DataPointLabelStyles[0].Font = new System.Drawing.Font("Arial", 8);
                // Add the shadow
                chart.DataPointLabelStyles[0].ShadowDepthPxl = 1;
                chart.Legend.Visible = true;
            }
            chart.DataBind();

            SetStandardLegendStyles(chart);

            if (!chartSeriesType.Equals("Doughnut"))
            {
                try
                {
                    AxisAnnotation xAnnotation = chart.CoordinateSystem.XAxis.AxisAnnotations["X@Zmax"];
                    AxisAnnotation yAnnotation = chart.CoordinateSystem.YAxis.AxisAnnotations["Y@Zmax"];
                    xAnnotation.AxisTitle = chartDataset.Tables[0].Columns[0].ColumnName;
                    yAnnotation.AxisTitle = yAxisText;   //chartDataset.Tables[0].Columns[2].ColumnName;
                }
                catch (Exception ex) { }
            }

            ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();
            chart.Palettes.Add(CorpColorPalette);
            chart.SelectedPaletteName = "CorpPalette";

            return chart;
        }       

        /// <summary>
        /// Plots the Pie chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>        
        internal static Chart PlotPieChart(DataSet chartDataset, 
                                        Chart targetChart, 
                                        string chartSeriesType,
                                        string AxisCriteria)
        {
            //BuildAxisCriteria(targetChart, AxisCriteria);
            //To store the chart at a temperory path.
            string gId = System.Guid.NewGuid().ToString();                   
            targetChart.ImageOutputDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
            targetChart.CustomImageFileName = "Chart_" + gId + ".png";            
            targetChart.CacheInterval = 0;            
            targetChart.DeletionDelay = 864000000;
            
            targetChart.Legend.Visible = true;
            //To set backColor of a chart.
            targetChart.BackColor = Color.White;
            // targetChart.AdjustReferenceValue = true;
            //Apply chart width,height.
            targetChart.Width = Unit.Pixel(500);
            targetChart.Height = Unit.Pixel(300);
            //targetChart.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(targetChart.Width.Value), Convert.ToInt32(targetChart.Height.Value));
            //targetChart.View.ViewDirection = new Vector3D(1,15,10);  //(5,30,5); //5, 30, 20new Vector3D(1, 20, 20);
            targetChart.View.ViewDirection = new Vector3D(1, 20, 15);
            //To clear the series and legend of Chart.
            targetChart.Series.Clear();
            targetChart.Legend.Clear();
            //Series creation
            Series s = new Series("S0");
            targetChart.Series.Add(s);
            //Define axis to chart.
            targetChart.DefineValue("x", chartDataset.Tables[0].Columns[0]);
            targetChart.DefineValue("y", chartDataset.Tables[0].Columns[1]);

            targetChart.ResizeMarginsToFitLabels = false;                     
            //Apply label styles.
            SeriesLabels labs = new SeriesLabels();
            //labs.LabelExpression = "''+ Format(y,'##,###')";
            //labs.LabelExpression = "' '+ Format(y+ (y / sum(y)), '0.0%')"; 
            labs.LabelExpression = "' '+ Format(y / sum(y), '0%')";
            labs.LabelStyleKind = DataPointLabelStyleKind.InsideRadial;
            s.Labels.Add(labs);
            
            //targetChart.DataPointLabelStyles[0].Font = new System.Drawing.Font("Arial", 9, FontStyle.Regular);
            //targetChart.DataPointLabelStyles[0].ForeColor = System.Drawing.Color.Gray;            
      
            targetChart.MainStyle = "Pie";  
            targetChart.DataBind();

            // Customize labels style
            DataPointLabelStyle LS = targetChart.DataPointLabelStyles[0];
            LS.ForeColor = System.Drawing.Color.Gray;
            LS.Font = new System.Drawing.Font("Arial", 8);

            //targetChart.View.Margins.Left = 15;
            //Define Palette
            ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();
            targetChart.Palettes.Add(CorpColorPalette);
            targetChart.SelectedPaletteName = "CorpPalette";
            //Apply LEGEND styles
            SetStandardLegendStyles(targetChart);          
            
            return targetChart;
        }

        /// <summary>
        /// Plots the DualYAxis chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>        
        internal static Chart PlotDualYAxisChart(DataSet chartDataset, 
                                        Chart chart, 
                                        string chartSeriesType,
                                        string yAxis1Text, 
                                        string yAxis2Text)
        {
            //To store the chart at a temperory path.
            string gId = System.Guid.NewGuid().ToString();                  
            chart.ImageOutputDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
            chart.CustomImageFileName = "Chart_" + gId + ".png";
            chart.CacheInterval = 0;          
            chart.DeletionDelay = 864000000;
            chart.Legend.Visible = false;
            chart.Series.Clear(); 
            //Define axis to chart.
            int y1AxisValue = 0;
            int y2AxisValue = 0;

            chart.DefineValue("x", chartDataset.Tables[0].Columns[0]);            
            for (int i = 0; i < chartDataset.Tables[0].Rows.Count; i++)
            {
                if (chartDataset.Tables[0].Rows[i][1].ToString() != "")
                {
                    y1AxisValue += 1;
                }
            }
            for (int i = 0; i < chartDataset.Tables[0].Rows.Count; i++)
            {
                if (chartDataset.Tables[0].Rows[i][2].ToString() != "")
                {
                    y2AxisValue += 1;
                }
            }            
            if (y1AxisValue != 0)
            {
                Series s0 = new Series("S0");
                chart.Series.Add(s0);
                chart.DefineValue("y", chartDataset.Tables[0].Columns[1]);
                s0.RemoveParentNameFromLabel = true;
            }
            ////We can't plot Line chart with one series point.so we are checking y2AxisValue >1 or not.
            if (y2AxisValue > 1) 
            {
                Series s1 = new Series("S1");
                chart.Series.Add(s1);
                chart.DefineValue("S1:y", chartDataset.Tables[0].Columns[2]);
                s1.HasIndependentYAxis = true;
                s1.RemoveParentNameFromLabel = true;
                s1.StyleKind = SeriesStyleKind.Line;
            }

            chart.ResizeMarginsToFitLabels = false;
            //Set chart height,width
            chart.Width = Unit.Pixel(530);
            chart.Height = Unit.Pixel(300);
            chart.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(chart.Width.Value), Convert.ToInt32(chart.Height.Value));

            //Setting chart label's font.                        
            chart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("verdana", 7);
            chart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

            chart.DataBind();
      
            //Chart Annotations.
            AxisAnnotation xAnnotation = chart.CoordinateSystem.XAxis.AxisAnnotations["X@Zmax"];
            AxisAnnotation yAnnotation = chart.CoordinateSystem.YAxis.AxisAnnotations["Y@Zmax"];
            if (xAnnotation != null && yAnnotation != null)
            {
                xAnnotation.AxisTitle = chartDataset.Tables[0].Columns[0].ColumnName;
                yAnnotation.AxisTitle = yAxis1Text;                  
            }
            AxisAnnotation y1Annotation = chart.CoordinateSystem.YAxis.AxisAnnotations["Z@Ymin"];
            if (y1Annotation != null)
            {
                y1Annotation.AxisTitle = yAxis2Text;
            }           
            //Apply chart palette.
            ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();
            chart.Palettes.Add(CorpColorPalette);
            chart.SelectedPaletteName = "CorpPalette";       

            return chart;
        }
        
        /// <summary>
        /// Plots the Bubble chart.
        /// </summary>
        /// <param name="chartDataset">The chart dataset.</param>
        /// <param name="chart">The chart object.</param>        
        internal static Chart PlotBubbleChart(DataSet chartDataset, 
                                        Chart chart, 
                                        string chartSeriesType,
                                        string xAxisText,
                                        string yAxisText)
        {   int yAxisValue = 0;
            int bubbleValue = 0;
            //chart.DefineValue("x", chartDataset.Tables[0].Columns[0]);

            for (int i = 0; i < chartDataset.Tables[0].Rows.Count; i++)
            {
                if (chartDataset.Tables[0].Rows[i][2].ToString() != "")
                {
                    yAxisValue += 1;
                }               
            }
            for (int i = 0; i < chartDataset.Tables[0].Rows.Count; i++)
            {   
                if ((chartDataset.Tables[0].Rows[i][2].ToString() != "") && (chartDataset.Tables[0].Rows[i][3].ToString() == ""))
                {
                    bubbleValue += 1;
                }
            }   

            if (yAxisValue != 0)
            {
                //To store the chart at a temperory path.
                string gId = System.Guid.NewGuid().ToString();
                chart.ImageOutputDirectory = HttpContext.Current.Server.MapPath("~/TempImages");
                chart.CustomImageFileName = "Chart_" + gId + ".png";
                chart.CacheInterval = 0;
                chart.DeletionDelay = 864000000;  

                Series S = new Series("S0");
                chart.Series.Add(S);

                chart.DefineValue("x", chartDataset.Tables[0].Columns[0]);
                chart.DefineValue("y", chartDataset.Tables[0].Columns[2]);

                if (bubbleValue == 0)
                {
                    chart.DefineValue("S0:size", chartDataset.Tables[0].Columns[3]);
                }               

                S.StyleKind = SeriesStyleKind.Bubble;
                chart.ResizeMarginsToFitLabels = false;
                //To set chart height,width
                chart.Width = Unit.Pixel(530);
                chart.Height = Unit.Pixel(300);
                chart.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(chart.Width.Value), Convert.ToInt32(chart.Height.Value));
                //Setting chart label's font.                        
                chart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("verdana", 7);
                chart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

                chart.DataBind();

                //chart annotations
                AxisAnnotation xAnnotation = chart.CoordinateSystem.XAxis.AxisAnnotations["X@Zmax"];
                AxisAnnotation yAnnotation = chart.CoordinateSystem.YAxis.AxisAnnotations["Y@Zmax"];
                if (xAnnotation != null && yAnnotation != null)
                {
                    xAnnotation.AxisTitle = xAxisText;
                    yAnnotation.AxisTitle = yAxisText;
                }
                //Apply Chart palette
                ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();
                chart.Palettes.Add(CorpColorPalette);
                chart.SelectedPaletteName = "CorpPalette";
            }
            else
            {
                chart = null;
            }
            return chart;
        }

        /// <summary>
        /// Sets the standard legend styles.
        /// </summary>
        /// <param name="objChart">The obj chart.</param>
        private static void SetStandardLegendStyles(Chart chart)
        {
            System.Drawing.Font f = new System.Drawing.Font("Arial", 10, FontStyle.Regular);            
            chart.Legend.BackColor = Color.White;           
            chart.Legend.DrawBackgroundRectangle = true;
            chart.Legend.BorderColor = Color.White;            
            chart.Legend.Font = f;
            chart.Legend.FontColor = Color.Black;           
            chart.Legend.LegendLayout = LegendKind.Column;
            chart.Legend.LegendPosition = LegendPositionKind.CenterRight;
            chart.Legend.DrawBackgroundRectangle = false;
            chart.Legend.SharesChartArea = false;
        }

        /// <summary>
        /// Build Color Palette.
        /// </summary>
        /// <returns>Palette</returns>
        private static Palette buildPalette()
        {
            Palette CorpColorPalette = new Palette();
            CorpColorPalette.Name = "CorpPalette";
            CorpColorPalette.PrimaryColors = ChartColorCollection.FromString("002252,6c80bf,a5aecc,d2d9e5,d84519,ff6600,ffac70,fadec7,ff9900,ffcc00");
            return CorpColorPalette;
        }

        /// <summary>
        /// Apply standard styles to chart.
        /// </summary>
        /// <param name="targetChart">chart</param>
        private static void setStandardChartStyles(Chart targetChart)
        {
            //To clear Gridlines,Stripsets.
            targetChart.CoordinateSystem.PlaneXY.StripSets.Clear();
            targetChart.CoordinateSystem.PlaneXY.Grids.Clear();
            targetChart.CoordinateSystem.PlaneYZ.StripSets.Clear();
            targetChart.CoordinateSystem.PlaneYZ.Grids.Clear();
            targetChart.CoordinateSystem.PlaneZX.StripSets.Clear();
            targetChart.CoordinateSystem.PlaneZX.Grids.Clear();

            //Setting X-Axis title.
            AxisAnnotation xAnnotation = targetChart.CoordinateSystem.XAxis.AxisAnnotations["X@Zmax"];
            xAnnotation.LabelStyleName = "Sai";
            xAnnotation.RotationAngle = 20;
            xAnnotation.AxisTitleOffsetPts = 60.0;
            xAnnotation.AxisTitlePositionKind = AxisTitlePositionKind.AtMiddlePoint;
            xAnnotation.LabelStyleKind = LabelStyleKind.DefaultAxisLabels;
            xAnnotation.AxisTitle = "Industries";
            //xAnnotation.HOffset = 50D;            

            //Create Y-Axis
            AxisAnnotation yAnnotation = targetChart.CoordinateSystem.YAxis.AxisAnnotations["Y@Zmax"];
            yAnnotation.LabelStyleName = "Sai";
            yAnnotation.AxisTitleOffsetPts = 60.0;
            yAnnotation.AxisTitle = "data";

        }

        /// <summary>
        /// Plots the XYChart with scrolling.
        /// </summary>
        /// <param name="chartDataset">chartDataset</param>
        /// <param name="targetChart">targetChart</param>
        /// <param name="ScrollChart">ScrollChart</param>
        /// <param name="chartSeriesType">chartSeriesType</param>
        internal static void PlotXYScrollChart(DataSet chartDataset, 
                                       Chart targetChart, 
                                       Chart ScrollChart, 
                                       string chartSeriesType)
        {
            try
            {
                ComponentArt.Web.Visualization.Charting.Palette CorpColorPalette = buildPalette();

                targetChart.Clientside.ClientsideApiEnabled = true;
                targetChart.Clientside.LoadingChartImagePath = "../Assets/skins/arcticwhite/loading.gif";
                targetChart.Clientside.MyScrollControl = ScrollChart;

                //select the High-speed rendering engine [OPTIONAL]
                targetChart.GeometricEngineKind = GeometricEngineKind.HighSpeedRendering;

                //Make Y-axis start at the lowest relevant value
                targetChart.AdjustReferenceValue = false;

                targetChart.Series.Clear();
                targetChart.Visible = false;
                Series s = new Series("S0");
                s.RemoveParentNameFromLabel = true;
                targetChart.Series.Add(s);
                //s.Depth = 4;
                targetChart.MainStyle = chartSeriesType;
                Series s1 = new Series("PreSeries");
                
                targetChart.DefineValue("x", chartDataset.Tables[0].Columns[0]);
                targetChart.DefineValue("y", chartDataset.Tables[0].Columns[2]);
                targetChart.DefineValue("series", chartDataset.Tables[0].Columns[1]);

                targetChart.ResizeMarginsToFitLabels = false;

                targetChart.Width = Unit.Pixel(530);
                targetChart.Height = Unit.Pixel(300);
                //Chart1.View.NativeSize = new System.Drawing.Size(Convert.ToInt32(Chart1.Width.Value), Convert.ToInt32(Chart1.Height.Value));
                targetChart.View.Perspective = 57;
                targetChart.View.ViewDirection = new Vector3D(0, 0, 23.3);
                targetChart.CoordinateSystem.PlaneYZ.Visible = false;
                targetChart.View.Margins.Left = 5;
                targetChart.View.Margins.Right = 5;
                //Setting chart label's font.                        
                targetChart.LabelStyles["DefaultAxisLabels"].Font = new System.Drawing.Font("Arial", 8);
                targetChart.LabelStyles["DefaultAxisLabels"].ForeColor = System.Drawing.Color.Black;

                if (chartSeriesType.Equals("Doughnut"))
                {
                    // Create labels
                    s.Labels.Add(new SeriesLabels());
                    // Change the datapoint font color
                    targetChart.DataPointLabelStyles[0].ForeColor = System.Drawing.Color.White;
                    targetChart.DataPointLabelStyles[0].Font = new System.Drawing.Font("Arial", 8);
                    // Add the shadow
                    targetChart.DataPointLabelStyles[0].ShadowDepthPxl = 1;
                    targetChart.Legend.Visible = true;
                }

                //chart2
                ScrollChart.Series.Clear();

                //configure the look of the control chart
                ScrollChart.Clientside.IsScrollControll = true;
                ScrollChart.Clientside.ScrollImagesDirectoryPath = "../Assets/skins/arcticwhite/scroller/";
                ScrollChart.Clientside.ControlResizeButtonWidthPx = 14;
                ScrollChart.Clientside.ScrollControllHeight = 16;
                ScrollChart.Clientside.ScrollShadowColor = "#000";
                ScrollChart.Clientside.ScrollShadowOpacity = 0.3;
                ScrollChart.Clientside.ScrollStepPercentage = 0.5;
                ScrollChart.Clientside.RangeClientTemplateYoffset = 3;

                ClientTemplate ClientTemplate1 = new ClientTemplate();
                ClientTemplate1.Text = "<div class='popup'><div class='top'><div class='inner'><div class='lbl'>From:<br />To:</div><div class='val'>## DataItem.From ##<br />## DataItem.To ##</div></div></div><div class='btm'><div class='inner'><div class='lbl'>Range:</div><div class='val'>## DataItem.Range ##</div></div></div></div>";
                ScrollChart.ClientTemplates.Add(ClientTemplate1);

                ScrollChart.SelectedPaletteName = "Cool Blue";
                ScrollChart.AdjustReferenceValue = true;

                //create the chart
                Series S0a = new Series("Dow");
                ScrollChart.Series.Add(S0a);

                ScrollChart.View.Kind = ProjectionKind.TwoDimensional;
                ScrollChart.Width = 500;
                ScrollChart.Height = 70;

                ScrollChart.DefineValue("x", chartDataset.Tables[0].Columns[0]);
                ScrollChart.DefineValue("y", chartDataset.Tables[0].Columns[2]);
                ScrollChart.DefineValue("series", chartDataset.Tables[0].Columns[1]);

                ScrollChart.MainStyle = chartSeriesType;

                ScrollChart.View.Margins.Left = (15.0 / 500) * 100;
                ScrollChart.View.Margins.Right = (15.0 / 500) * 100;
                ScrollChart.View.Margins.Top = 0;
                ScrollChart.View.Margins.Bottom = 0;

                ScrollChart.CoordinateSystem.XAxis.RoundValueRange = false;

                //create larger labels for zoom chart and position them above X-axis
                LabelStyle ZoomStyle2 = new LabelStyle("ZoomStyle2");
                ZoomStyle2.ReferencePoint = TextReferencePoint.CenterBottom;
                ZoomStyle2.VOffset = 5;
                ZoomStyle2.Font = new System.Drawing.Font("Tahoma", 55);

                ScrollChart.LabelStyles.Add(ZoomStyle2);

                ScrollChart.DataBind();

                targetChart.DataBind();

                if (!chartSeriesType.Equals("Doughnut"))
                {
                    try
                    {
                        AxisAnnotation xAnnotation = targetChart.CoordinateSystem.XAxis.AxisAnnotations["X@Zmax"];
                        AxisAnnotation yAnnotation = targetChart.CoordinateSystem.YAxis.AxisAnnotations["Y@Zmax"];
                        xAnnotation.AxisTitle = chartDataset.Tables[0].Columns[0].ColumnName;
                        yAnnotation.AxisTitle = chartDataset.Tables[0].Columns[2].ColumnName;
                        //xAnnotation.TextOrientation = TextOrientation.YXOrientation;
                    }
                    catch (Exception ex) { }
                }

                targetChart.SelectedPaletteName = "Cool Blue";
            }
            catch (Exception ex) 
            {
                
            }
        }       
        
        private static void BuildAxisCriteria(Chart targetChart,string AxisCriteria)
        {
            ChartTitle title = new ChartTitle(AxisCriteria);
            title.RectangleLeftMargin = 5;
            title.RectangleRightMargin = 5;
            title.Font = new System.Drawing.Font("verdana",8, System.Drawing.FontStyle.Regular);
            targetChart.Titles.Add(title);
        }
    }
}

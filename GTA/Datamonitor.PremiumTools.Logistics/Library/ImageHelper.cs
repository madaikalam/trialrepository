using System;
using System.Data;
using System.Web;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using Microsoft.VisualBasic;

namespace Datamonitor.PremiumTools.Generic.Library
{
    public class ImageHelper
    {
        string sBgColor = "white";
        string sAvgLineColor = "gray";
        string sLineColor = "000000";
        string sStdDevColor = "dcdcdc";
        bool bStdDev = true;
        string sData = "";


        public decimal iImageWidth = 100;
        public decimal iImageHeight = 20;
        public decimal iTopMargin = 5;
        public decimal iBottomMargin = 5;
        public decimal iLeftMargin = 5;
        public decimal iRightMargin = 30;
        public decimal iMax = decimal.Parse("202796.3959");
        public decimal iMin = decimal.Parse("44466.9806");
        public decimal iAvg = 0;
        public decimal iSum = 0;
        public decimal iStdDev = 0;
        public decimal ibargap = 1;
        private void SetAvg(string[] arrDataPoints)
        {
            // Here we are calclating the sum of all the values and also finding the max and min values. 
            //we also calculate the average value;            

            for (int i = 0; i <= arrDataPoints.Length - 1; i++)
            {
                string sValue = arrDataPoints[i].ToString();
                decimal iValue = 0;
                if (sValue != "" && Information.IsNumeric(sValue))
                {
                    iValue = decimal.Parse(sValue);
                }

                iSum += iValue;
                if (i == 0)
                {
                    iMax = iValue;
                    iMin = iValue;
                }
                else
                {
                    if (iMax < iValue)
                        iMax = iValue;
                    if (iMin > iValue)
                        iMin = iValue;
                }
            }

            iAvg = iSum / arrDataPoints.Length;

            double iVar = 0;

            if (bStdDev)
            {

                for (int i = 0; i <= arrDataPoints.Length - 1; i++)
                {
                    string sValue = arrDataPoints[i].ToString();
                    decimal iValue = 0;
                    if (sValue != "" & Information.IsNumeric(sValue))
                    {
                        iValue = decimal.Parse(sValue);
                    }

                    iVar += double.Parse(Math.Pow(double.Parse(iValue.ToString()) - double.Parse(iAvg.ToString()), 2).ToString());
                }

                iStdDev = decimal.Parse(Math.Sqrt(double.Parse(iVar.ToString()) / double.Parse(arrDataPoints.Length.ToString())).ToString());

            }

        }

        public byte[] CreateImage(string chartType, string strDataPoints)
        {
            string sData = "";
            string[] arrDataPoints = strDataPoints.Split(',');

            sData = strDataPoints;
            //int iWidth = 100;
            //int iHeight = 20;
            decimal d;
            sBgColor = "white";
            sAvgLineColor = "Red";
            sLineColor = "Black";
            iTopMargin = 2;
            iBottomMargin = 2;
            iLeftMargin = 5;
            iRightMargin = 5;

            int zeroline = -10;
            SetAvg(arrDataPoints);

            Point[] oPoints = new Point[arrDataPoints.Length];
            decimal iScale;
            if (iMin > 0)
            {
                iMin = iMin - (iMin * Convert.ToDecimal("0.2"));
            }
            if (iMax != iMin)
                iScale = (iImageHeight - (iTopMargin + iBottomMargin)) / Math.Abs(iMax - iMin);
            else
                iScale = 0;
            int iStepWidth = Convert.ToInt32(Math.Floor((iImageWidth - (iLeftMargin + iRightMargin) - (ibargap * (arrDataPoints.Length - 1))) / (arrDataPoints.Length)));
            zeroline = Convert.ToInt32(iMax * iScale) + 2;

            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap oBitmap = new Bitmap(int.Parse(iImageWidth.ToString()), int.Parse(iImageHeight.ToString()));            
            oBitmap.MakeTransparent(GetColor(sBgColor));
            System.Drawing.Pen oPen = new System.Drawing.Pen(GetColor(sLineColor));
            System.Drawing.Pen oAvgPen = new System.Drawing.Pen(Color.Gray);
            Graphics oGraphics = Graphics.FromImage(oBitmap);
            Rectangle[] recs = new Rectangle[arrDataPoints.Length];

            oGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            oGraphics.FillRectangle(new SolidBrush(GetColor(sBgColor)), 0, 0, float.Parse(iImageWidth.ToString()), float.Parse(iImageHeight.ToString()));


            int prevX = Convert.ToInt32(iImageHeight - iBottomMargin), prevY = Convert.ToInt32(iImageHeight - iBottomMargin);

            if (!double.IsInfinity(double.Parse(iScale.ToString())))
            {
                for (int i = 0; i <= arrDataPoints.Length - 1; i++)
                {
                    string sValue = arrDataPoints[i].ToString();
                    decimal iValue = 0;
                    if (sValue != "" & Information.IsNumeric(sValue))
                    {
                        iValue = decimal.Parse(sValue);
                    }

                    decimal x = (i * iStepWidth) + (i * ibargap) + iLeftMargin;
                    decimal y;
                    if (iValue != 0)
                    {
                        y = iImageHeight - (Math.Abs(iValue - iMin) * iScale) - iBottomMargin;

                        d = decimal.Round(decimal.Parse(y.ToString()), 0);
                        y = decimal.Parse(d.ToString());
                    }
                    else
                        y = 0;

                    d = decimal.Round(decimal.Parse(x.ToString()), 0);
                    x = decimal.Parse(d.ToString());

                    if (chartType == "Lines")
                    {
                        if (y == 0)
                        {
                            oPoints[i] = new Point(prevX, prevY);
                        }
                        else
                        {
                            oPoints[i] = new Point(int.Parse(x.ToString()), int.Parse(y.ToString()));
                            prevX = int.Parse(x.ToString());
                            prevY = int.Parse(y.ToString());
                        }
                    }
                    else
                    {
                        if (y == 0)
                        {
                            y = zeroline;
                        }

                        int barheight;
                        if (y < zeroline)
                        {
                            barheight = Convert.ToInt32(zeroline - y);
                            recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(y.ToString()), iStepWidth, barheight);
                            oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 0, 160)), recs[i]);
                        }
                        else if (y > zeroline)
                        {
                            barheight = Convert.ToInt32(y - zeroline);
                            recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(zeroline.ToString()), iStepWidth, barheight);
                            oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 0, 0)), recs[i]);
                        }
                        else
                        {
                            barheight = 0;
                            recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(y.ToString()), iStepWidth, barheight);
                            oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 0, 0)), recs[i]);
                        }


                    }
                }
            }

            if (chartType == "Lines")
            {
                decimal iMiddleY;
                if (!double.IsInfinity(double.Parse(iScale.ToString())))
                {

                    iMiddleY = iImageHeight - (Math.Abs(iAvg - iMin) * iScale) - iBottomMargin;

                    //StdDev
                    if (bStdDev)
                    {
                        Rectangle oRect = new Rectangle();
                        oRect.Width = int.Parse(iImageWidth.ToString()) - (int.Parse(iRightMargin.ToString()) + int.Parse((iLeftMargin.ToString())));

                        d = decimal.Round(decimal.Parse(iStdDev.ToString()), 0);
                        iStdDev = decimal.Parse(d.ToString());

                        d = decimal.Round(decimal.Parse(iScale.ToString()), 0);
                        iScale = decimal.Parse(d.ToString());

                        oRect.Height = Convert.ToInt32(iStdDev * iScale);
                        oRect.X = int.Parse(iLeftMargin.ToString());

                        d = decimal.Round(decimal.Parse(iMiddleY.ToString()), 0);
                        iMiddleY = decimal.Parse(d.ToString());

                        oRect.Y = int.Parse(iMiddleY.ToString()) - (oRect.Height / 2);
                        oGraphics.FillRectangle(new SolidBrush(GetColor(sStdDevColor)), oRect);
                    }


                    // Agv Line
                    oGraphics.DrawLine(oAvgPen, float.Parse(iLeftMargin.ToString()), float.Parse(iMiddleY.ToString()), float.Parse(iImageWidth.ToString()) - float.Parse(iRightMargin.ToString()), float.Parse(iMiddleY.ToString()));

                    //Lines
                    oGraphics.DrawLines(oPen, oPoints);

                    //Final Point
                    Point oLastPoint = oPoints[oPoints.Length - 1];
                    Brush oBrush;
                    oBrush = new SolidBrush(Color.Red);
                    oGraphics.FillPie(oBrush, oLastPoint.X - 2, oLastPoint.Y - 2, 4, 4, 0, 360);

                    ////Final Value
                    //String drawString = oData[oData.Length - 1];
                    //Font drawFont  = new Font("Arial", 8);
                    //SolidBrush  drawBrush = new  SolidBrush(Color.Black);
                    //oGraphics.DrawString(drawString, drawFont, drawBrush, oLastPoint.X + 2, oLastPoint.Y - 6);


                }
            }

            //Response.ContentType = "image/jpeg";
            oBitmap.Save(outStream, ImageFormat.Jpeg);
            oGraphics.Dispose();
            oBitmap.Dispose();

            return outStream.ToArray();
        }

        public byte[] CreateImage(string chartType, string strDataPoints, int rowIndex)
        {
            string sData = "";
            string[] arrDataPoints = strDataPoints.Split(',');

            sData = strDataPoints;
            //int iWidth = 100;
            //int iHeight = 20;
            decimal d;
            sBgColor = "gray";
            sAvgLineColor = "Red";
            sLineColor = "Black";
            iTopMargin = 2;
            iBottomMargin = 2;
            iLeftMargin = 5;
            iRightMargin = 5;

            Color bgColor;
            bgColor = (rowIndex % 2 == 0) ? Color.FromArgb(236, 240, 245) : Color.White;

            int zeroline = -10;
            SetAvg(arrDataPoints);

            Point[] oPoints = new Point[arrDataPoints.Length];
            decimal iScale;
            if (iMin > 0)
            {
                iMin = iMin - (iMin * Convert.ToDecimal("0.2"));
            }
            if (iMax != iMin)
                iScale = (iImageHeight - (iTopMargin + iBottomMargin)) / Math.Abs(iMax - iMin);
            else
                iScale = 0;
            int iStepWidth = Convert.ToInt32(Math.Floor((iImageWidth - (iLeftMargin + iRightMargin) - (ibargap * (arrDataPoints.Length - 1))) / (arrDataPoints.Length)));
            zeroline = Convert.ToInt32(iMax * iScale) + 2;

            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap oBitmap = new Bitmap(int.Parse(iImageWidth.ToString()), int.Parse(iImageHeight.ToString()));
            oBitmap.MakeTransparent(GetColor(sBgColor));
            System.Drawing.Pen oPen = new System.Drawing.Pen(GetColor(sLineColor));
            System.Drawing.Pen oAvgPen = new System.Drawing.Pen(Color.Gray);
            Graphics oGraphics = Graphics.FromImage(oBitmap);
            Rectangle[] recs = new Rectangle[arrDataPoints.Length];

            oGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            oGraphics.FillRectangle(new SolidBrush(bgColor), 0, 0, float.Parse(iImageWidth.ToString()), float.Parse(iImageHeight.ToString()));


            int prevX = Convert.ToInt32(iImageHeight - iBottomMargin), prevY = Convert.ToInt32(iImageHeight - iBottomMargin);

            if (!double.IsInfinity(double.Parse(iScale.ToString())))
            {
                for (int i = 0; i <= arrDataPoints.Length - 1; i++)
                {
                    string sValue = arrDataPoints[i].ToString();
                    decimal iValue = 0;
                    if (sValue != "" & Information.IsNumeric(sValue))
                    {
                        iValue = decimal.Parse(sValue);
                    }

                    decimal x = (i * iStepWidth) + (i * ibargap) + iLeftMargin;
                    decimal y;
                    if (iValue != 0)
                    {
                        y = iImageHeight - (Math.Abs(iValue - iMin) * iScale) - iBottomMargin;

                        d = decimal.Round(decimal.Parse(y.ToString()), 0);
                        y = decimal.Parse(d.ToString());
                    }
                    else
                        y = 0;

                    d = decimal.Round(decimal.Parse(x.ToString()), 0);
                    x = decimal.Parse(d.ToString());

                    if (chartType == "Lines")
                    {
                        if (y == 0)
                        {
                            oPoints[i] = new Point(prevX, prevY);
                        }
                        else
                        {
                            oPoints[i] = new Point(int.Parse(x.ToString()), int.Parse(y.ToString()));
                            prevX = int.Parse(x.ToString());
                            prevY = int.Parse(y.ToString());
                        }
                    }
                    else
                    {
                        if (y == 0)
                        {
                            y = zeroline;
                        }

                        int barheight;
                        if (y < zeroline)
                        {
                            barheight = Convert.ToInt32(zeroline - y);
                            recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(y.ToString()), iStepWidth, barheight);
                            oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(100, 120, 185)), recs[i]);
                        }
                        else if (y > zeroline)
                        {
                            barheight = Convert.ToInt32(y - zeroline);
                            recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(zeroline.ToString()), iStepWidth, barheight);
                            oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(255, 0, 0)), recs[i]);
                        }
                        else
                        {
                            barheight = 0;
                            recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(y.ToString()), iStepWidth, barheight);
                            oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 0, 0)), recs[i]);
                        }


                    }
                }
            }

            if (chartType == "Lines")
            {
                decimal iMiddleY;
                if (!double.IsInfinity(double.Parse(iScale.ToString())))
                {

                    iMiddleY = iImageHeight - (Math.Abs(iAvg - iMin) * iScale) - iBottomMargin;

                    //StdDev
                    if (bStdDev)
                    {
                        Rectangle oRect = new Rectangle();
                        oRect.Width = int.Parse(iImageWidth.ToString()) - (int.Parse(iRightMargin.ToString()) + int.Parse((iLeftMargin.ToString())));

                        d = decimal.Round(decimal.Parse(iStdDev.ToString()), 0);
                        iStdDev = decimal.Parse(d.ToString());

                        d = decimal.Round(decimal.Parse(iScale.ToString()), 0);
                        iScale = decimal.Parse(d.ToString());

                        oRect.Height = Convert.ToInt32(iStdDev * iScale);
                        oRect.X = int.Parse(iLeftMargin.ToString());

                        d = decimal.Round(decimal.Parse(iMiddleY.ToString()), 0);
                        iMiddleY = decimal.Parse(d.ToString());

                        oRect.Y = int.Parse(iMiddleY.ToString()) - (oRect.Height / 2);
                        oGraphics.FillRectangle(new SolidBrush(GetColor(sStdDevColor)), oRect);
                    }


                    // Agv Line
                    oGraphics.DrawLine(oAvgPen, float.Parse(iLeftMargin.ToString()), float.Parse(iMiddleY.ToString()), float.Parse(iImageWidth.ToString()) - float.Parse(iRightMargin.ToString()), float.Parse(iMiddleY.ToString()));

                    //Lines
                    oGraphics.DrawLines(oPen, oPoints);

                    //Final Point
                    Point oLastPoint = oPoints[oPoints.Length - 1];
                    Brush oBrush;
                    oBrush = new SolidBrush(Color.Red);
                    oGraphics.FillPie(oBrush, oLastPoint.X - 2, oLastPoint.Y - 2, 4, 4, 0, 360);

                    ////Final Value
                    //String drawString = oData[oData.Length - 1];
                    //Font drawFont  = new Font("Arial", 8);
                    //SolidBrush  drawBrush = new  SolidBrush(Color.Black);
                    //oGraphics.DrawString(drawString, drawFont, drawBrush, oLastPoint.X + 2, oLastPoint.Y - 6);


                }
            }

            //Response.ContentType = "image/jpeg";
            oBitmap.Save(outStream, ImageFormat.Jpeg);
            oGraphics.Dispose();
            oBitmap.Dispose();

            return outStream.ToArray();
        }

        public byte[] CreateImage(string chartType, string strDataPoints, string strBaseValue)
        {
            string sData = "";
            string[] arrDataPoints = strDataPoints.Split(',');

            sData = strDataPoints;
            //int iWidth = 100;
            //int iHeight = 20;
            decimal d;
            sBgColor = "White";
            sAvgLineColor = "Red";
            sLineColor = "Black";
            iTopMargin = 2;
            iBottomMargin = 2;
            iLeftMargin = 5;
            iRightMargin = 5;

            SetAvg(arrDataPoints);

            Point[] oPoints = new Point[arrDataPoints.Length];
            decimal iScale;
            if (iMax != 0 && iMin != 0)
                iScale = (iImageHeight - (iTopMargin + iBottomMargin)) / Math.Abs(iMax - iMin);//
            else
                iScale = 0;
            int iStepWidth = Convert.ToInt32(Math.Floor((iImageWidth - (iLeftMargin + iRightMargin) - (ibargap * (arrDataPoints.Length - 1))) / (arrDataPoints.Length)));


            System.IO.MemoryStream outStream = new System.IO.MemoryStream();
            Bitmap oBitmap = new Bitmap(int.Parse(iImageWidth.ToString()), int.Parse(iImageHeight.ToString()));
            System.Drawing.Pen oPen = new System.Drawing.Pen(GetColor(sLineColor));
            System.Drawing.Pen oAvgPen = new System.Drawing.Pen(Color.Gray);
            Graphics oGraphics = Graphics.FromImage(oBitmap);
            Rectangle[] recs = new Rectangle[arrDataPoints.Length];

            oGraphics.SmoothingMode = SmoothingMode.AntiAlias;
            oGraphics.FillRectangle(new SolidBrush(GetColor(sBgColor)), 0, 0, float.Parse(iImageWidth.ToString()), float.Parse(iImageHeight.ToString()));

            int prevX = Convert.ToInt32(iImageHeight - iBottomMargin), prevY = Convert.ToInt32(iImageHeight - iBottomMargin);

            if (!double.IsInfinity(double.Parse(iScale.ToString())))
            {
                for (int i = 0; i <= arrDataPoints.Length - 1; i++)
                {
                    string sValue = arrDataPoints[i].ToString();
                    decimal iValue = 0;
                    if (sValue != "" & Information.IsNumeric(sValue))
                    {
                        iValue = decimal.Parse(sValue);
                    }

                    decimal x = (i * iStepWidth) + (i * ibargap) + iLeftMargin;
                    decimal y;
                    if (iValue != 0)
                    {
                        y = iImageHeight - (Math.Abs(iValue - iMin) * iScale) - iBottomMargin;

                        d = decimal.Round(decimal.Parse(y.ToString()), 0);
                        y = decimal.Parse(d.ToString());
                    }
                    else
                        y = 0;

                    d = decimal.Round(decimal.Parse(x.ToString()), 0);
                    x = decimal.Parse(d.ToString());

                    if (chartType == "Lines")
                    {
                        if (y == 0)
                        {
                            oPoints[i] = new Point(prevX, prevY);
                        }
                        else
                        {
                            oPoints[i] = new Point(int.Parse(x.ToString()), int.Parse(y.ToString()));
                            prevX = int.Parse(x.ToString());
                            prevY = int.Parse(y.ToString());
                        }
                    }
                    else
                    {
                        int barheight;
                        if (y != 0)
                            barheight = Convert.ToInt32(iImageHeight - y);
                        else
                            barheight = 0;
                        recs[i] = new Rectangle(int.Parse(x.ToString()), int.Parse(y.ToString()), iStepWidth, barheight);

                        oGraphics.FillRectangle(new SolidBrush(Color.FromArgb(0, 0, 160)), recs[i]);
                    }
                }
            }

            if (chartType == "Lines")
            {
                decimal iMiddleY;
                if (!double.IsInfinity(double.Parse(iScale.ToString())))
                {

                    iMiddleY = iImageHeight - (Math.Abs(decimal.Parse(strBaseValue) - iMin) * iScale) - iBottomMargin;

                    //StdDev
                    if (bStdDev)
                    {
                        Rectangle oRect = new Rectangle();
                        oRect.Width = int.Parse(iImageWidth.ToString()) - (int.Parse(iRightMargin.ToString()) + int.Parse((iLeftMargin.ToString())));

                        d = decimal.Round(decimal.Parse(iStdDev.ToString()), 0);
                        iStdDev = decimal.Parse(d.ToString());

                        d = decimal.Round(decimal.Parse(iScale.ToString()), 0);
                        iScale = decimal.Parse(d.ToString());

                        oRect.Height = int.Parse(iStdDev.ToString()) * int.Parse(iScale.ToString());
                        oRect.X = int.Parse(iLeftMargin.ToString());

                        d = decimal.Round(decimal.Parse(iMiddleY.ToString()), 0);
                        iMiddleY = decimal.Parse(d.ToString());

                        oRect.Y = int.Parse(iMiddleY.ToString()) - (oRect.Height / 2);
                        oGraphics.FillRectangle(new SolidBrush(GetColor(sStdDevColor)), oRect);
                    }


                    // Agv Line
                    oGraphics.DrawLine(oAvgPen, float.Parse(iLeftMargin.ToString()), float.Parse(iMiddleY.ToString()), float.Parse(iImageWidth.ToString()) - float.Parse(iRightMargin.ToString()), float.Parse(iMiddleY.ToString()));

                    //Lines
                    oGraphics.DrawLines(oPen, oPoints);

                    //Final Point
                    Point oLastPoint = oPoints[oPoints.Length - 1];
                    Brush oBrush;
                    oBrush = new SolidBrush(Color.Red);
                    oGraphics.FillPie(oBrush, oLastPoint.X - 2, oLastPoint.Y - 2, 4, 4, 0, 360);

                    ////Final Value
                    //String drawString = oData[oData.Length - 1];
                    //Font drawFont  = new Font("Arial", 8);
                    //SolidBrush  drawBrush = new  SolidBrush(Color.Black);
                    //oGraphics.DrawString(drawString, drawFont, drawBrush, oLastPoint.X + 2, oLastPoint.Y - 6);


                }
            }

            //Response.ContentType = "image/jpeg";
            oBitmap.Save(outStream, ImageFormat.Jpeg);
            oGraphics.Dispose();
            oBitmap.Dispose();

            return outStream.ToArray();
        }

        private System.Drawing.Color GetColor(string sColor)
        {
            if (sColor == null)
                sColor = "";

            Color oColor = Color.FromName(sColor);
            bool bColorEmpty = oColor.R == 0 & oColor.G == 0 & oColor.B == 0;
            if ((bColorEmpty == false))
                return oColor;
            return oColor;
        }

        public int HexToInt(string hexString)
        {
            return int.Parse(hexString, System.Globalization.NumberStyles.HexNumber, null);
        }
    }
}

<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/AnalysisMaster.Master" 
        AutoEventWireup="true" 
        CodeBehind="ChartWizard.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Analysis.ChartWizard"        
        Theme="NormalTree"%>
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<style type="text/css">
 .clear-left {    
    clear:left;    
    }
.ChartLink
{
    padding-top:3px;
    width:100%;
    float:left;
}

</style>
<script type="text/javascript">

function IndicatorTreeview_onNodeCheckChange(sender,eventArgs)
{
    var hdnControl=document.getElementById('<%= hdnSelectedIndicators.ClientID %>');    
    var checkedNode = eventArgs.get_node();
    
    if (checkedNode.get_checked()) 
    {
        hdnControl.value = hdnControl.value  + checkedNode.get_id()+ ",";        
    }
    else
    {
        hdnControl.value = hdnControl.value.replace(checkedNode.get_id()+",","");        
    }
    document.getElementById('<%= ChartLinksPanel.ClientID %>').style.display="none"; 
}
function CheckValidChartsLink_Click()
{
    var hdnControl=document.getElementById('<%= hdnSelectedIndicators.ClientID %>'); 
    ValidChartsCallback.callback(hdnControl.value); 
    return false;
}
</script>

<div >
<h2><asp:Label ID="ChartTitle" runat="server" Text="Chart Wizard"></asp:Label></h2>
</div>

    <h3>Select Indicators</h3>

    
    <table border="0">
        <tr>
            <td>  
           <div style="width:400px;float:left;vertical-align:top;">
          <ComponentArt:TreeView id="IndicatorTreeview" Height="350" Width="400"
            DragAndDropEnabled="false"
            NodeEditingEnabled="false"
            KeyboardEnabled="true"
            CssClass="powersearch_tree"
            NodeCssClass="TreeNode"                                                        
            NodeEditCssClass="NodeEdit"
            SelectedNodeCssClass="NodeSelected"
            LineImageWidth="19"
            LineImageHeight="20"
            DefaultImageWidth="16"
            DefaultImageHeight="16"
            ItemSpacing="0"
            NodeLabelPadding="3"
            ImagesBaseUrl="../assets/images/tvlines/"
            LineImagesFolderUrl="../assets/images/tvlines/"
            ShowLines="true"
            EnableViewState="false"                           
            runat="server" BorderWidth="0px" >
            <ClientEvents>
                <NodeCheckChange EventHandler="IndicatorTreeview_onNodeCheckChange" />
            </ClientEvents>
          </ComponentArt:TreeView>
          
    </div>
    
               
            </td>
            <td valign="top" align="left" width="160px"> 
             <div class="button_right" style="width:160px;">
                   <asp:LinkButton ID="CheckValidChartsLink" Text="Suggest Charts" runat="server"  Width="160px" 
                    OnClientClick="javascript:return CheckValidChartsLink_Click();" 
                    ToolTip="click to get valid charts for the selected indicators"/>
                </div>
            <div style="width:160;height:80px">
           <ComponentArt:CallBack ID="ValidChartsCallback" runat="server"
                OnCallback="ShowValidCharts_Callback" > 
                <Content>
                    <asp:Panel ID="ChartLinksPanel" runat="server" Width="160" >      
                        <br />   <br />   
                        <div id="XYChartLink" runat="server">
                        <asp:HyperLink ID="XYChartHyperLink" runat="server" Font-Bold="true" NavigateUrl="~/analysis/xychart.aspx?source=wizard">XY Chart</asp:HyperLink>
                        </div>
                        <%--
                        <asp:HyperLink ID="DualYaxisChartLink" CssClass="ChartLink" runat="server" Font-Bold="true" NavigateUrl="~/analysis/dualyaxischart.aspx?source=wizard">Dual Y-Axis Chart</asp:HyperLink>--%>
                        <div id="BubbleChartLink" runat="server">
                        <asp:HyperLink ID="BubbleChartHyperLink" runat="server" Font-Bold="true" NavigateUrl="#">Bubble Chart</asp:HyperLink>
                        </div>
                        <div id="PieChartLink" runat="server">
                        <asp:HyperLink ID="PieChartHyperLink" runat="server" Font-Bold="true" NavigateUrl="~/analysis/PieChart.aspx?source=wizard">Pie Chart</asp:HyperLink>
                        </div>
                        <div id="HeatMapLink" runat="server">
                        <asp:HyperLink ID="HeatMapHyperLink" runat="server" Font-Bold="true" NavigateUrl="~/analysis/HeatMap.aspx?source=wizard">Heat Map</asp:HyperLink>
                        </div>
                    </asp:Panel>
                </Content>
                <LoadingPanelClientTemplate>
                    <table class="loadingpanel" width="160px" style="height:100px" cellspacing="0" cellpadding="0" border="0">
                      <tr>
                        <td align="center">
                        <table cellspacing="0" cellpadding="0" border="0">
                        <tr>
                          <td colspan="2">
                          <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="../assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                          </td>
                        </tr>
                        </table>
                        </td>
                      </tr>
                      </table>
                </LoadingPanelClientTemplate>
            </ComponentArt:CallBack>   
            </div>
            </td>
        </tr>
    </table>
   
   <asp:HiddenField ID="hdnSelectedIndicators" runat="server" />
</asp:Content>

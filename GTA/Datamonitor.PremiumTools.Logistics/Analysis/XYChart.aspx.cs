using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class XYChart : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string AxisCriteria = string.Empty;
        private bool IsXAxisVisible = true;
        private bool IsSeriesVisible = true;
        private bool IsYAxisVisible = true;
        private bool IsPredefinedChart = false;

        // EVENT OVERRIDES
        
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack &&
                !ChartFilterCallback.IsCallback &&
                !ChartsCallback.IsCallback)
            {
                //Bind the axis selection dropdowns.
                BindAxisDropdownsData();

                //Select the default chart type
                SelectDefaultchartType();               

                xAxisDropdown.Attributes.Add("onchange", "javascript:XAxis_onChange();");
                SeriesDropDown.Attributes.Add("onchange", "javascript:Series_onChange();");

                if (string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    YAxisDropDown.Attributes.Add("onchange", "javascript:YAxis_onChange(true);");
                }
                else
                {
                    YAxisDropDown.Attributes.Add("onchange", "javascript:YAxis_onChange(false);");
                }
            }
            
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";

            //Hide the 'back to chart wizard' link when loading predefined chart
            BackToChartWizardDiv.Visible = (Request.QueryString["id"]==null);

            if (!ChartFilterCallback.IsCallback &&
                !ChartsCallback.IsCallback &&
                !IsPostBack)
            {
                //Set selected y Axis to update filter dropdowns
                ChartFilters1.YAxis = hdnYAxis.Value.Length > 0 ?
                       Convert.ToString(System.Math.Abs(Convert.ToInt32(hdnYAxis.Value))) :
                       "";

                ChartFilters1.LoadData();
            }     
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsPostBack && !ChartsCallback.IsCallback)
            {  
                PlotChart_Click(null, null);                
            }
        }

        // PRIVATE METHODS (initial population)       
        private void BindAxisDropdownsData()
        {
            //bool IsXAxisVisible = true;
            //bool IsSeriesVisible = true;
            //bool IsYAxisVisible = true;
            //bool IsPredefinedChart = false;
            XmlDocument document = new XmlDocument();
            XmlDocument doc = new XmlDocument();
            string xPathExpression;
            string taxonomtypes;
            string includeYears;
            string from;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                DataSet ds = SqlDataService.GetPredefinedChartMetaData(Convert.ToInt32(Request.QueryString["id"]));
                IsPredefinedChart = true;
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string filterXml = ds.Tables[0].Rows[0]["Selections"].ToString();            
                    document.LoadXml(filterXml);
                    IsXAxisVisible = Convert.ToBoolean(document.SelectSingleNode("//XAxis").Attributes["Visible"].Value);
                    IsYAxisVisible = Convert.ToBoolean(document.SelectSingleNode("//YAxis").Attributes["Visible"].Value);
                    if (document.SelectSingleNode("//ChartType").Attributes["SeriesChartType"] != null)
                    {
                        hdnChartSeriesType.Value = document.SelectSingleNode("//ChartType").Attributes["SeriesChartType"].Value;
                        ChartTypeLabel.Visible = false;
                        ChartTypesDropDown.Visible = false;
                    }
                    IsSeriesVisible = Convert.ToBoolean(document.SelectSingleNode("//Series").Attributes["Visible"].Value);
                    hiddenChartType.Value = document.SelectSingleNode("//ChartType").Attributes["SelectedValue"].Value;                   
                    
                    ChartTitle.Text = document.SelectSingleNode("//Title").InnerText;                    
                }
            }
            if (IsXAxisVisible)
            {
                doc.Load(Server.MapPath(GlobalSettings.ChartFilterOptionsConfigPath));
                xPathExpression = "//Taxonomy[@type='XAxis']";
                XmlElement xaxisItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
                //Get and bind data to X Axis drop down
                taxonomtypes = xaxisItem.Attributes["taxonomtypes"].Value;
                from = xaxisItem.Attributes["from"].Value;
                includeYears = xaxisItem.Attributes["includeYears"].Value;
                DataSet XAxisData = new DataSet();
                if (from.Equals("taxonomtypes"))
                {
                    XAxisData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
                }
                else if (from.Equals("dataset"))
                {
                    XAxisData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
                }

                xAxisDropdown.DataSource = XAxisData;
                xAxisDropdown.DataTextField = XAxisData.Tables[0].Columns["Name"].ToString();
                xAxisDropdown.DataValueField = XAxisData.Tables[0].Columns["ID"].ToString();
                xAxisDropdown.DataBind();
                xaxisDropdownDiv.Visible = true;
                try
                {
                    xAxisDropdown.SelectedValue = GlobalSettings.DefaultXAxisType;
                    hdnXAxis.Value = GlobalSettings.DefaultXAxisType;
                    hdnXAxisText.Value = xAxisDropdown.SelectedItem.Text;
                    
                }
                catch (Exception ex) { }

            }
            else
            {                
                xaxisDropdownDiv.Visible = false;
                hdnXAxis.Value = document.SelectSingleNode("//XAxis").Attributes["SelectedValue"].Value;
                hdnXAxisText.Value = document.SelectSingleNode("//XAxis").Attributes["SelectedText"].Value;
            }

            if (IsSeriesVisible)
            {
                xPathExpression = "//Taxonomy[@type='Series']";
                //Get and bind data to series drop down
                XmlElement SeriesItem = (XmlElement)doc.SelectSingleNode(xPathExpression);

                taxonomtypes = SeriesItem.Attributes["taxonomtypes"].Value;
                from = SeriesItem.Attributes["from"].Value;
                includeYears = SeriesItem.Attributes["includeYears"].Value;
                DataSet SeriesData = new DataSet();
                if (from.Equals("taxonomtypes"))
                {
                    SeriesData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
                }
                else if (from.Equals("dataset"))
                {
                    SeriesData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
                }

                SeriesDropDown.DataSource = SeriesData;
                SeriesDropDown.DataTextField = SeriesData.Tables[0].Columns["Name"].ToString();
                SeriesDropDown.DataValueField = SeriesData.Tables[0].Columns["ID"].ToString();
                SeriesDropDown.DataBind();
                seriesDropdownDiv.Visible = true;
                try
                {                    
                    SeriesDropDown.SelectedValue = GlobalSettings.DefaultSeriesType;                 
                    hdnSeries.Value = GlobalSettings.DefaultSeriesType;
                    hdnSeriesText.Value = SeriesDropDown.SelectedItem.Text;
                }
                catch (Exception ex) { }

            }
            else
            {
                seriesDropdownDiv.Visible = false;
                hdnSeries.Value = document.SelectSingleNode("//Series").Attributes["SelectedValue"].Value;
                hdnSeriesText.Value = document.SelectSingleNode("//Series").Attributes["SelectedText"].Value;
            }

            if (!IsPredefinedChart)
            {
                //Get Y-Axis data.
                string SelectionsXML = GetFilterCriteriaFromSession();
                yaxisDropdownDiv.Visible = true;
                DataSet YAxisDataSet = SqlDataService.GetYAxisTaxonomyByCriteria(SelectionsXML, true);

                //Binding Y-Axis
                if (YAxisDataSet != null && YAxisDataSet.Tables.Count > 0)
                {

                    if (GlobalSettings.ShowChartWizard &&
                        Request.QueryString["source"] != null &&
                        Request.QueryString["source"].ToString().Equals("wizard") &&
                        Session["SelectedMeasures"] != null &&
                        Session["SelectedMeasures"].ToString() != "")
                    {
                        //Filter measures if chart wizard selections are found
                        DataView SelectedMeasures = YAxisDataSet.Tables[0].DefaultView;
                        SelectedMeasures.RowFilter = "id in (" + Session["SelectedMeasures"].ToString() + ")";
                        DataTable SelectedMeasuresTable = SelectedMeasures.ToTable();

                        //Binding Y-Axis
                        YAxisDropDown.DataSource = SelectedMeasuresTable;
                        YAxisDropDown.DataTextField = SelectedMeasuresTable.Columns["displayName"].ToString();
                        YAxisDropDown.DataValueField = SelectedMeasuresTable.Columns["ID"].ToString();
                        YAxisDropDown.DataBind();
                    }
                    else
                    {
                        //Binding Y-Axis
                        YAxisDropDown.DataSource = YAxisDataSet;
                        YAxisDropDown.DataTextField = YAxisDataSet.Tables[0].Columns["displayName"].ToString();
                        YAxisDropDown.DataValueField = YAxisDataSet.Tables[0].Columns["ID"].ToString();
                        YAxisDropDown.DataBind();
                    }

                    try
                    {
                        //YAxisDropDown.SelectedValue = GlobalSettings.DefaultYAxisType;
                        hdnYAxis.Value = YAxisDropDown.SelectedValue;
                        hdnYAxisText.Value = YAxisDropDown.SelectedItem.Text;

                    }
                    catch (Exception ex) { }
                }
            }
            else
            {
                string YAxisDefaultValue = document.SelectSingleNode("//YAxis").Attributes["SelectedValue"].Value;
                if (IsYAxisVisible)
                {
                    string YAxisValues = document.SelectSingleNode("//YAxis").Attributes["Values"].Value;

                    string[] YAxisItems = YAxisValues.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < YAxisItems.Length; i++)
                    {
                        YAxisDropDown.Items.Add(new ListItem(YAxisItems[i].Split(new char[] { '|' })[1], YAxisItems[i].Split(new char[] { '|' })[0]));
                    }
                    try
                    {
                        YAxisDropDown.SelectedValue = YAxisDefaultValue;
                        hdnYAxis.Value = YAxisDefaultValue;
                        hdnYAxisText.Value = YAxisDropDown.SelectedItem.Text;
                    }
                    catch (Exception ex) { }
                    yaxisDropdownDiv.Visible = true;
                }
                else
                {
                    hdnYAxis.Value = YAxisDefaultValue;
                    hdnYAxisText.Value = document.SelectSingleNode("//YAxis").Attributes["SelectedText"].Value;
                    yaxisDropdownDiv.Visible = false;
                }
            }
            //Store chart title in hidden field
            hdnChartTitle.Value = ChartTitle.Text;
        }               
        
        private void SelectDefaultchartType()
        {
            if (!string.IsNullOrEmpty(hiddenChartType.Value))
            {
                foreach (ComponentArt.Web.UI.TreeViewNode TypeNode in ChartTypesTreeView.Nodes[0].Nodes)
                {
                    if (TypeNode.Value.ToLower().Contains(hiddenChartType.Value.ToLower()))
                    {
                        ChartTypesTreeView.SelectedNode = TypeNode;
                        ChartTypesDropDown.Text = TypeNode.Text;
                        break;
                    }
                }
            }
            else
            {
                CArt.TreeViewNode LeafNode = null;
                foreach (CArt.TreeViewNode tnode in ChartTypesTreeView.Nodes)
                {
                    if (tnode.Nodes.Count > 0)
                    {
                        LeafNode = GetFirstLeafNode(tnode);
                        break;
                    }
                    else
                    {
                        LeafNode = tnode;
                        break;
                    }
                }
                if (LeafNode != null)
                {
                    ChartTypesDropDown.Text = LeafNode.Text;
                    if (string.IsNullOrEmpty(Request.QueryString["id"]))
                    {
                        hiddenChartType.Value = LeafNode.Value;
                    }

                    ChartTypesTreeView.SelectedNode = LeafNode;
                }
            }
        }
       
        private CArt.TreeViewNode GetFirstLeafNode(CArt.TreeViewNode parentNode)
        {
            foreach (CArt.TreeViewNode tnode in parentNode.Nodes)
            {
                if (tnode.Nodes.Count > 0)
                {
                    return GetFirstLeafNode(tnode);
                }
                else
                {
                    return tnode;
                }
            }
            return parentNode;
        }

        // METHODS (Build selection criteria into XML)      
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;

            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {
                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    string TaxonomySelectedText = string.Empty;

                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        //Get the x axis, series taxonomies filter data from session
                        if (hdnXAxis.Value.Equals(taxonomyRow["ID"].ToString()) || hdnSeries.Value.Equals(taxonomyRow["ID"].ToString()))
                        {
                            Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                            if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                            {
                                //Get all keys (ids) from dictionary
                                List<int> keys = new List<int>(selectedIDsList.Keys);
                                //convert int array to string array
                                string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                                TaxonomySelectedValue = string.Join(",", SelectedIDs);
                            }
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                                TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                            }
                            if (ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                                TaxonomySelectedText = TaxonomySelectedText.TrimEnd(new char[] { ',' });
                            }
                        }
                        else
                        {
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                            }
                            if (ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText") != null)
                            {
                                TaxonomySelectedText = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy" + taxonomyRow["ID"].ToString() + "SelectionText")).Value;
                            }
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedText))
                        {
                            if (TaxonomySelectedText.Contains("'"))
                            {
                               TaxonomySelectedText = TaxonomySelectedText.Replace("'","&apos;");                              
                            }

                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedText);
                        }
                    }
                    else if (taxonomyRow["ID"].ToString().Equals("999"))
                    {

                        if (hdnXAxis.Value.Equals(taxonomyRow["ID"].ToString()) ||
                            hdnSeries.Value.Equals(taxonomyRow["ID"].ToString()))
                        {
                            TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnMultipleTaxonomy999Selection")).Value;
                            TaxonomySelectedValue = TaxonomySelectedValue.TrimEnd(new char[] { ',' });
                        }
                        else
                        {
                            //Get selected year value from user control's hidden field
                            TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("hdnSingleTaxonomy999Selection")).Value;
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                               taxonomyRow["Name"].ToString(),
                               TaxonomySelectedValue);
                        }
                    }

                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
                SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
                Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            }

            return SelectionsXML;
        }

        private string GetCallbackDataFilterXML(string xAxis,
           string yAxis,
           string series,
           string filterValues,
           string filterValuesText)
        {
            string SelectionsXML = string.Empty;
            string SelectionsTextXML = string.Empty;

            string[] taxonomyFilters = filterValues.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            string[] taxonomyFiltersText = filterValuesText.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < taxonomyFilters.Length; i++)
            {
                string taxonomyID = taxonomyFilters[i].Split('|')[0];
                string taxonomyName = taxonomyFiltersText[i].Split('|')[0];
                string taxonomyRowID = GetColumnRowIDById(taxonomyID);
                string taxonomyValues = taxonomyID.Equals(xAxis) || taxonomyID.Equals(series) ?
                    taxonomyFilters[i].Split('|')[2] :
                    taxonomyFilters[i].Split('|')[1];

                string taxonomyValuesText = taxonomyID.Equals(xAxis) || taxonomyID.Equals(series) ?
                  taxonomyFiltersText[i].Split('|')[2] :
                  taxonomyFiltersText[i].Split('|')[1];

                SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                                    taxonomyID,
                                    taxonomyValues.TrimEnd(new char[] { ',' }),
                                    taxonomyRowID);

                if (taxonomyID == "999")
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                                    taxonomyName,
                                    taxonomyValues.TrimEnd(new char[] { ',' }));
                }
                else
                {
                    SelectionsTextXML += string.Format("<taxonomy id='{0}' selectIDs='{1}'/>",
                         taxonomyName,
                         taxonomyValuesText.TrimEnd(new char[] { ',' }));
                }

            }

            SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);
            Session["SelectionsTextXML"] = SelectionsTextXML.ToString();
            return SelectionsXML;
        }
        
        // EVENT HANDLERS
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            DataSet ChartDataset = null;
            
                //Get Axis Criteria            
                //if (IsXAxisVisible && IsYAxisVisible && IsSeriesVisible)
                //{
                //    if (xAxisDropdown.SelectedItem != null && YAxisDropDown.SelectedItem != null)
                //    {
                //        AxisCriteria = "X Axis: " + xAxisDropdown.SelectedItem.Text.ToString()
                //                     + "; Y Axis: " + YAxisDropDown.SelectedItem.Text.ToString()
                //                     + "; Series: " + SeriesDropDown.SelectedItem.Text;
                //    }
                //}
                //else
                //{
                //    AxisCriteria = "X Axis: " + hdnXAxisText.Value.ToString()
                //                + "; Y Axis: " + hdnYAxisText.Value.ToString()
                //                + "; Series: " + hdnSeriesText.Value.ToString();
                //}

                //Get selection criteria   
                string SelectionsXML = GetDataFilterXML();
                string predefinedID = string.IsNullOrEmpty(Request.QueryString["id"]) ?
                    "" :
                    "Predefined Chart:" + Request.QueryString["id"].ToString();

                LogUsage("Charts - XY Chart", predefinedID, SelectionsXML);

                //Get chart data 
                if (!string.IsNullOrEmpty(hdnXAxis.Value) && 
                    !string.IsNullOrEmpty(hdnYAxis.Value) && 
                    !string.IsNullOrEmpty(hdnSeries.Value))
                {
                    ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(hdnXAxis.Value),
                        Int32.Parse(hdnYAxis.Value),
                        Int32.Parse(hdnSeries.Value),
                        SelectionsXML);
                }
                ViewState["ChartDataset"] = ChartDataset;
            
            //Plot chart
            if (ChartDataset!=null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                //string ChartOptions = ((HiddenField)ChartFilters1.FindControl("ChartType")).Value;
                string ChartOptions = hiddenChartType.Value;
                string ChartType = "2D";
                string ChartSeriesType = "Line";
                string YAxisLabel = ChartDataset.Tables[0].Rows[0]["Units"].ToString();
                //= ((DropDownList)ChartFilters1.FindControl("ChartTypeDropDown")).SelectedItem.Text;
                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];
                    ChartSeriesType = ChartOptions.Split('|')[1];
                }

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotXYChart(ChartDataset, 
                        ThreeDChart, 
                        ChartSeriesType,
                        YAxisLabel,
                        AxisCriteria);
                    TwoDChart.Visible = false;
                    targetChart.Visible = true;
                   
                    //Save the chart in viewstate, used in extractions.
                    ViewState["3DChartImage"] = targetChart.ImageOutputDirectory.ToString();
                    //Save the Chart GUID in viewstate, used in extractions.
                    ViewState["ChartID"] = targetChart.CustomImageFileName.ToString();
                    ViewState["ChartType"] = "3D";

                    hdnImageurl.Value = targetChart.ImageOutputDirectory.ToString();                    
                    hdnChartID.Value = targetChart.CustomImageFileName.ToString();
                    hdnImageType.Value = "3D";
                    
                }
                else
                {
                    dotnetCHARTING.Chart targetChart;          
                    targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        YAxisLabel,
                        AxisCriteria);
                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);

                    //Set chart series types                   
                    //else
                    //{
                        string[] ChartTypes = hdnChartSeriesType.Value.Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] types = ChartTypes.Length > 0 ? ChartTypes[0].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                        string[] seriesNames = ChartTypes.Length > 1 ? ChartTypes[1].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                        if (ChartTypes.Length == 0 && ChartSeriesType.Equals("stacked"))
                        {
                            targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                        }
                        else if (ChartSeriesType.Equals("Horizontal"))
                        {
                            targetChart.Type = dotnetCHARTING.ChartType.ComboHorizontal;
                        }
                        for (int i = 0; i < targetChart.SeriesCollection.Count; i++)
                        {
                            if (ChartTypes.Length > 0)
                            {
                                if (seriesNames != null)
                                {
                                    for (int j = 0; j < seriesNames.Length; j++)
                                    {
                                        if (seriesNames[j].Equals(targetChart.SeriesCollection[i].Name))
                                        {
                                            targetChart.SeriesCollection[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(types[j]);
                                            if (ChartSeriesType.Equals("stacked") && (types[j] == "4" || types[j] == "7"))
                                            {
                                                targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                            }
                                            if (types[j] == "4")
                                            {                                               
                                                targetChart.SeriesCollection[i].DefaultElement.Transparency = 20;
                                                targetChart.SeriesCollection[i].DefaultElement.Marker.Type = dotnetCHARTING.ElementMarkerType.None;
                                            }
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    string Seriestype;
                                    Seriestype = i < types.Length ? types[i] : ChartSeriesType;

                                    if (!Seriestype.Equals("Horizontal"))
                                    {
                                        targetChart.SeriesCollection[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(Seriestype);

                                        if (Seriestype == "4")
                                        {
                                            targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                            targetChart.SeriesCollection[i].DefaultElement.Transparency = 20;
                                            targetChart.SeriesCollection[i].DefaultElement.Marker.Type = dotnetCHARTING.ElementMarkerType.None;
                                        }
                                    }
                                }
                            }
                        }
                    //}

                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }
                    targetChart.FileManager.TempDirectory = "temp";

                    // Save the legend as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.BottomMiddle;
                    if (targetChart.SeriesCollection.Count > 3)
                    {
                        targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                    }
                    LegendImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;

                    //Save the chart in viewstate, used in extractions.
                    ViewState["2DChartImage"] = ChartImg.ImageUrl;
                    ViewState["ChartType"] = "2D";

                    hdnImageurl.Value = ChartImg.ImageUrl;                  
                    hdnImageType.Value = "2D";
                    hdnLegendUrl.Value = LegendImg.ImageUrl;
                    
                }
            }
            else
            {
                TwoDChart.NoDataLabel.Text = "Data not available";
                TwoDChart.Height = 320;
                TwoDChart.Width = 500;
                if (!Directory.Exists(Server.MapPath("temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("temp"));
                }
                TwoDChart.FileManager.TempDirectory = "temp";
                ChartImg.ImageUrl = TwoDChart.FileManager.SaveImage(TwoDChart.GetChartBitmap()).Replace("\\", "/");
            }
        }              

        // CALLBACK EVENT HANDLERS
        public void UpdateChartfilters_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            string YAxisValue = e.Parameters[0];
            string FilterData= e.Parameters[1];
            string FilterDataText = e.Parameters[2];

            ChartFilters1.YAxis = Convert.ToString(System.Math.Abs(Convert.ToInt32(YAxisValue)));
            ChartFilters1.LoadDataInCallback(FilterData, FilterDataText);
            ChartFilters1.RenderControl(e.Output);
        }

        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            ErrorMessagelabel.Text = "";
            //Get Axis Criteria
            //if (e.Parameters[6].ToString() != "")
            //{
            //   AxisCriteria = "YAxis: " + e.Parameters[6].ToString();
            //}
            //if (e.Parameters[5].ToString() != "" && e.Parameters[6].ToString() != "" && e.Parameters[7].ToString() != "")
            //{
            //    AxisCriteria = "X Axis: " + e.Parameters[5].ToString()
            //                 + "; Y Axis: " + e.Parameters[6].ToString()
            //                 + "; Series: " + e.Parameters[7].ToString();
            //}
           
            //Get Filter XML
            string FilterXML = GetCallbackDataFilterXML(e.Parameters[0].ToString(),
             e.Parameters[1].ToString(),
             e.Parameters[2].ToString(),
             e.Parameters[3].ToString(),
             e.Parameters[8].ToString());

            DataSet ChartDataset = null;
            try
            {

                //Get chart data 
                ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(e.Parameters[0].ToString()),
                    Int32.Parse(e.Parameters[1].ToString()),
                    Int32.Parse(e.Parameters[2].ToString()),
                    FilterXML);
            }
            catch (Exception DataFetchException)
            {
                ChartDataset = null;
            }

            //Plot chart
            if (ChartDataset!=null && ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartOptions = e.Parameters[4].ToString();
                string ChartType = "2D";
                string ChartSeriesType = "Line";
                string YAxisLabel = ChartDataset.Tables[0].Rows[0]["Units"].ToString();

                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];
                    ChartSeriesType = ChartOptions.Split('|')[1];
                }

                if (ChartType == "3D")
                {
                    ComponentArt.Web.Visualization.Charting.Chart targetChart;
                    targetChart = ThreeDimensionalCharts.PlotXYChart(ChartDataset,
                        ThreeDChart,
                        ChartSeriesType,
                        YAxisLabel,
                        AxisCriteria);
                    TwoDChart.Visible = false;
                    targetChart.Visible = true;
                    targetChart.RenderControl(e.Output);

                    hdnImageurl.Value = targetChart.ImageOutputDirectory.ToString();
                    hdnImageurl.RenderControl(e.Output);
                    hdnChartID.Value = targetChart.CustomImageFileName.ToString();
                    hdnChartID.RenderControl(e.Output);
                    hdnImageType.Value = ChartType;
                    hdnImageType.RenderControl(e.Output);

                }
                else
                {
                    dotnetCHARTING.Chart targetChart;
                    targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        YAxisLabel,
                        AxisCriteria);
                    targetChart.Visible = false;
                    ThreeDChart.Visible = false;
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                    //Set chart series types

                    //if (ChartSeriesType.Equals("stacked"))
                    //{
                    //    targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                    //}
                    //else
                    //{
                        string[] ChartTypes = e.Parameters[9].ToString().Split(new char[] { '|' }, StringSplitOptions.RemoveEmptyEntries);
                        string[] types = ChartTypes.Length > 0 ? ChartTypes[0].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                        string[] seriesNames = ChartTypes.Length > 1 ? ChartTypes[1].Split(new char[] { '~' }, StringSplitOptions.RemoveEmptyEntries) : null;
                        
                        if (ChartTypes.Length == 0 && ChartSeriesType.Equals("stacked"))
                        {
                            targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                        }
                        else if (ChartSeriesType.Equals("Horizontal"))
                        {
                            targetChart.Type = dotnetCHARTING.ChartType.ComboHorizontal;
                        }
                        for (int i = 0; i < targetChart.SeriesCollection.Count; i++)
                        {
                            if (ChartTypes.Length > 0)
                            {
                                if (seriesNames != null)
                                {
                                    for (int j = 0; j < seriesNames.Length; j++)
                                    {
                                        if (seriesNames[j].Equals(targetChart.SeriesCollection[i].Name))
                                        {
                                            targetChart.SeriesCollection[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(types[j]);
                                            if (types[j] == "4")
                                            {
                                                targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                                targetChart.SeriesCollection[i].DefaultElement.Transparency = 20;
                                                targetChart.SeriesCollection[i].DefaultElement.Marker.Type = dotnetCHARTING.ElementMarkerType.None;
                                            }
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    string Seriestype;
                                    Seriestype = i < types.Length ? types[i] : ChartSeriesType;

                                    if (!Seriestype.Equals("Horizontal"))
                                    {
                                        targetChart.SeriesCollection[i].Type = (dotnetCHARTING.SeriesType)Convert.ToInt32(Seriestype);

                                        if (Seriestype == "4")
                                        {
                                            targetChart.YAxis.Scale = dotnetCHARTING.Scale.Stacked;
                                            targetChart.SeriesCollection[i].DefaultElement.Transparency = 20;
                                            targetChart.SeriesCollection[i].DefaultElement.Marker.Type = dotnetCHARTING.ElementMarkerType.None;
                                        }
                                    }
                                }
                            }


                        }
                    //}



                    if (!Directory.Exists(Server.MapPath("temp")))
                    {
                        Directory.CreateDirectory(Server.MapPath("temp"));
                    }

                    targetChart.FileManager.TempDirectory = "temp";
                    string strLegendPath = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");

                    targetChart.Width = Unit.Point(520);
                    targetChart.Height = Unit.Point(300);
                    // Remove the legend from the chart and save the chart as a separate image file
                    targetChart.LegendBox.Position = dotnetCHARTING.LegendBoxPosition.None;
                    string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");

                    //string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap());

                    ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);
                    ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                    ImageMapLabel.Text = targetChart.ImageMapText;
                    ChartImg.RenderControl(e.Output);
                    ImageMapLabel.RenderControl(e.Output);
                    hdnImageurl.Value = strChartPath;
                    hdnImageurl.RenderControl(e.Output);
                    
                    hdnImageType.Value = ChartType;
                    hdnImageType.RenderControl(e.Output);

                    LegendImg.ImageUrl = ChartImg.ResolveUrl(strLegendPath);
                    LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                    divChart.RenderControl(e.Output);
                    hdnLegendUrl.Value = strLegendPath;
                    hdnLegendUrl.RenderControl(e.Output);

                }
            }
            else
            {
                ErrorMessagelabel.Text = "Data not available";                
            }
            ErrorMessagelabel.RenderControl(e.Output);
        }
        
        //EXPORT EVENT HANDLERS       
        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis.Value),
                Int32.Parse(hdnSeries.Value),
                SelectionsXML);
            LogUsage("Charts - XY - Word download","XYChart 1"," ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            if (chartBitmap != null)
            {
                Document doc = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "XYChart",
                    WordTemplatePath,
                    hdnChartTitle.Value);
                doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);               
            }
        }
       
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;           
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;
           
            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis.Value),
                Int32.Parse(hdnSeries.Value),
                SelectionsXML);
            LogUsage("Charts - XY - Excel download", "XYChart1", " ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }

            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            Workbook Excel;
            if (chartBitmap != null)
            {
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "XYChart",
                    ExcelTemplatePath,
                    LogoPath,
                    hdnChartTitle.Value);

                Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            } 
        }
       
        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string strExtractName = "OvumExtract";
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            string SelectionsXML = GetDataFilterXML();

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetAnalysisData(Int32.Parse(hdnXAxis.Value),
                Int32.Parse(hdnYAxis.Value),
                Int32.Parse(hdnSeries.Value),
                SelectionsXML);

            LogUsage("Charts - XY - PPT download", "XYChart1", " ");

            if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("3D"))
            {
                chartImagePath = hdnImageurl.Value.Replace('\\', '/') + "/" + hdnChartID.Value;
                chartBitmap = new Bitmap(chartImagePath);
            }
            else if (hdnImageurl.Value.Length > 0 && hdnImageType.Value.Equals("2D"))
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            if (chartBitmap != null)
            {
                Presentation presentation = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "XYChart",
                    PowerpointTempaltePath,
                    hdnChartTitle.Value);
                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presentation.Write(Response.OutputStream);
                Response.Flush();
            }
        }
    }
}

using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using ComponentArt.Web.Visualization.Charting;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class XYScrollChart : AuthenticationBasePage
    {
        static string SelectedYear;
        static string DataFilterXML;
        static string ChartOptions;
        static int xaxisTypeID;
        static int yAxisTaxonomyID;
        static int seriesTypeID;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                xAxisDropdown.Attributes.Add("onchange", "javascript: ShowHideFilterOptions();");
                SeriesDropDown.Attributes.Add("onchange", "javascript: ShowHideFilterOptions();");
                //Bind the axis selection dropdowns.
                BindAxisDropdownsData();
                if (DataFilterXML != null && SelectedYear != null && ChartOptions != null)
                {

                    PlotScrollChart();
                }
            }
        }

        /// <summary>
        /// Binds the axis dropdowns data.
        /// </summary>
        private void BindAxisDropdownsData()
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(Server.MapPath(GlobalSettings.ChartFilterOptionsConfigPath));
            string xPathExpression = "//Taxonomy[@type='XAxis']";
            XmlElement xaxisItem = (XmlElement)doc.SelectSingleNode(xPathExpression);
            //Get and bind data to X Axis drop down
            string taxonomtypes = xaxisItem.Attributes["taxonomtypes"].Value;
            string from = xaxisItem.Attributes["from"].Value;
            string includeYears = xaxisItem.Attributes["includeYears"].Value;
            DataSet XAxisData = new DataSet();
            if (from.Equals("taxonomtypes"))
            {
                XAxisData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
            }
            else if (from.Equals("dataset"))
            {
                XAxisData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
            }

            xAxisDropdown.DataSource = XAxisData;
            xAxisDropdown.DataTextField = XAxisData.Tables[0].Columns["Name"].ToString();
            xAxisDropdown.DataValueField = XAxisData.Tables[0].Columns["ID"].ToString();
            xAxisDropdown.DataBind();

            //Get and bind data to series drop down
            XmlElement SeriesItem = (XmlElement)doc.SelectSingleNode(xPathExpression);

            taxonomtypes = SeriesItem.Attributes["taxonomtypes"].Value;
            from = SeriesItem.Attributes["from"].Value;
            includeYears = SeriesItem.Attributes["includeYears"].Value;
            DataSet SeriesData = new DataSet();
            if (from.Equals("taxonomtypes"))
            {
                SeriesData = AnalaysisData.GetAxisData(taxonomtypes, includeYears.Equals("true"));
            }
            else if (from.Equals("dataset"))
            {
                SeriesData = SqlDataService.GetTaxonomyData(Convert.ToInt32(taxonomtypes));
            }

            SeriesDropDown.DataSource = SeriesData;
            SeriesDropDown.DataTextField = SeriesData.Tables[0].Columns["Name"].ToString();
            SeriesDropDown.DataValueField = SeriesData.Tables[0].Columns["ID"].ToString();
            SeriesDropDown.DataBind();

            try
            {
                xAxisDropdown.SelectedValue = GlobalSettings.DefaultXAxisType;
                SeriesDropDown.SelectedValue = GlobalSettings.DefaultSeriesType;
            }
            catch (Exception ex) { }


            //Get Y-Axis data.
            string SelectionsXML = GetFilterCriteriaFromSession();

            DataSet YAxisDataSet = SqlDataService.GetYAxisTaxonomyByCriteria(SelectionsXML, true);
            if (YAxisDataSet.Tables.Count > 0)
            {
                //Binding Y-Axis
                YAxisDropDown.DataSource = YAxisDataSet;
                YAxisDropDown.DataTextField = YAxisDataSet.Tables[0].Columns["Name"].ToString();
                YAxisDropDown.DataValueField = YAxisDataSet.Tables[0].Columns["ID"].ToString();
                YAxisDropDown.DataBind();

                try
                {
                    YAxisDropDown.SelectedValue = GlobalSettings.DefaultYAxisType;
                }
                catch (Exception ex) { }
            }
        }
        
        /// <summary>
        /// Gets the data filter XML.
        /// </summary>
        /// <returns></returns>
        private string GetDataFilterXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {

                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE") && !taxonomyRow["ID"].ToString().Equals("999"))
                    {
                        //Get the x axis, series taxonomies filter data from session
                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()) ||
                            SeriesDropDown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                            if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                            {
                                //Get all keys (ids) from dictionary
                                List<int> keys = new List<int>(selectedIDsList.Keys);
                                //convert int array to string array
                                string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                                TaxonomySelectedValue = string.Join(",", SelectedIDs);
                            }
                        }
                        else
                        {
                            //Get selected taxonomy id from user control's hidden field
                            if (ChartFilters1.FindControl("Taxonomy" + taxonomyRow["ID"].ToString() + "Selection") != null)
                            {
                                TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("Taxonomy" + taxonomyRow["ID"].ToString() + "Selection")).Value;
                            }

                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue);
                        }
                    }
                    else if (taxonomyRow["ID"].ToString().Equals("999"))
                    {

                        if (xAxisDropdown.SelectedValue.Equals(taxonomyRow["ID"].ToString()) ||
                            SeriesDropDown.SelectedValue.Equals(taxonomyRow["ID"].ToString()))
                        {
                            //Year is selected as x-axis or series
                            //Get the years filter data from session
                            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
                            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

                            TaxonomySelectedValue = StartYear.ToString();
                            while (EndYear > StartYear)
                            {
                                StartYear++;
                                TaxonomySelectedValue += "," + StartYear;
                            }

                        }
                        else
                        {
                            //Get selected year value from user control's hidden field
                            TaxonomySelectedValue = ((HiddenField)ChartFilters1.FindControl("Taxonomy999Selection")).Value;

                        }
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue);
                        }
                    }

                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }

        /// <summary>
        /// Handles the Click event of the PlotChart control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="System.EventArgs"/> instance containing the event data.</param>
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            //Get selection criteria   
            DataFilterXML = GetDataFilterXML();
            //Get selected year from user control's hidden field
            SelectedYear = ((HiddenField)ChartFilters1.FindControl("Taxonomy999Selection")).Value;

            ChartOptions = ((HiddenField)ChartFilters1.FindControl("ChartType")).Value;
            xaxisTypeID = Int32.Parse(xAxisDropdown.SelectedValue);
            yAxisTaxonomyID = Int32.Parse(YAxisDropDown.SelectedValue);
            seriesTypeID = Int32.Parse(SeriesDropDown.SelectedValue);

            PlotScrollChart();
        }

        protected void PlotScrollChart()
        {
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetAnalysisData(xaxisTypeID,
                yAxisTaxonomyID,
                seriesTypeID,
                DataFilterXML);

            //Plot chart
            if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                
                string ChartType = "2D";
                string ChartSeriesType = "Line";
                //= ((DropDownList)ChartFilters1.FindControl("ChartTypeDropDown")).SelectedItem.Text;
                if (!string.IsNullOrEmpty(ChartOptions))
                {
                    ChartType = ChartOptions.Split('|')[0];
                    ChartSeriesType = ChartOptions.Split('|')[1];
                }

                if (ChartType == "3D")
                {
                    ThreeDimensionalCharts.PlotXYScrollChart(ChartDataset, ThreeDChart, ScrollChart, ChartSeriesType);
                    TwoDChart.Visible = false;
                    ThreeDChart.Visible = true;
                    ScrollChart.Visible = true;
                }
                else
                {
                    TwoDimensionalCharts.PlotXYChart(ChartDataset, TwoDChart, ChartSeriesType,null,null);
                    TwoDChart.Visible = true;
                    ThreeDChart.Visible = false;
                }
            }
        }

        ComponentArt.Web.Visualization.Charting.Chart getChart(Control p)
        {
            foreach (Control c in p.Controls)
            {
                if (c.GetType() == typeof(ComponentArt.Web.Visualization.Charting.Chart))
                    return (ComponentArt.Web.Visualization.Charting.Chart)c;

                ComponentArt.Web.Visualization.Charting.Chart wc = getChart((Control)c);
                if (wc != null)
                    return wc;

            }
            return null;
        }
       
    }
}

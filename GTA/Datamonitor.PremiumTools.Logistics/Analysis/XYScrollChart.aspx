<%@ Page Language="C#" 
        MasterPageFile="~/MasterPages/Logistics.Master" 
        AutoEventWireup="true" 
        CodeBehind="XYScrollChart.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Generic.Analysis.XYScrollChart" 
        Title="Datamonitor PremiumTools - Upstream Oil & Gas Data, Analyse results" %>
<%@ Register TagPrefix="ComponentArt" 
        Namespace="ComponentArt.Web.Visualization.Charting" 
        Assembly="ComponentArt.Web.Visualization.Charting" %>  
<%@ Register Assembly="dotnetCHARTING" 
        Namespace="dotnetCHARTING" 
        TagPrefix="dotnetCHARTING" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>
<%@ Register Src="../Controls/ChartFilters.ascx" TagName="ChartFilters" TagPrefix="GPMT" %>
<asp:Content ID="Content1" ContentPlaceHolderID="LogisticsMaster" runat="server">
<link rel="stylesheet" type="text/css" href="../Assets/css/arcticwhite.css" media="all" />
<script type="text/javascript">
function ShowHideFilterOptions()
{   
    var XAxis= "#<%= xAxisDropdown.ClientID%>";    
    var Series = "#<%= SeriesDropDown.ClientID%>";
    var selectedXAxis = $(XAxis).val();
    var selectedSeries = $(Series).val();
    $("#filterContainer > div").show();
     $("#FilterDiv"+selectedXAxis).hide();
     $("#FilterDiv"+selectedSeries).hide();
}
$(document).ready(function() {
    setTimeout("ShowHideFilterOptions()",500);
});
</script>
<div id="maincontent_full">
    <GMPT:PageTitleBar id="PageTitleBar1" runat="server"></GMPT:PageTitleBar>
    <ul id="productscan_tabs">
        <li><asp:HyperLink ID="SearchLink" runat="server" NavigateUrl="~/Search.aspx" Text="Search"></asp:HyperLink></li>
        <li><asp:HyperLink ID="ViewResultsLink" runat="server" NavigateUrl="~/Results/Default.aspx" Text="View Results"></asp:HyperLink></li>
        <li><a class="selected">Analyze Results</a></li>
    </ul> 
    <br />
    <div id="column_other_chartnav">
        <div id="navSection" class="refinesearch">           
            <h2>Analytical options </h2>
            <ul>
              <li><a href="XYChart.aspx"> XY Chart</a></li>
              <li><a href="DualYAxisChart.aspx">Dual Y-Axis Chart</a></li>
              <li><a href="PieChart.aspx">Pie Chart</a></li>
              <li><a href="BarTimeSeries.aspx">Bars(time Series)+Pie</a></li>              
              <li><a href="BubbleChart.aspx">Bubble Chart</a></li>
              <li><a href="GrowthComparison.aspx">Growth Comparison</a></li>
              <li><a href="HeatMap.aspx">Heat Map</a></li>
              <li><a href="CustomChart.aspx">Build Custom Chart</a></li>
              <li class="selected"><a href="#"> XY Scroll Chart</a></li>
            </ul>  

        </div>
    </div>
    <div id="maincontent_chart" class="other_styles">
        <h2>X-Y Chart Title</h2>
        <div class="analyzeresults_col1">
            <div style="margin-bottom:10px;"> 
                X Axis <asp:DropDownList ID="xAxisDropdown" runat="server" 
                Width="110px" Font-Size="11px"></asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;
                Y Axis <asp:DropDownList ID="YAxisDropDown" runat="server" Width="110px" Font-Size="11px"></asp:DropDownList>            
                &nbsp;&nbsp;&nbsp;&nbsp; 
                Series type<asp:DropDownList ID="SeriesDropDown" runat="server" Width="110px" Font-Size="11px"></asp:DropDownList>
            </div>
            <div class="iReport_figure">

                <ul class="iReport_extract">
                    <%--<li class="iReport_extract_nolink">Figure 1</li>--%>
                    <li class="iReport_extract_nolink">Extract to...</li>
                    <li><a href="http://www.datamonitor.com/kc/officeExport/OfficeExport.aspx?pid=CSCM0284&amp;sid=d0e32&amp;iid=d0e130&amp;type=CompanyReport&amp;ExtractType=FigureExtract&amp;app=word&amp;site=consumer "><img width="12" height="10" border="0" src="../assets/images/tool_word.gif" alt=""/>Word</a></li>
                    <li><a href="http://www.datamonitor.com/kc/officeExport/OfficeExport.aspx?pid=CSCM0284&amp;sid=d0e32&amp;iid=d0e130&amp;type=CompanyReport&amp;ExtractType=FigureExtract&amp;app=ppt&amp;site=consumer"><img width="12" height="10" border="0" src="../assets/images/tool_excel.gif" alt=""/>Excel</a></li>
                    <li><a href="http://www.datamonitor.com/kc/officeExport/OfficeExport.aspx?pid=CSCM0284&amp;sid=d0e32&amp;iid=d0e130&amp;type=CompanyReport&amp;ExtractType=FigureExtract&amp;app=ppt&amp;site=consumer"><img width="12" height="10" border="0" src="../assets/images/tool_powerpoint.gif" alt=""/>PowerPoint</a></li>						                    
                </ul>
            </div>


            <ComponentArt:Chart id="ThreeDChart" runat="server" 
                BackColor="White" 
                RenderingPrecision="0.1"            
                SaveImageOnDisk="False" 
                Visible="false">
            </ComponentArt:Chart>
             <ComponentArt:Chart id="ScrollChart" runat="server" 
                BackColor="White" 
                RenderingPrecision="0.1"                            
                SaveImageOnDisk="False"
                Visible="false">               
            </ComponentArt:Chart>

            <dotnetCHARTING:Chart ID="TwoDChart" runat="server" 
                Visible="false">
            </dotnetCHARTING:Chart>
        </div>
        <div class="analyzeresults_col2" id="analyzeresults_filter">
            <div id="filterContainer">
                <GPMT:ChartFilters ID="ChartFilters1" runat="server" />        
            </div>
            <div class="button_right_chart" style="width:120px">
                <asp:LinkButton ID="PlotChart"  runat="server" Text="Plot chart" OnClick="PlotChart_Click">
                </asp:LinkButton>
            </div>


            <br /><br />
                <h1>MY TOOLS</h1>
                <h2 class="tool_excel"><a href="#">Extract to Excel</a></h2>
                <h2 class="tool_word"><a href="#">Extract to Word</a></h2>
                <h2 class="tool_powerpoint"><a href="#">Extract to Powerpoint</a></h2>
        <br /><br /><br /><br /></div>
    </div>
</div>


</asp:Content>

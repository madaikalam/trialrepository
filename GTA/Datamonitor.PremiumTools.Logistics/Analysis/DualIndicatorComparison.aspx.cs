using System;
using System.Data;
using System.Xml;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI.WebControls;
using Datamonitor.PremiumTools.Generic.Controls;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Generic.Library;
using System.Drawing;
using System.IO;
using Aspose.Cells;
using Aspose.Slides;
using Aspose.Words;
using dotnetCHARTING;
using CArt = ComponentArt.Web.UI;

namespace Datamonitor.PremiumTools.Generic.Analysis
{
    public partial class DualIndicatorComparison : AuthenticationBasePage
    {
        //PRIVATE MEMBERS
        private string AxisCriteria = string.Empty;
        private bool IsPredefinedChart = false;
        private bool IsIndicatorsVisible = true;

        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            if (!IsPostBack &&
                !ChartsCallback.IsCallback)
            {
                BindControls();
                //LineRadio.Checked = true;
                hiddenChartType.Value = "3";
                StartYearDropDown.Attributes.Add("onchange", "javascript:Years_onChange('startyear');");
                EndYearDropDown.Attributes.Add("onchange", "javascript:Years_onChange('endyear');");
                ChartTypeDropdown.Attributes.Add("onchange", "javascript:OnChartTypeChange(3);");
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.CustomPageTitle = "Analysis";
        }

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            if (!IsPostBack && !ChartsCallback.IsCallback)
            {  
                PlotChart_Click(null, null);                
            }
        }

        // PRIVATE METHODS (initial population)       
        private string GetSelectionsXML()
        {
            string SelectionsXML = string.Empty;
            DataSet TaxonomyDefaults = GlobalSettings.GetTaxonomyDefaults();
            if (TaxonomyDefaults != null && TaxonomyDefaults.Tables.Count > 0)
            {

                //Build selection criteria xml
                foreach (DataRow taxonomyRow in TaxonomyDefaults.Tables[0].Rows)
                {
                    string TaxonomySelectedValue = string.Empty;
                    if (!taxonomyRow["TaxonomyID"].ToString().Equals("IGNORE"))
                    {   
                        Dictionary<int, string> selectedIDsList = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyRow["ID"].ToString());
                        if (selectedIDsList != null && selectedIDsList.Count > 0) //Check if the taxonomy data exists in session
                        {
                            //Get all keys (ids) from dictionary
                            List<int> keys = new List<int>(selectedIDsList.Keys);
                            //convert int array to string array
                            string[] SelectedIDs = Array.ConvertAll<int, string>(keys.ToArray(), delegate(int key) { return key.ToString(); });
                            TaxonomySelectedValue = string.Join(",", SelectedIDs);
                        }
                        
                        if (!string.IsNullOrEmpty(TaxonomySelectedValue))
                        {
                            SelectionsXML += string.Format("<taxonomy id='{0}' selectIDs='{1}' columnID='{2}' />",
                               taxonomyRow["ID"].ToString(),
                               TaxonomySelectedValue,
                               taxonomyRow["ColumnRowID"].ToString());
                        }
                    }
                }
                SelectionsXML = string.Format("<selections>{0}</selections>", SelectionsXML);
            }

            return SelectionsXML;
        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text">The node text.</param>
        /// <param name="value">The node value.</param>
        /// <param name="expanded">Is node expanded</param>
        /// <param name="hassCheckbox">Has checkbox</param>
        /// <param name="isChecked">Is node checked</param>
        /// <returns></returns>
        private CArt.TreeViewNode CreateNode(string text,
            string value,
            bool expanded,
            bool hasCheckbox,
            bool isChecked)
        {
            ComponentArt.Web.UI.TreeViewNode node = new CArt.TreeViewNode();
            node.Text = text;
            node.Value = value;
            node.ID = value;
            node.Expanded = expanded;
            node.ShowCheckBox = hasCheckbox;
            node.Checked = isChecked;
            return node;
        }

        private void BindControls()
        {
            string SelectionsXML = GetFilterCriteriaFromSession();
            DataSet IndicatorsAndCountries = SqlDataService.GetIndicatorsAndCountries(SelectionsXML);
            XmlDocument document = new XmlDocument();
            hdnIsPredefined.Value = "false";
            string startYear=string.Empty;
            string endYear = string.Empty;

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                hdnIsPredefined.Value = "true";
                DataSet ds = SqlDataService.GetPredefinedChartMetaData(Convert.ToInt32(Request.QueryString["id"]));
                IsPredefinedChart = true;
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    string filterXml = ds.Tables[0].Rows[0]["Selections"].ToString();
                    document.LoadXml(filterXml);
                    ChartTitle.Text= document.SelectSingleNode("//Title").InnerText;
                    hdnIndicators.Value = document.SelectSingleNode("//Indicators").Attributes["SelectedValue"].Value;
                    hdnIndicatorText.Value = document.SelectSingleNode("//Indicators").Attributes["SelectedText"].Value;
                    IsIndicatorsVisible = Convert.ToBoolean(document.SelectSingleNode("//Indicators").Attributes["Visible"].Value);
                    startYear = document.SelectSingleNode("//Years").Attributes["start"].Value;
                    endYear = document.SelectSingleNode("//Years").Attributes["end"].Value;
                    chartTypeDiv.Visible = document.SelectSingleNode("//Yaxis1").Attributes["SeriesChatType"].Value.Length == 0;
                    hdnYaxis1SeriesTypes.Value = document.SelectSingleNode("//Yaxis1").Attributes["SeriesChatType"].Value;
                    hdnYaxis2SeriesTypes.Value = document.SelectSingleNode("//Yaxis2").Attributes["SeriesChatType"].Value;
                }
            }

            if (IsIndicatorsVisible)
            {//binding indicator1 data
                foreach (DataRow dbRow in IndicatorsAndCountries.Tables[0].Rows)
                {
                    //Indicator1
                    ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(dbRow["DisplayName"].ToString(),
                          dbRow["rowID"].ToString(),
                          true,
                          false,
                          false);

                    Indicator1Tree.Nodes.Add(newNode);
                    //Indicator2
                    newNode = CreateNode(dbRow["DisplayName"].ToString(),
                          dbRow["rowID"].ToString(),
                          true,
                          false,
                          false);

                    Indicator2Tree.Nodes.Add(newNode);
                }

                hdnIndicator1.Value = Indicator1Tree.Nodes.Count > 0 ? Indicator1Tree.Nodes[0].Value : "0";
                Indicator1Combo.Text = Indicator1Tree.Nodes.Count > 0 ? Indicator1Tree.Nodes[0].Text : "";

                hdnIndicator2.Value = Indicator2Tree.Nodes.Count > 1 ? Indicator2Tree.Nodes[1].Value : "0";
                Indicator2Combo.Text = Indicator2Tree.Nodes.Count > 1 ? Indicator2Tree.Nodes[1].Text : "";

                hdnIndicatorText.Value = Indicator1Combo.Text + "; " + Indicator2Combo.Text;
            }
            else
            {
                Indicator1Div.Visible = false;
                Indicator2Div.Visible = false;
            }

            //Assign default values of xaxis to  hidden fields
            hdnXaxis.Value = XaxisCombo.Text;
            hdnXaxisText.Value = XaxisCombo.Text;

            //Get list of valid years from session
            int StartYear = CurrentSession.GetFromSession<int>("StartYear");
            int EndYear = CurrentSession.GetFromSession<int>("EndYear");

            //Start year combo
            //End year combo
            for (int counter = StartYear; counter <= EndYear; counter++)
            {
                StartYearDropDown.Items.Add(new ListItem(counter.ToString()));
                EndYearDropDown.Items.Add(new ListItem(counter.ToString()));
            }

            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                StartYearDropDown.SelectedValue = StartYear.ToString();
                EndYearDropDown.SelectedValue = EndYear.ToString();
                hdnStartYear.Value = StartYear.ToString();
                hdnEndYear.Value = EndYear.ToString();
            }
            else
            {
                StartYearDropDown.SelectedValue = string.IsNullOrEmpty(startYear) ? StartYearDropDown.Items[0].Value : startYear;
                EndYearDropDown.SelectedValue = string.IsNullOrEmpty(endYear) ? EndYearDropDown.Items[EndYearDropDown.Items.Count - 1].Value : endYear;
                hdnStartYear.Value = StartYearDropDown.SelectedValue;
                hdnEndYear.Value = EndYearDropDown.SelectedValue;
            }
            
        }

        // EVENT HANDLERS
        protected void PlotChart_Click(object sender, EventArgs e)
        {
            string Indicators;
            Indicators = string.IsNullOrEmpty(Request.QueryString["id"]) ? hdnIndicator1.Value + "," + hdnIndicator2.Value : hdnIndicators.Value;
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetIndicatorComparisonChartData(Indicators,
                Convert.ToInt32(hdnXaxis.Value),
               Convert.ToInt32(StartYearDropDown.SelectedValue),
               Convert.ToInt32(EndYearDropDown.SelectedValue));

            LogUsage("Charts - Indicator Comparison", hdnXaxis.Value, hdnIndicator1.Value + "," + hdnIndicator2.Value);
            dotnetCHARTING.Chart targetChart;
            //Plot chart
            if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartSeriesType = ChartTypeDropdown.SelectedValue;  //LineRadio.Checked ? "3" : "7";
                
                
                if (ChartDataset.Tables.Count > 1 && ChartDataset.Tables[1].Rows.Count > 1)
                {
                    if (string.IsNullOrEmpty(Request.QueryString["id"]))
                    {
                        targetChart = TwoDimensionalCharts.PlotIndicatorComparisonChart(ChartDataset,
                                                  TwoDChart,
                                                  ChartSeriesType,
                                                  ChartDataset.Tables[1].Rows[0][0].ToString(),
                                                  ChartDataset.Tables[1].Rows[1][0].ToString(),
                                                  "Country: CountryCombo.Text"  );
                        TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                        targetChart.Visible = false;
                    }
                    else
                    {
                        targetChart = TwoDimensionalCharts.PlotMultiSeriesIndicatorComparisonChart(ChartDataset,
                                                 TwoDChart,
                                                 ChartSeriesType,
                                                 ChartDataset.Tables[1].Rows[0][0].ToString(),
                                                 ChartDataset.Tables[1].Rows[1][0].ToString(),
                                                 "Country: CountryCombo.Text",
                                                 hdnYaxis1SeriesTypes.Value,
                                                 hdnYaxis2SeriesTypes.Value);
                        TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                        targetChart.Visible = false;
                    }
                }
                else
                {
                    targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        ChartDataset.Tables[0].Rows[0][3].ToString(),
                        "Country: CountryCombo.Text");
                    TwoDimensionalCharts.AddCopyrightText(targetChart, 370); 
                    targetChart.Visible = false;
                }
               

                if (!Directory.Exists(Server.MapPath("temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("temp"));
                }
                targetChart.FileManager.TempDirectory = "temp";
                // Save the legend as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.BottomMiddle;
                if (targetChart.SeriesCollection.Count > 3)
                {
                    targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                }
                LegendImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");
                LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                targetChart.Width = Unit.Point(520);
                targetChart.Height = Unit.Point(300);
                // Remove the legend from the chart and save the chart as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.None;
                ChartImg.ImageUrl = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap()).Replace("\\", "/");

                ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                ImageMapLabel.Text = targetChart.ImageMapText;

                //Save the chart in viewstate, used in extractions.
                ViewState["2DChartImage"] = ChartImg.ImageUrl;
                hdnImageurl.Value = ChartImg.ImageUrl;
                hdnLegendUrl.Value = LegendImg.ImageUrl;      
            }          
        }              

        protected void Chart_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetIndicatorComparisonChartData(e.Parameters[0].ToString(),
                Convert.ToInt32(e.Parameters[1].ToString()),
                Int32.Parse(e.Parameters[3].ToString()),
                Int32.Parse(e.Parameters[4].ToString()));

            //Plot chart
            if (ChartDataset.Tables.Count > 0 && ChartDataset.Tables[0].Rows.Count > 0)
            {
                string ChartSeriesType = e.Parameters[2].ToString();

                dotnetCHARTING.Chart targetChart;
               if (ChartDataset.Tables.Count > 1 && ChartDataset.Tables[1].Rows.Count > 1)                
                {
                    if (e.Parameters[6].ToString()!="true")
                    {
                        targetChart = TwoDimensionalCharts.PlotIndicatorComparisonChart(ChartDataset,
                           TwoDChart,
                           ChartSeriesType,
                           ChartDataset.Tables[1].Rows[0][0].ToString(),
                           ChartDataset.Tables[1].Rows[1][0].ToString(),
                           "Country: e.Parameters[5].ToString()");
                    }
                    else
                    {
                        targetChart = TwoDimensionalCharts.PlotMultiSeriesIndicatorComparisonChart(ChartDataset,
                          TwoDChart,
                          ChartSeriesType,
                          ChartDataset.Tables[1].Rows[0][0].ToString(),
                          ChartDataset.Tables[1].Rows[1][0].ToString(),
                          "Country: e.Parameters[5].ToString()",
                           e.Parameters[7].ToString(),
                            e.Parameters[8].ToString());
                    }
                   TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                }
                else
                {
                     targetChart = TwoDimensionalCharts.PlotXYChart(ChartDataset,
                        TwoDChart,
                        ChartSeriesType,
                        ChartDataset.Tables[0].Rows[0][3].ToString(),
                        "Country: e.Parameters[5].ToString()");
                     TwoDimensionalCharts.AddCopyrightText(targetChart, 370);
                }

                targetChart.Visible = false;

                if (!Directory.Exists(Server.MapPath("temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("temp"));
                }

                targetChart.FileManager.TempDirectory = "temp";

                // Save the legend as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.BottomMiddle;
                if (targetChart.SeriesCollection.Count > 3)
                {
                    targetChart.Height = Unit.Point(150 + ((targetChart.SeriesCollection.Count - 3) * 30));
                }
                string strLegendPath = targetChart.FileManager.SaveImage(targetChart.GetLegendBitmap()).Replace("\\", "/");

                targetChart.Width = Unit.Point(520);
                targetChart.Height = Unit.Point(300);
                // Remove the legend from the chart and save the chart as a separate image file
                targetChart.LegendBox.Position = LegendBoxPosition.None;
                string strChartPath = targetChart.FileManager.SaveImage(targetChart.GetChartBitmap());

                ChartImg.ImageUrl = ChartImg.ResolveUrl(strChartPath);
                ChartImg.Attributes.Add("usemap", targetChart.ImageMapName);
                ImageMapLabel.Text = targetChart.ImageMapText;
                ChartImg.RenderControl(e.Output);
                hdnImageurl.Value = strChartPath;
                hdnImageurl.RenderControl(e.Output);
                ImageMapLabel.RenderControl(e.Output);

                LegendImg.ImageUrl = ChartImg.ResolveUrl(strLegendPath);
                LegendImg.Visible = string.IsNullOrEmpty(LegendImg.ImageUrl.Trim()) ? false : true;
                divChart.RenderControl(e.Output);
                hdnLegendUrl.Value = strLegendPath;
                hdnLegendUrl.RenderControl(e.Output);
            }
        }
        
        //EXPORT EVENT HANDLERS       
        protected void lnkWord_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            string Indicators;
            Indicators = string.IsNullOrEmpty(Request.QueryString["id"]) ? hdnIndicator1.Value + "," + hdnIndicator2.Value : hdnIndicators.Value;
            LogUsage("Charts - Indicator Comparison - Word download", hdnXaxis.Value, hdnIndicator1.Value + "," + hdnIndicator2.Value);

            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetIndicatorComparisonChartData(Indicators,
               Convert.ToInt32(hdnXaxis.Value),
               Convert.ToInt32(StartYearDropDown.SelectedValue),
               Convert.ToInt32(EndYearDropDown.SelectedValue));


            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts

            string Years = "";
            for (int cnt = Int32.Parse(StartYearDropDown.SelectedValue); cnt <= Int32.Parse(EndYearDropDown.SelectedValue); cnt++)
            {
                Years += cnt.ToString() + ",";
            }
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            //SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", Years.TrimEnd(','));
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Document doc = ExtractionsHelper.ExportToWord(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "IndicatorComparisonChart",
                    WordTemplatePath,
                    ChartTitle.Text);
                doc.Save("OvumExtract.doc", SaveFormat.FormatDocument, Aspose.Words.SaveType.OpenInWord, Response);
            }
        }
       
        protected void lnkExcel_Click(object sender, EventArgs e)
        {
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            string Indicators;
            Indicators = string.IsNullOrEmpty(Request.QueryString["id"]) ? hdnIndicator1.Value + "," + hdnIndicator2.Value : hdnIndicators.Value;
            LogUsage("Charts - Indicator Comparison - Excel download", hdnXaxis.Value, hdnIndicator1.Value + "," + hdnIndicator2.Value);
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetIndicatorComparisonChartData(Indicators,
               Convert.ToInt32(hdnXaxis.Value),
               Convert.ToInt32(StartYearDropDown.SelectedValue),
               Convert.ToInt32(EndYearDropDown.SelectedValue));

            string ChartType = string.Empty;

            if (hdnImageurl.Value.Length > 0 )
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts

            string Years = "";
            for (int cnt = Int32.Parse(StartYearDropDown.SelectedValue); cnt <= Int32.Parse(EndYearDropDown.SelectedValue); cnt++)
            {
                Years += cnt.ToString() + ",";
            }
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            //SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", Years.TrimEnd(','));
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            Workbook Excel;
            if (chartBitmap != null)
            {
                Excel = ExtractionsHelper.ExportToExcel(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "IndicatorComparisonChart",
                    ExcelTemplatePath, 
                    LogoPath,
                    ChartTitle.Text);

                Excel.Save("Ovum" + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + ".xls", Aspose.Cells.FileFormatType.Default, Aspose.Cells.SaveType.OpenInExcel, Response);
            } 

        }
       
        protected void lnkPPT_Click(object sender, EventArgs e)
        {
            string strExtractName = "OvumExtract";
            string chartImagePath = string.Empty;
            Bitmap chartBitmap = null;
            string legendImagePath = string.Empty;
            Bitmap legendBitmap = null;

            LogUsage("Charts - Indicator Comparison - PPT download", hdnXaxis.Value, hdnIndicator1.Value + "," + hdnIndicator2.Value);

            string Indicators;
            Indicators = string.IsNullOrEmpty(Request.QueryString["id"]) ? hdnIndicator1.Value + "," + hdnIndicator2.Value : hdnIndicators.Value;
            //Get chart data 
            DataSet ChartDataset = SqlDataService.GetIndicatorComparisonChartData(Indicators,
               Convert.ToInt32(hdnXaxis.Value),
               Convert.ToInt32(StartYearDropDown.SelectedValue),
               Convert.ToInt32(EndYearDropDown.SelectedValue));

            if (hdnImageurl.Value.Length > 0)
            {
                chartImagePath = Server.MapPath(hdnImageurl.Value.Replace('\\', '/'));
                chartBitmap = new Bitmap(chartImagePath);
            }
            if (hdnLegendUrl.Value.Length > 0)
            {
                legendImagePath = Server.MapPath(hdnLegendUrl.Value.Replace('\\', '/'));
                legendBitmap = new Bitmap(legendImagePath);
            }

            #region build Filter criteria xml to show in extracts

            string Years = "";
            for (int cnt = Int32.Parse(StartYearDropDown.SelectedValue); cnt <= Int32.Parse(EndYearDropDown.SelectedValue); cnt++)
            {
                Years += cnt.ToString() + ",";
            }
            string SelectionsTextXML = string.Format("<taxonomy id='Indicator' selectIDs='{0}'/>", Microsoft.JScript.GlobalObject.escape(hdnIndicatorText.Value));
            //SelectionsTextXML += string.Format("<taxonomy id='Country' selectIDs='{0}'/>", hdnCountryText.Value.Replace(';', ','));
            SelectionsTextXML += string.Format("<taxonomy id='Year' selectIDs='{0}'/>", Years.TrimEnd(','));
            SelectionsTextXML = string.Format("<selections>{0}</selections>", SelectionsTextXML);

            Session["SelectionsTextXML"] = SelectionsTextXML;

            #endregion

            if (chartBitmap != null)
            {
                Presentation presentation = ExtractionsHelper.ExportToPowerpoint(ChartDataset.Tables[0],
                    chartBitmap,
                    legendBitmap,
                    "IndicatorComparisonChart",  
                    PowerpointTempaltePath,
                    ChartTitle.Text);

                Response.ContentType = "application/vnd.ms-powerpoint";
                Response.AppendHeader("Content-Disposition", "attachment; filename=" + strExtractName + ".ppt");
                presentation.Write(Response.OutputStream);
                Response.Flush();
            }
        }
    }
}


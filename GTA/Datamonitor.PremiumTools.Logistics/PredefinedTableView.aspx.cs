using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Generic;
using Datamonitor.PremiumTools.Generic.Library;
using Datamonitor.PremiumTools.Generic.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Logistics
{
    public partial class PredefinedTableView : System.Web.UI.Page
    {

        private int _movementColumnIndex;
        private const string MOVEMENTCOLUMNNAME="movement";
        //PAGE EVENTS

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["tableid"] != null && !string.IsNullOrEmpty(Request.QueryString["tableid"]))
            {
                DataSet TableData = SqlDataService.GetPredefinedTableData(Convert.ToInt32(Request.QueryString["tableid"].ToString()));
                if (TableData != null &&
                    TableData.Tables.Count > 0 &&
                    TableData.Tables[0].Rows.Count > 0)
                {
                    _movementColumnIndex = TableData.Tables[0].Columns.IndexOf(MOVEMENTCOLUMNNAME);
                    PredefinedTableGrid.DataSource = TableData;
                    PredefinedTableGrid.DataBind();
                }

                if (Request.QueryString["heading"] != null)
                {
                    ChartHeading.Text = Request.QueryString["heading"].ToString();
                }
            }

        }

        protected void PredefinedTableGrid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                
                if(_movementColumnIndex>-1)
                {
                    string MovementValueText = e.Row.Cells[_movementColumnIndex].Text;
                    e.Row.Cells[_movementColumnIndex].HorizontalAlign = HorizontalAlign.Center;
                    if (!string.IsNullOrEmpty(MovementValueText))
                    {
                        int MovementValue = Convert.ToInt32(MovementValueText);

                        if (MovementValue == 0)
                        {
                            e.Row.Cells[_movementColumnIndex].Text = "no change";
                        }
                        else if (MovementValue > 0)
                        {
                            e.Row.Cells[_movementColumnIndex].Text = "";
                            HtmlImage StatusImage = new HtmlImage();
                            StatusImage.Src = "assets/images/uparrow.gif";
                            e.Row.Cells[_movementColumnIndex].Controls.Add(StatusImage);
                        }
                        else
                        {
                            e.Row.Cells[_movementColumnIndex].Text = "";
                            HtmlImage hm = new HtmlImage();
                            hm.Src = "assets/images/downarrow.gif";
                            e.Row.Cells[_movementColumnIndex].Controls.Add(hm);
                        }
                    }
                }
                
            }
        }
    }
}

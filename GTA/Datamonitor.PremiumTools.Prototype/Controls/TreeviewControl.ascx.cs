using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Xml;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;
using Datamonitor.PremiumTools.Prototype.Library;

namespace Datamonitor.PremiumTools.Prototype.Controls
{
    public partial class TreeviewControl : BaseSelectionControl
    {  
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                tvForAll.Nodes.Clear();
                LoadData();
            }
        }

        /// <summary>
        /// Gets the treeview data and binds.
        /// </summary>        
        /// <param name="taxonomyTypeID">The taxonomy type id.</param>
        public override void LoadData()
        {
            tvForAll.Nodes.Clear();
            DataTable TaxonomyTable;
            DataSet TaxonomyList;           
            bool ShowTreeLines = true;
            string SiteName = CurrentSite.SiteKey;
            
            TaxonomyList = (ParentIDs!=null && ParentIDs.Length > 0) ?
                 SqlDataService.GetTaxonomyForSelection(TaxonomyTypeID, ParentIDs, SiteName) :
                 SqlDataService.GetTaxonomyList(TaxonomyTypeID, Source, CurrentSite.TaxonomyDefaultsConfigPath, SiteName);                           

            //check whether treelines can be shown or not
            string FilePath = Server.MapPath(CurrentSite.TaxonomyLinksConfigPath);
            XmlDocument TaxonomyLinks = new XmlDocument();
            TaxonomyLinks.Load(FilePath);
            XmlNode TaxonomyLink = TaxonomyLinks.SelectSingleNode("//Taxonomies/Taxonomy[@ID='" + TaxonomyTypeID + "'][@FullID='" + 
                 (string.IsNullOrEmpty(FullTaxonomyTypeID)? TaxonomyTypeID.ToString():FullTaxonomyTypeID) + "']");
            if (TaxonomyLink != null)
            {
                ShowTreeLines = TaxonomyLink.Attributes.GetNamedItem("ShowTreeLines").Value.Equals("true") ? true : false;
            }
            tvForAll.ShowLines = ShowTreeLines;


            TaxonomyTable = TaxonomyList.Tables.Count > 0 ? TaxonomyList.Tables[0] : null;

            TaxonomyList.Relations.Add("NodeRelation", TaxonomyList.Tables[0].Columns["ID"], TaxonomyList.Tables[0].Columns["parentTaxonomyID"]);

            //Get current selections from sessionstate
            Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(TaxonomyTypeID.ToString());

            foreach (DataRow dbRow in TaxonomyTable.Rows)
            {
                if (dbRow.IsNull("parentTaxonomyID"))
                {
                    ComponentArt.Web.UI.TreeViewNode ParentNode = CreateNode(dbRow["ID"].ToString(),
                        dbRow["Name"].ToString(),
                        dbRow["displayName"].ToString(),
                        Convert.ToBoolean(dbRow["isLeafNode"].ToString()),
                        Convert.ToBoolean(dbRow["expandNode"].ToString()),
                        "",
                        Convert.ToBoolean(dbRow["ShowCheckBox"].ToString()));

                    bool HasDefinition = dbRow["definition"] != null && dbRow["definition"].ToString().Length > 0;                    

                    ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DT" : "NT";
                    //if (Source == "search1")
                    //{
                        if (CurrentSelections != null && CurrentSelections.ContainsKey(Convert.ToInt32(dbRow["ID"])))
                        {
                            if (CurrentSelections[Convert.ToInt32(dbRow["ID"])].Equals(dbRow["displayName"].ToString() + "(All)") && (Source == "search1"))
                            {
                                ParentNode.Checked = true;
                            }
                            else if (CurrentSelections[Convert.ToInt32(dbRow["ID"])].Equals(dbRow["displayName"].ToString()))
                            {
                                if (Convert.ToBoolean(dbRow["isDataAvailable"].ToString()) && dbRow.GetChildRows("NodeRelation").Length > 0 && (Source == "search1"))
                                {
                                    ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPn" : "NTPn";
                                }
                                else
                                {
                                    ParentNode.Checked = true;
                                }
                            }
                        }
                        else
                        {
                            if (Convert.ToBoolean(dbRow["isDataAvailable"].ToString()) && dbRow.GetChildRows("NodeRelation").Length > 0 && (Source == "search1"))
                            {
                                ParentNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPnSel" : "NTPnSel";
                            }
                        }
                    //}
                    //populate sub level tree if child nodes exists
                    if (dbRow.GetChildRows("NodeRelation").Length > 0)
                    {
                        PopulateSubTree(dbRow, ParentNode, CurrentSelections);
                    }
                    
                    tvForAll.Nodes.Add(ParentNode);
                }
            }

            if (tvForAll.Nodes.Count == 1)
            {
                tvForAll.Nodes[0].Expanded = true;
            }
        }

        /// <summary>
        /// Populates the sub tree.
        /// </summary>
        /// <param name="dbRow">The data row.</param>
        /// <param name="node">The node.</param>
        /// <param name="CurrentSelections">The current selections.</param>
        private void PopulateSubTree(DataRow dbRow, 
            TreeViewNode node,
            Dictionary<int, string> currentSelections)
        {            
            foreach (DataRow childRow in dbRow.GetChildRows("NodeRelation"))
            {
                //ComponentArt.Web.UI.TreeViewNode childNode = CreateNode(childRow["Text"].ToString(), childRow["ImageUrl"].ToString(), true);
                ComponentArt.Web.UI.TreeViewNode newNode = CreateNode(childRow["ID"].ToString(),
                        childRow["Name"].ToString(),
                        childRow["displayName"].ToString(),
                        Convert.ToBoolean(childRow["isLeafNode"].ToString()),
                        Convert.ToBoolean(childRow["expandNode"].ToString()),
                        "",
                        Convert.ToBoolean(childRow["ShowCheckBox"].ToString()));

                bool HasDefinition = childRow["definition"] != null && childRow["definition"].ToString().Length > 0;
               

                newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DT" : "NT";
                newNode.Checked = node.Checked;
                //if (Source == "search1")
                //{
                    if (currentSelections != null && currentSelections.ContainsKey(Convert.ToInt32(childRow["ID"])))
                    {
                        if (currentSelections[Convert.ToInt32(childRow["ID"])].Equals(childRow["displayName"].ToString() + "(All)") && (Source == "search1"))
                        {
                            newNode.Checked = true;
                        }
                        else if (currentSelections[Convert.ToInt32(childRow["ID"])].Equals(childRow["displayName"].ToString()))
                        {
                            if (Convert.ToBoolean(childRow["isDataAvailable"].ToString()) && childRow.GetChildRows("NodeRelation").Length > 0 && (Source == "search1"))
                            {
                                newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPn" : "NTPn";
                            }
                            else
                            {
                                newNode.Checked = true;
                            }
                        }
                    }
                    else
                    {
                        if (Convert.ToBoolean(childRow["isDataAvailable"].ToString()) && childRow.GetChildRows("NodeRelation").Length > 0 && (Source == "search1"))
                        {
                            newNode.ClientTemplateId = HasDefinition && ShowTaxonomyDefinition ? "DTPnSel" : "NTPnSel";
                        }
                    }
                //}
                
                //populate sub level tree if child nodes exists
                if (childRow.GetChildRows("NodeRelation").Length > 0)
                {
                    PopulateSubTree(childRow, newNode, currentSelections);
                }
                //add current node to parent node
                node.Nodes.Add(newNode);
            }           
        }

        /// <summary>
        /// Creates the node.
        /// </summary>
        /// <param name="nodeID">The id.</param>
        /// <param name="nodeText">The text.</param>
        /// <param name="nodeValue">The value.</param>
        /// <param name="isLeafNode"></param>
        /// <param name="expanded">if set to <c>true</c> [expanded].</param>
        /// <param name="imageUrl">The imageurl.</param>
        /// <param name="showCheckBox"></param>
        /// <returns></returns>
        private ComponentArt.Web.UI.TreeViewNode CreateNode(string nodeID,
            string nodeText,
            string nodeValue,
            bool isLeafNode,
            bool expanded,
            string imageUrl,
            bool showCheckBox)
        {
            ComponentArt.Web.UI.TreeViewNode Node = new ComponentArt.Web.UI.TreeViewNode();
            Node.Text = nodeText;
            Node.ImageUrl = imageUrl;
            Node.Expanded = expanded;
            Node.Value = nodeValue;
            Node.ID = nodeID;
            Node.ShowCheckBox = showCheckBox;
            //if (!isLeafNode)
            //{
            //    Node.ContentCallbackUrl = "TaxonomySubtreeXml.aspx?id=" + nodeID;
            //}
            return Node;
        }

       

    }
}
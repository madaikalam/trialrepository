using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Prototype;
using Datamonitor.PremiumTools.Prototype.Library;

namespace Datamonitor.PremiumTools.Prototype.Controls
{
    public partial class ViewOptions : System.Web.UI.UserControl
    {
        string _source;

        /// <summary>
        /// Gets or sets the value to _source.
        /// </summary>
        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        // EVENT OVERRIDES

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {                
                
                //if switching views is allowed
                if (Source.ToLower()=="view1")
                {
                    //if current page is default view
                    View1LI.Attributes.Add("class", "lozenge");
                    View1Link.NavigateUrl = "";

                    View2LI.Attributes.Add("class", "");
                    View2Link.NavigateUrl = "../" + GlobalSettings.AlternateResultsPage;
                    CountryComparisonLI.Attributes.Add("class", "");
                    CountryComparisonLink.NavigateUrl = "../results/CountryComparisonGrid.aspx";
                    CountryOverviewLI.Attributes.Add("class", "");
                    CountryOverviewLink.NavigateUrl = "../results/CountryOverview.aspx";
                }
                else if (Source.ToLower() == "view2")
                {
                    //if current page is alternative view
                    View2LI.Attributes.Add("class", "lozenge");
                    View2Link.NavigateUrl = "";

                    View1LI.Attributes.Add("class", "");
                    View1Link.NavigateUrl = "../" + GlobalSettings.DefaultResultsPage;
                    CountryComparisonLI.Attributes.Add("class", "");
                    CountryComparisonLink.NavigateUrl = "../results/CountryComparisonGrid.aspx";
                    CountryOverviewLI.Attributes.Add("class", "");
                    CountryOverviewLink.NavigateUrl = "../results/CountryOverview.aspx";
                }
                else if (Source.ToLower() == "countrycomparison")
                {
                    CountryComparisonLI.Attributes.Add("class", "lozenge");
                    CountryComparisonLink.NavigateUrl = "";

                    View2LI.Attributes.Add("class", "");
                    View2Link.NavigateUrl = "../" + GlobalSettings.AlternateResultsPage;
                    View1LI.Attributes.Add("class", "");
                    View1Link.NavigateUrl = "../" + GlobalSettings.DefaultResultsPage;
                    CountryOverviewLI.Attributes.Add("class", "");
                    CountryOverviewLink.NavigateUrl = "../results/CountryOverview.aspx";
                }
                else if (Source.ToLower() == "countryoverview")
                {
                    CountryOverviewLI.Attributes.Add("class", "lozenge");
                    CountryOverviewLink.NavigateUrl = "";

                    View2LI.Attributes.Add("class", "");
                    View2Link.NavigateUrl = "../" + GlobalSettings.AlternateResultsPage;
                    View1LI.Attributes.Add("class", "");
                    View1Link.NavigateUrl = "../" + GlobalSettings.DefaultResultsPage;
                    CountryComparisonLI.Attributes.Add("class", "");
                    CountryComparisonLink.NavigateUrl = "../results/CountryComparisonGrid.aspx";
                }
                if (!GlobalSettings.ShowAllResultViews)
                {
                    //if switching views is not allowed
                    View1LI.Visible = false;
                    View2LI.Visible = false;
                }
                
                CountryComparisonLI.Visible = GlobalSettings.ShowCountryComparison;
                CountryOverviewLI.Visible = GlobalSettings.ShowCountryOverview;
            }
        }
    }
}
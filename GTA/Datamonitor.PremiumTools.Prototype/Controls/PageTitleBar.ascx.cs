using System;
using System.Data;
using System.Text;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using ComponentArt.Web.UI;
using Datamonitor.PremiumTools.Prototype;
using Datamonitor.PremiumTools.Prototype.Library;

namespace Datamonitor.PremiumTools.Prototype.Controls
{
    public partial class PageTitleBar : System.Web.UI.UserControl
    {
        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
           
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TitleLabel.Text = GlobalSettings.SiteTitle;
            }
        }
    }
}
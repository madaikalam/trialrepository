using System;
using System.Data;
using System.Text;
using System.Web;
using System.Xml;
using System.Collections.Generic;
using System.Web.UI;
using System.Configuration;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Prototype.Library
{   
    /// <summary>
    /// The Base page for all pages in the apllication
    /// </summary>
    public class BasePage : Page
    {
        #region Private fields
        private string _pageTitle;
        private SiteSettings _currentSite;

       
        #endregion

        #region Protected properties
        /// <summary>
        /// Property used to set the custom title of the page. By default the page title will be the site's title e.g. "Datamonitor".
        /// If you wish to append to this title then set this property and the page title will become "{Site title} - {CustomPageTitle}".
        /// </summary>
        protected string CustomPageTitle
        {
            get { return this._pageTitle; }
            set { this._pageTitle = value; }
        }

        /// <summary>
        /// Gets the current page title, taking into account the current value of
        /// the CustomPageTitle property.
        /// </summary>
        protected string PageTitle
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(CurrentSite.SiteTitle);
                if (!string.IsNullOrEmpty(this.CustomPageTitle))
                {
                    sb.Append(" - ");
                    sb.Append(this.CustomPageTitle);
                }
                return sb.ToString();
            }
        }

        public SiteSettings CurrentSite
        {
            get { return _currentSite; }
            set { _currentSite = value; }
        }

        #endregion

        #region Protected virtual methods
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (HttpContext.Current.Request.QueryString["site"] != null)
            {
                _currentSite = new SiteSettings(HttpContext.Current.Request.QueryString["site"].ToString());               
            }
        }
        protected override void OnPreRender(EventArgs e)
        {
            if (this.Page.Header != null)
            {
                // Set Custom Page Title
                this.Title = this.PageTitle;
            }
        }
        #endregion
        
        /// <summary>
        /// Gets the predefined view's selectable Ids.
        /// </summary>
        /// <param name="viewID">The view ID.</param>
        /// <returns></returns>
        protected static string GetPredefinedViewSelectableIDs(string viewID)
        {
            return "";
        }
    }
}

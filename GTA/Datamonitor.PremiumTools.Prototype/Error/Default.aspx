<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" 
        Inherits="Datamonitor.PremiumTools.Prototype.Error.Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">    
    <link href="~/Assets/css/datamonitor.css" rel="stylesheet" type="text/css" />
    <link href="~/Assets/css/layout.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
     <div id=wrapper>    
        <div id="header" class="auto">
            <h1>Automotive Knowledge Centre</h1><a href="http://www.datamonitor.com/kc/auto/" class="orbys">Return to your Knowledge Centre</a>
                       
        </div>
        <div id="maincontent_full"> 
             <div id="maincontent" class="other_styles">
              <div id="login" class="company_styles" runat="server">
                    <div id="div404" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Page Cannot Be Found</b></p><br />
                          <p align="center">Unfortunately, the page you are trying to retrieve does not exist on the Knowledge Center.</p>
                          <p align="center">This is the same as a &quot;HTTP 404: File not found&quot; error and technical support team has already been alerted.</p>
                          <br />
                          <p align="center">Go to <b><a href="../Search.aspx">Home Page</a></b>
                          </p>
                      </div>
                      <div id="div403" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Access Denied</b></p><br />
                          <p align="center">The resource you are trying cannot be accessed.</p>
                          <p align="center">This is the same as a &quot;HTTP 403: Access denied&quot; error and technical support team has already been alerted.</p>
                          <br />
                          <p align="center">Go to <b><a href="../Search.aspx">Home Page</a></b> 
                          </p>
                      </div>
                       <div id="divError" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Sorry.. an error has occured</b></p><br />
                          <p align="center">Unfortunately, the page you are trying to retrieve cannot be displayed and technical support team has already been alerted.</p>              
                          <br /><br />
                          <p align="center">Go to <b><a href="../Search.aspx">Home Page</a></b> 
                          </p>
                      </div>
                      <div id="divSessionExpired" runat="server" visible="false">
                          <p align="center"><img src="../assets/images/transparent.gif" alt="Error" width="100" height="85" class="background_404" /></p><br />
                          <p align="center" class="textcolor"><b>Sorry.. your session expired</b></p><br />                          
                          <br /><br />
                          <p align="center">Go to <b><a href="../Search.aspx">Home Page</a></b> 
                          </p>
                      </div>
                </div>
                <br /><br /><br />
             </div>
        </div>          
        <!--FOOTER-->
            <div style="clear: both">
                <div id="footer_left">
                    &copy; Datamonitor 2009. All rights reserved.</div>
                <div id="footer_right"></div>
            </div>    
    </div>
    </form>
</body>
</html>


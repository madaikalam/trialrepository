<%@ Page Language="C#"
         MasterPageFile="~/MasterPages/Logistics.Master"
         AutoEventWireup="true" 
         CodeBehind="Search.aspx.cs" 
         Inherits="Datamonitor.PremiumTools.Prototype.Search" %>
<%@ Register Src="~/Controls/CheckListControl.ascx" TagName="CheckListControl" TagPrefix="uc1" %>
<%@ Register Src="~/Controls/TreeviewControl.ascx" TagName="TreeviewControl" TagPrefix="uc2" %>
<%@ Register Src="~/Controls/SelectionList.ascx" TagName="Selections" TagPrefix="uc3" %>
<%@ Reference Control="Controls/TreeviewControl.ascx" %>
<%@ Reference Control="Controls/CheckListControl.ascx" %>
<%@ Register Assembly="ComponentArt.Web.UI" Namespace="ComponentArt.Web.UI" TagPrefix="ComponentArt" %>  
      
<asp:Content ID="Content1" runat="server" ContentPlaceHolderID="LogisticsMaster">
<script type="text/javascript" language="javascript">
 
     hs.graphicsDir = 'Assets/Images/popup/';
     
     function ValidateAndRedirect(redirectTo)
     {
        var redirectUrl = "Search.aspx";
        //check for mandatory selections
        var mandFields="<% =GetMandatoryTaxonomy() %>";
        var fieldsArray=mandFields.split(",");
        var counter=0;
        if(mandFields.length>0)
	    {
    	   for(field in fieldsArray)
    	   {
    	      if($("#advanced_criteria p:contains('"+fieldsArray[field]+"')").length == 0)
    	      {
    	         counter = counter+1;
    	      }
    	   }
    	   if(counter == fieldsArray.length)
	       {
	            //mandatory selections are not done, through alert
	            mandFields=mandFields.replace(/,/g," or ");
	            alert("Select "+mandFields);
	       }
	       else
	       {
	            //mandatory selections are done..                
                var StartYearDropdown = document.getElementById('<%=StartYear.ClientID %>');
                var EndYearDropdown = document.getElementById('<%=EndYear.ClientID %>');
                
                if(StartYearDropdown.value > EndYearDropdown.value)
                {
                    alert('Start year cannot be greater than End year');                    
                }
                else
                {
                    //Update start year value to session
                    UpdateSession('999', StartYearDropdown.value, 'startyear', 'yearsUpdate');
                    //Update end year value to session
                    UpdateSession('999', EndYearDropdown.value, 'endyear', 'yearsUpdate');
            
                    //Update the session 
                    syncUserSelectionsToSession();
                    //redirect the page after updating the session with current selections
                    window.location.href = redirectUrl;
                }
            }    	   
	    }        
    }
    function syncUserSelectionsToSession()
	{	    
	    //Update the session 
        var selection=$("#<% = hdnUserSelections.ClientID%>").val();
        if(selection.length>0)
        {
            UpdateSession("", "",selection,"addGroupSelection");
        }
	}
	function ClearSelections()
	{	
	    //clears all selections in right pane
        var taxTypeid= $("#hdnTaxonomyTypeID").val();
        var name=$("#hdnTaxonomyType").val();
        var controlType=$("#hdnControlType").val();
        var parentIDs=$("#<% =hdnparentIDs.ClientID %>").val();
        var fullid=$("#<% =hdnFullTaxonomyTypeID.ClientID %>").val();
        if(taxTypeid!="" && name!="" && controlType!="")
        {
            $("#advanced_criteria p").remove();
            //clear the session
            UpdateSession("0", "0", "0", "removeAll");            
            //clear the hidden user selections
            $("#<% = hdnUserSelections.ClientID%>").val("");

            //refresh the controls
            CallBack1.callback(taxTypeid,unescape(name),controlType,fullid,parentIDs);
        }
	}
	
	function tryToUncheckSelectedNode(taxonomyID)
	{
	    var currentTreeView =GetTreeView();	   	    
	    var currentNode = currentTreeView.findNodeByProperty('ID',taxonomyID); 	    	    
        try
        {            
            if(currentNode != null)
            {
                //uncheck current node
                currentTreeView.beginUpdate();                 
                currentNode.set_checked(false);                  
                currentTreeView.endUpdate();
            }
        }
        catch(ex){ }
	}
	
	function tryToUncheckSelectItem(taxonomyID)
	{	   
	    var str = "input[id='chk" + taxonomyID + "']";
	    $(str).attr('checked', false);	 
	}
	
	///This method is called when the check box inside grid selection control is checked 
	///Updates the corresponding selection in session
	function ListitemCheckChange(control, taxonomyType, taxonomyTypeID, taxonomyID, name)
    {   
        var str = "A[id='selspan" + taxonomyID + "']";	 
        var currentValue = $("#<%=hdnUserSelections.ClientID%>").val();
        var newSelection = taxonomyTypeID+'~'+ taxonomyID+'~'+name;  
                
        if (control.checked == true)
        {
            if($(str).length==0)
            {
                addToSelection(taxonomyTypeID, taxonomyType, taxonomyID, name,"false");                
                //Persist the current selections to hidden field
                if(currentValue.length==0)
                {
                    currentValue = currentValue +'|';
                }
                $("#<%=hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
            }               
        }
        else
        {
            if($(str).length==1)
            {
                removeFromSelection(taxonomyTypeID,taxonomyType, taxonomyID,name,"false");                
                //Persist the current selections to hidden field                
                $("#<%=hdnUserSelections.ClientID%>").val(currentValue.replace('|'+newSelection+'|', '|'));
             }
        }
        //alert($("#hdnUserSelections").val());
    }  
    
    function ExpandAndCheckAllChildNodes(currentNode,taxonomyTypeID,fullid,taxonomyType)
    {   
        var cNodes=currentNode.Nodes(); 
        for(var i=0;i<cNodes.length;i++)
        {  
            //if(cNodes[i].get_showCheckBox())
           // {
                //cNodes[i].set_checked(true);              
                
                if(cNodes[i].Nodes().length==0)
                {    
                    addToSelection(fullid, taxonomyType, cNodes[i].ID, escape(cNodes[i].Value),"false");
                    var newSelection = taxonomyTypeID+'~'+ cNodes[i].ID+'~'+cNodes[i].Value; 
                    var subTaxonomySelection =  fullid+'~'+ cNodes[i].ID+'~'+cNodes[i].Value;
                   
                    if(fullid!=null && fullid != taxonomyTypeID)
                    {
                        newSelection = newSelection + '|' + subTaxonomySelection;
                    }
                    var currentValue = $("#<% = hdnUserSelections.ClientID%>").val();                       
                    $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
                }
             //}
            cNodes[i].expand();
            if(cNodes[i].Nodes().length>0)
            {
                ExpandAndCheckAllChildNodes(cNodes[i],taxonomyTypeID,fullid,taxonomyType);
            }
        }
    }
    
    function CollapseAndUncheckAllChildNodes(currentNode,fullid,taxonomyType)
    {
        var cNodes=currentNode.Nodes();
        for(var i=0;i<cNodes.length;i++)
        {  
            //cNodes[i].set_checked(false); 
            cNodes[i].collapse();
            if(cNodes[i].Nodes().length==0)
            {
                removeFromSelection(fullid, taxonomyType, cNodes[i].ID, escape(cNodes[i].Value),"false");                
            }
            else
            {
                CollapseAndUncheckAllChildNodes(cNodes[i],fullid,taxonomyType);
            }
        }
    }
     
   ///This method is called when the check box inside tree control is checked 
	///Updates the corresponding selection in session
    function tvForAll_onNodeCheckChange(node)
    {   
        //debugger;        
        var taxonomyTypeID = $("#hdnTaxonomyTypeID").val();
        var taxonomyType = $("#hdnTaxonomyType").val();
        var currentValue = $("#<% = hdnUserSelections.ClientID%>").val();
        var fullid=$("#<%= hdnFullTaxonomyTypeID.ClientID%>").val();
         
        var newSelection = taxonomyTypeID+'~'+ node.ID+'~'+node.Value; 
        var subTaxonomySelection =  fullid+'~'+ node.ID+'~'+node.Value;
       
        if(fullid!=null && fullid != taxonomyTypeID)
        {
            newSelection = newSelection + '|' + subTaxonomySelection;
        }
                
        if(node.Checked==true)
        {   
            //Persist the current selections to hidden field
            if(currentValue.length==0)
            {
                currentValue = currentValue +'|';
            }            
            if(node.Nodes().length==0)
            {
                addToSelection(fullid, taxonomyType, node.ID, escape(node.Value),"false");
                $("#<%= hdnUserSelections.ClientID%>").val(currentValue + newSelection +'|');
            }
            else
            {
                node.expand();
                //check childs
                var currentTreeView =GetTreeView();	
                currentTreeView.beginUpdate();
                node.checkAll();
                currentTreeView.endUpdate();
                ExpandAndCheckAllChildNodes(node,taxonomyTypeID,fullid,taxonomyType);            
            }
        }
        else
        {
            var currentTreeView =GetTreeView();	
            currentTreeView.beginUpdate();
            node.unCheckAll();
            currentTreeView.endUpdate();
            removeFromSelection(fullid,taxonomyType, node.ID,  escape(node.Value),"false");     
            if(node.Nodes().length>0)
            {
                CollapseAndUncheckAllChildNodes(node,fullid,taxonomyType);
            }
        }
        //alert($("#<%= hdnUserSelections.ClientID%>").val());
    }
    
    function checkAllSubNode(nodeID)
    {
        var currentTreeView =GetTreeView();	   	            
        var node = currentTreeView.get_selectedNode();
        currentTreeView.beginUpdate();
        node.checkAll();
        node.set_checked(false); 
        tvForAll_onNodeCheckChange(node);
        currentTreeView.endUpdate();
        checkAllSubNodesofCurrentNode(node);
    }
    
    function checkAllSubNodesofCurrentNode(node)
    {
        var flag =false;
        for(var j=0;j<node.get_nodes().get_length();j++)
        {
            //alert(node.get_nodes().getNode(j).get_checked());
            if (node.get_nodes().getNode(j).get_showCheckBox())
            {
                //node.get_nodes().getNode(j).set_checked(true); 
                tvForAll_onNodeCheckChange(node.get_nodes().getNode(j));
            }
            if (node.get_nodes().getNode(j).get_clientTemplateId().indexOf("CheckAll")!=-1)
            {
                var curCTemplate = node.get_nodes().getNode(j).get_clientTemplateId(); 
                curCTemplate = curCTemplate.replace("UnCheckAll", ""); 
                curCTemplate = curCTemplate.replace("CheckAll", ""); 
                curCTemplate = curCTemplate + 'UnCheckAll';
                node.get_nodes().getNode(j).set_clientTemplateId(curCTemplate);
            }
            checkAllSubNodesofCurrentNode(node.get_nodes().getNode(j));
            flag=true;            
        }
        if (flag)
        {
          var curCTemplate = node.get_clientTemplateId(); 
          curCTemplate = curCTemplate.replace("UnCheckAll", ""); 
          curCTemplate = curCTemplate.replace("CheckAll", ""); 
          curCTemplate = curCTemplate + 'UnCheckAll';
          node.set_clientTemplateId(curCTemplate);
        }  
    }
    
    function unCheckAllSubNode(nodeID)
    {
        var currentTreeView =GetTreeView();	   	    
        var node = currentTreeView.get_selectedNode(); 
        currentTreeView.beginUpdate();
        node.unCheckAll();  
        tvForAll_onNodeCheckChange(node)
        unCheckAllSubNodesofCurrentNode(node);
        currentTreeView.endUpdate();
    }

    function unCheckAllSubNodesofCurrentNode(node)
    {
        var flag =false;  
        var arynodes = node.get_nodes();
        //alert(arynode.get_length());
        //alert(node.get_nodes().get_length());
        for(var j=0;j<arynodes.get_length();j++)
        {
            //alert(node.get_nodes().getNode(j).get_checked());
            //if (node.get_nodes().getNode(j).get_checked()==true && node.get_nodes().getNode(j).get_showCheckBox())
            //{
                //node.get_nodes().getNode(j).set_checked(false);
                tvForAll_onNodeCheckChange(arynodes.getNode(j)); 
            //}
            if (arynodes.getNode(j).get_clientTemplateId().indexOf("UnCheckAll")!=-1)
            {
                var curCTemplate = arynodes.getNode(j).get_clientTemplateId(); 
                curCTemplate = curCTemplate.replace("UnCheckAll", ""); 
                curCTemplate = curCTemplate.replace("CheckAll", "");
                curCTemplate = curCTemplate + "CheckAll";
                arynodes.getNode(j).set_clientTemplateId(curCTemplate);
            }
            unCheckAllSubNodesofCurrentNode(arynodes.getNode(j));
            flag =true;
        }
        if (flag)
        {
          var curCTemplate = node.get_clientTemplateId(); 
          curCTemplate = curCTemplate.replace("UnCheckAll", ""); 
          curCTemplate = curCTemplate.replace("CheckAll", "");
          curCTemplate = curCTemplate + 'CheckAll';
          node.set_clientTemplateId(curCTemplate);
        }
    }
    
    function removeFromSelection(fullid,taxonomyType, taxonomyID, name, canUpdateSession)
	{	
	    name=unescape(name);
        var strMsg = "true"; 
  	  
        var controlType = $("#hdnControlType").val();
        switch(controlType)
        {
            case "CHKLIST":
              tryToUncheckSelectItem(taxonomyID);
              break;
            case "TREE":
              tryToUncheckSelectedNode(taxonomyID);
              break;
            default:
        }	    
	    var str = "A[id='selspan" + taxonomyID + "']";
	    //alert($(str).text());
	    $(str).remove();
	    if(canUpdateSession == "true")
        {
	        //update session
            strMsg = UpdateSession(fullid, taxonomyID, name, "remove");
        }
        if(strMsg == "true")
        {
	        var selectionPara = "P[id='taxtype" + fullid + "']";
	        if($(selectionPara + " A").length==0)
	        {
	            $(selectionPara).remove();	          
	        }
	    }
	    //Persist the current selections to hidden field  
	    var currentValue = $("#<% = hdnUserSelections.ClientID%>").val(); 	    

        var newSelection = parseInt(fullid) +'~'+ taxonomyID+'~'+name; 
        var subTaxonomySelection =  fullid+'~'+ taxonomyID+'~'+name;

        $("#<% = hdnUserSelections.ClientID%>").val(currentValue.replace('|'+subTaxonomySelection+'|', '|'));
        currentValue = $("#<% = hdnUserSelections.ClientID%>").val(); 	    
        $("#<% = hdnUserSelections.ClientID%>").val(currentValue.replace('|'+newSelection+'|', '|'));       
	}	

    function renderControl(taxTypeid,name,controlType,needCallback, fullid, parentIDs)
    {
        //debugger;
        if($("#hdnControlType").val()=="TREE")
            RefreshTreview();      

        //change the navigation selection
        $("#navSection > ul li").removeClass("selected");
        $("#nav"+fullid).addClass("selected");   

        //set navigation changes          
        $("#hdnTaxonomyTypeID").val(taxTypeid);
        $("#hdnTaxonomyType").val(name);
        $("#hdnControlType").val(controlType);
       
        $("#<%= hdnFullTaxonomyTypeID.ClientID%>").val(fullid);
        $("#<%= hdnparentIDs.ClientID%>").val(parentIDs);
       
      if(needCallback=="true")
      {
         syncUserSelectionsToSession();
         CallBack1.callback(taxTypeid,unescape(name),controlType, fullid, parentIDs);
      }
    }
   
    function RefreshTreview()
    {
        var currentTreeView = GetTreeView();
        try
        {
            currentTreeView.get_nodes().clear();
        }
        catch(ex)
        {
        }
    }

</script>

 <div id="maincontent_full">
     <div id="pagetitle">
        <table width="100%;" style="height:25" border="0">
            <tr>
                <td width="580"> 
                    <h1><asp:Label ID="TitleLabel" runat="server"></asp:Label></h1>
                </td>
               
            </tr>
      </table>
    </div>
     <div style="float:right;position:relative;margin-top:-35px">
     <%--<GMPT:SavedSearch ID="SavedSearch1" runat="server"/>--%>
      </div>
    <ul id="database_tabs">        
        <li><a class="selected">Search</a></li>
        <li><asp:HyperLink ID="ViewResultsLink" runat="server" NavigateUrl="#" Text="View Results"></asp:HyperLink></li> <%--javascript:ValidateAndRedirect('Results');--%>
        <li><asp:HyperLink ID="AnalyzeResultsLink" runat="server" NavigateUrl="#" Text="Analyze Results"></asp:HyperLink></li> <%--javascript:ValidateAndRedirect('Analysis');--%>
          <li class="other_option last">
            <a href="#" class="ask" 
                style="font-size:10px; font-family:verdana; font-weight:lighter;" 
                onclick="return hs.htmlExpand(this, { contentId: 'highslide-ask' } )">Ask an analyst a question</a>
        </li>
        <li class="other_option">
           <div id="divMethodology" runat="server">                            
                <asp:HyperLink ID="MethodologyLink" runat="server" 
                    class="help" 
                    Target="_blank"
                    Text="Methodology"
                    ></asp:HyperLink>
            </div>
        </li>
         <li class="other_option">
            <div id="divNotes" runat="server">                            
                <asp:HyperLink ID="notesLink" runat="server" 
                    class="help" 
                    Target="_blank"
                    Text="Glossary"
                    ></asp:HyperLink>
            </div>
        </li>
        <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
    </ul>
    <br />
    <div id="column_other">  
          <div id="navSection" class="refinesearch">
            <h5>1) search type</h5>
            <div class="highslide-html-content" id="highslide-reset">

              <div class="highslide-header">
                <ul>
                  <li class="highslide-move"> <a href="#" onclick="return false">Move</a> </li>
                  <li class="highslide-close"> <a href="#" onclick="return hs.close(this)">Close</a> </li>
                </ul>
                <h1>Are you sure?</h1>

              </div>
              <div class="highslide-body">
                <p><b>Are you sure you want to switch to Keyword Search?</b></p>
                <p>You will lose all the terms you have entered for Field Search</p>
                <p>&nbsp;</p>
                <div class="button_right" style="width:90px"><a href="#" style="width:90px">OK</a></div>
                <div class="button_orange" style="width:90px"><a href="#" onclick="return hs.close(this)"style="width:90px">Cancel</a></div>

              </div>
            </div>
            <asp:Repeater ID="TaxonomyTypesList" runat="server" >
                <HeaderTemplate>
                    <ul>
                </HeaderTemplate>
                <ItemTemplate>
                    <li id ="nav<%# DataBinder.Eval(Container.DataItem, "FullID") %>" class="<%# DataBinder.Eval(Container.DataItem, "type") %>">
                        <a <%# GetVisiblity(DataBinder.Eval(Container.DataItem, "FullID").ToString()) %>
                                href="javascript:renderControl('<%# DataBinder.Eval(Container.DataItem, "ID") %>',escape('<%# DataBinder.Eval(Container.DataItem, "Name") %>'),'<%# DataBinder.Eval(Container.DataItem, "ControlType") %>','true','<%# DataBinder.Eval(Container.DataItem, "FullID") %>','<%# DataBinder.Eval(Container.DataItem, "ParentIds") %>');" ><%# DataBinder.Eval(Container.DataItem, "Name") %></a>
                    </li>
                </ItemTemplate>
                <FooterTemplate>
                    </ul>
                </FooterTemplate>
            </asp:Repeater>

          </div>
     </div>
     <div id="maincontent" class="productscan_powersearch">
          <div class="powersearch_col">
          <h1>2) Make selections</h1>
                                 
           <ComponentArt:CallBack id="CallBack1" Debug="false" runat="server" OnCallback="CallBack1_Callback" PostState="true" >
            <Content>   
            <asp:Label ID="SelectedTaxonomy" runat="server" 
                CssClass="H2Heading"></asp:Label> 
                                
                <div id="controlHolder" runat="server">   
                
                
             </div>
            </Content>
            <LoadingPanelClientTemplate>
              <table class="loadingpanel" width="100%" style="height:340" cellspacing="0" cellpadding="0" border="0">
              <tr>
                <td align="center">
                <table cellspacing="0" cellpadding="0" border="0">
                <tr>
                  <td colspan="2">
                  <div  style="font-size:12px;vertical-align:middle;text-align:center;" > Loading...&nbsp;<img alt="" src="assets/images/spinner.gif" width="16" height="16" style="border-width:0"/></div>
                  </td>
                </tr>
                </table>
                </td>
              </tr>
              </table>
            </LoadingPanelClientTemplate>
          </ComponentArt:CallBack>
            
            </div>

          <div class="powersearch_col2">
            <h1>3) Confirm your  Search </h1> 

            <div style="position:relative; overflow-x:auto; 
                        overflow-y:hidden;
                        width:300px; scrollbar-face-color: #BFC4D1;
	                    scrollbar-shadow-color: #FFFFFF;
	                    scrollbar-highlight-color: #FFFFFF;
	                    scrollbar-3dlight-color: #FFFFFF;
	                    scrollbar-darkshadow-color: #FFFFFF;
	                    scrollbar-track-color: #FFFFFF;
	                    scrollbar-arrow-color: #FFFFFF;
	                    padding-bottom:30px;">
                <uc3:Selections ID="UcSelections" runat="server"/>
               <br />
               
            Start Year <asp:DropDownList ID="StartYear" runat="server" 
                                Width="80"
                                Font-Size="11px">
                            </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;End Year <asp:DropDownList ID="EndYear" runat="server" 
                    Width="80"
                    Font-Size="11px">
                </asp:DropDownList>
             </div>
            <div class="hr">
            </div>
            <div class="button_clear" style="width:105px"><a href="javascript:ClearSelections();" style="width:105px">clear all</a></div>
            <div class="button_right" style="width:150px"><a href="#" style="width:150px">View Results</a></div> <%--javascript:ValidateAndRedirect('Results');--%>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <div class="button_right" style="width:150px"><a href="#" style="width:150px">Analyze Results</a></div>
          </div>
      </div>
</div>
     <input id="hdnControlType" type="hidden" value="" />
     <input id="hdnUserSelections" type="hidden" value="|" runat="server"/>
     <input id="hdnFullTaxonomyTypeID" runat="server" type="hidden" value="" />
     <input id="hdnparentIDs" runat="server" type="hidden" value="" /> 
     
<script language="javascript" type="text/javascript">

$(".static a").attr("href","#");
$(".static a").css("cursor","default");

</script>
</asp:Content>


using System;
using System.IO;
using System.Data;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.SessionState;
using Datamonitor.PremiumTools.Prototype.Library;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Prototype.Library
{
    /// <summary>
    /// Session handler to store session data
    /// </summary>  
    public class SessionHandler : IHttpHandler, IRequiresSessionState
    {
        /// <summary>The query string parameter to use for the taxonomy type id.</summary>
        public const string TAXONOMY_TYPE_ID_PARAM = "taxonomyTypeID";

        /// <summary>The query string parameter to use for the taxonomy id.</summary>
        public const string TAXONOMY_ID_PARAM = "taxonomyID";

        /// <summary>The query string parameter to use for the name.</summary>
        public const string NAME_PARAM = "name";

        /// <summary>The query string parameter to use for the action type.</summary>
        public const string ACTION_TYPE_PARAM = "actionType";

        /// <summary>The query string parameter to use to decide whether the taxonomy type has subcategories or not.</summary>
        public const string HASSUBTAXONOMYTYPES_TYPE_PARAM = "hasSubcategory";

        /// <summary>
        /// Enables processing of HTTP Web requests by a custom HttpHandler that implements the <see cref="T:System.Web.IHttpHandler"/> interface.
        /// </summary>
        /// <param name="context">An <see cref="T:System.Web.HttpContext"/> object that provides references to the intrinsic server objects (for example, Request, Response, Session, and Server) used to service HTTP requests.</param>
        public void ProcessRequest(HttpContext context)
        {

           
        }

        /// <summary>
        /// Gets the current selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <returns></returns>
        private static Dictionary<int, string> GetCurrentSelection(string taxonomyTypeID)
        {
            Dictionary<int, string> CurrentSelections = CurrentSession.GetFromSession<Dictionary<int, string>>(taxonomyTypeID);
            if (CurrentSelections == null)
            {
                CurrentSelections = new Dictionary<int, string>();
            }
            return CurrentSelections;           
           
        }        

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        /// <summary>
        /// Adds the or remove taxonomy selection.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="name">The name.</param>
        /// 
        /// <param name="actionType">Type of the action.</param>
        /// <returns></returns>
        private string AddOrRemoveTaxonomySelection(string taxonomyTypeID, 
            int taxonomyID, 
            string name, 
            string actionType,
            string hasSubCategory)
        {            
            string Status = string.Empty;
            
            return Status;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="taxonomyID"></param>
        /// <param name="actionType"></param>
        public static void savedSearchCriteria(string taxonomyID,string actionType,string columnID,string name,int i)
        {
              
        }

        /// <summary>
        /// Adds the or remove taxonomy selection for saved searches.
        /// </summary>
        /// <param name="taxonomyTypeID">The taxonomy type ID.</param>
        /// <param name="taxonomyID">The taxonomy ID.</param>
        /// <param name="name">The name.</param>
        /// 
        /// <param name="actionType">Type of the action.</param>
        /// <returns></returns>
        private static string AddOrRemoveTaxonomySelectionForSavedSearch(string taxonomyTypeID,
            int taxonomyID,
            string name,
            string actionType,
            string hasSubCategory)
        {
            string Status = string.Empty;

            //Get taxonomy type object based on the input taxonomy type id
            Dictionary<int, string> CurrentSelections = GetCurrentSelection(taxonomyTypeID);
           
            if (actionType.Equals("update"))
            {
                //Add selections to the dictionary
                CurrentSelections.Add(taxonomyID, name);

                //update session                  
                CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);

                if (!string.IsNullOrEmpty(hasSubCategory) && hasSubCategory == "true")
                {
                    DataSet SubTaxonomyTypeID = SqlDataService.GetSubTaxonomyTypeID(taxonomyID);
                    if (SubTaxonomyTypeID != null &&
                        SubTaxonomyTypeID.Tables.Count > 0 &&
                        SubTaxonomyTypeID.Tables[0].Rows.Count > 0)
                    {
                        taxonomyTypeID = SubTaxonomyTypeID.Tables[0].Rows[0][0].ToString();
                    }
                }

                CurrentSelections = GetCurrentSelection(taxonomyTypeID);
                if (!CurrentSelections.ContainsKey(Convert.ToInt32(taxonomyID)))
                {
                    //Add selections to the dictionary
                    CurrentSelections.Add(taxonomyID, name);

                    //update session                  
                    CurrentSession.SetInSession<Dictionary<int, string>>(taxonomyTypeID, CurrentSelections);
                }

                Status = "true";
            }           
            return Status;
        }
    }
}

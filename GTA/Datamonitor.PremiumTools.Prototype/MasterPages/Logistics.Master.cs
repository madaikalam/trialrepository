using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Text;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Prototype.Library;

namespace Datamonitor.PremiumTools.Prototype.MasterPages
{
    public partial class Logistics : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(SourceKC))
            {
                sourceCSS.Attributes.Add("href", Page.ResolveClientUrl("~/assets/css/datamonitor.css"));
                switch (SourceKC.ToLower())
                {
                    case "bgcio":
                        //Load footer info
                        footerbutlergroup.Visible = true;
                        footerdefault.Visible = false;
                        break;
                    case "ovumit":
                    case "telecoms":
                    case "vendor":
                    case "enterprise":
                        footerOvum.Visible = true;
                        footerdefault.Visible = false;
                        sourceCSS.Attributes.Add("href", Page.ResolveClientUrl("~/assets/css/ovum.css"));
                        break;
                }
            }
        }

        
    }
}

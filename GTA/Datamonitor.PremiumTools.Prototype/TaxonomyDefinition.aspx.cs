using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Prototype;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;

namespace Datamonitor.PremiumTools.Prototype
{
    public partial class TaxonomyDefinition : System.Web.UI.Page
    {

        //PAGE EVENTS

        protected void Page_Load(object sender, EventArgs e)
        {
            //Check 'taxonomyid' is there in query string
            if (Request.QueryString["taxonomyid"] != null && !string.IsNullOrEmpty(Request.QueryString["taxonomyid"]))
            {
                //Get the taxonomy definition for the taxonomyid
                try
                {
                    DataSet TaxonomyDefinition = SqlDataService.GetTaxonomyDefinition(Convert.ToInt32(Request.QueryString["taxonomyid"].ToString()));

                    if (TaxonomyDefinition != null &&
                        TaxonomyDefinition.Tables.Count > 0 &&
                        TaxonomyDefinition.Tables[0].Rows.Count > 0)
                    {
                        //Bind the taxonomy definition details to labels
                        Indicator.Text = TaxonomyDefinition.Tables[0].Rows[0]["Indicator"].ToString();
                        Definition.Text = TaxonomyDefinition.Tables[0].Rows[0]["Definition"].ToString();
                    }
                }
                catch { }
            }
        }
    }
}

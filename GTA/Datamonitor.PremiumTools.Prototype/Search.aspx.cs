using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Datamonitor.PremiumTools.Prototype.Controls;
using Datamonitor.PremiumTools.Prototype.Library;
using Datamonitor.PremiumTools.Prototype.Library.SqlDataAccess;
using ComponentArt.Web.UI;
using System.Collections.Generic;
using System.Text;

namespace Datamonitor.PremiumTools.Prototype
{
    public partial class Search : AuthenticationBasePage
    {
        // EVENT OVERRIDES

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //LoadUserSelectionsToHiddenField();
            if (!IsPostBack)
            {
                if (CurrentSite != null)
                {
                    //Load taxonomy selection links
                    LoadTaxonomyTypes();
                    //Bind years drop downs
                    BindYearsDropdown();

                    TitleLabel.Text = CurrentSite.SiteTitle;
                    divNotes.Visible = CurrentSite.NotesLink.Length > 0;
                    notesLink.NavigateUrl = CurrentSite.NotesLink;

                    //Methodology link
                    divMethodology.Visible = CurrentSite.MethodologyLink.Length > 0;
                    MethodologyLink.NavigateUrl = CurrentSite.MethodologyLink;
                }
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            this.CustomPageTitle = "Search";
        }

        // PRIVATE METHODS (initial population)

        private void BindYearsDropdown()
        {
            //Get list of valid years from config
            string[] ValidYears = CurrentSite.ValidYears;
            StartYear.DataSource = ValidYears;
            StartYear.DataBind();
            EndYear.DataSource = ValidYears;
            EndYear.DataBind();

            try
            {
                StartYear.SelectedValue = CurrentSite.StartYearSelectValue;
                EndYear.SelectedValue = CurrentSite.EndYearSelectValue;
            }
            catch (Exception ex) { }
        }
                    

        public void LoadTaxonomyTypes()
        {
            DataSet TaxonomyTypes = CurrentSite.GetTaxonomyLinks();
            TaxonomyTypesList.DataSource = TaxonomyTypes;
            TaxonomyTypesList.DataBind();

            //Load a taxonomy type details
            if (TaxonomyTypes != null &&
                TaxonomyTypes.Tables.Count > 0 &&
                TaxonomyTypes.Tables[0].Rows.Count > 0)
            {
                LoadDefaultTaxonomyTypeControl(Convert.ToInt32(TaxonomyTypes.Tables[0].Rows[0]["FullID"].ToString()),
                                TaxonomyTypes.Tables[0].Rows[0]["Name"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[0]["ControlType"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[0]["FullID"].ToString(),
                                TaxonomyTypes.Tables[0].Rows[0]["ParentIds"].ToString());
            }
        }

        private void LoadDefaultTaxonomyTypeControl(int taxonomyTypeID,
                     string taxonomyType,
                     string controlType,
                     string fullTaxonomyTypeID,
                     string parentIds)
        {
            SelectedTaxonomy.Text = taxonomyType;
            BaseSelectionControl SelectionControl;
           

            switch (controlType)
            {
                case "CHKLIST":
                    SelectionControl = (CheckListControl)Page.LoadControl("Controls/CheckListControl.ascx");
                    break;
                case "TREE":
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
                default:
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
            }

            SelectionControl.ID = taxonomyTypeID.ToString();
            SelectionControl.TaxonomyTypeID = taxonomyTypeID;
            SelectionControl.TaxonomyType = taxonomyType;
            SelectionControl.FullTaxonomyTypeID = fullTaxonomyTypeID;
            SelectionControl.ParentIDs = parentIds;
            SelectionControl.Source = "search";
            SelectionControl.Visible = true;
            SelectionControl.ShowTaxonomyDefinition = CurrentSite.ShowTaxonomyDefinitionInSearch;
            SelectionControl.CurrentSite = CurrentSite;
            SelectionControl.LoadData();

            controlHolder.Controls.Add(SelectionControl);

            StringBuilder Selections = new StringBuilder();
            Selections.Append("<script language='javascript' type='text/javascript'>");
            Selections.Append(string.Format("renderControl('{0}','{1}','{2}','{3}','{4}','{5}');",
                                taxonomyTypeID.ToString(),
                                taxonomyType,
                                controlType,
                                "false",
                                fullTaxonomyTypeID,
                                parentIds));
            Selections.Append("</script>");
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "loadDefaultControl", Selections.ToString(), false);
            hdnFullTaxonomyTypeID.Value = fullTaxonomyTypeID;
        }

       
        //PUBLIC METHODS

        public string GetVisiblity(string id)
        {
            object ViewID = CurrentSession.GetFromSession<string>("PredefinedViewID");

            if (ViewID != null)
            {
                string SelectableTaxonomyTypes = GetPredefinedViewSelectableIDs((string)ViewID);
                SelectableTaxonomyTypes = "," + SelectableTaxonomyTypes + ",";

                if (SelectableTaxonomyTypes.Length > 0 && !SelectableTaxonomyTypes.Contains("," + id + ","))
                {
                    return "disabled";
                }
            }

            return "";
        }

        public string GetMandatoryTaxonomy()
        {
            string MandatoryTaxonomy = string.Empty;

            //MandatoryTaxonomy = CurrentSite.GetMandatoryTaxonomyInSearch;

            return MandatoryTaxonomy;
        }

        // EVENT HANDLERS

        public void CallBack1_Callback(object sender, ComponentArt.Web.UI.CallBackEventArgs e)
        {
            int taxonomyTypeID = Convert.ToInt32(e.Parameters[0]);
            string taxonomyType = e.Parameters[1].ToString();
            string ControlType = e.Parameters[2].ToString();
            string FullTaxonomyTypeID = e.Parameters[3].ToString();
            string ParentIds = e.Parameters[4].ToString();

            SelectedTaxonomy.Text = taxonomyType;
            SelectedTaxonomy.RenderControl(e.Output);

            BaseSelectionControl SelectionControl;           

            switch (ControlType)
            {
                case "CHKLIST":
                    SelectionControl = (CheckListControl)Page.LoadControl("Controls/CheckListControl.ascx");
                    break;
                case "TREE":
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
                default:
                    SelectionControl = (TreeviewControl)Page.LoadControl("Controls/TreeviewControl.ascx");
                    break;
            }

            controlHolder.Controls.Add(SelectionControl);

            SelectionControl.ID = taxonomyTypeID.ToString();
            SelectionControl.TaxonomyTypeID = taxonomyTypeID;
            SelectionControl.TaxonomyType = taxonomyType;
            SelectionControl.FullTaxonomyTypeID = FullTaxonomyTypeID;
            SelectionControl.ParentIDs = ParentIds;
            SelectionControl.Source = "search";
            SelectionControl.ShowTaxonomyDefinition = CurrentSite.ShowTaxonomyDefinitionInSearch;
            SelectionControl.Visible = true;
            SelectionControl.CurrentSite = CurrentSite;
            SelectionControl.LoadData();
            SelectionControl.RenderControl(e.Output);

        }

        public string GetResultsPage()
        {
            return ""; //GlobalSettings.DefaultResultsPage;
        }
    }
}
